//----------------------------------------------------------------------------------
//! Macro module LUTselector
/*!
// \file    LUTselector.script
// \author  K.M. van Hespen
// \date    2019-01-15
//
// Lets you pick a predefined LUT, without the need to create one yourself
*/
//----------------------------------------------------------------------------------



Interface {
  Inputs {
    Field MLbase{
      internalName = BaseBypass.baseIn0
      comment      = "Input histogram"
    }
  }
  Outputs {
    Field LUTfunction {
      internalName = LUTSelect.outLUT
      comment      = "LUTfunction"
    }
    Field SoMLLUT {
      internalName = SoSwitch2.self
      comment      = "SoMLLUT"
    }
  }
  Parameters {
    Field LUTsel { type = Enum 
      items {
        item 2 { title = "Viridis" }
        item 0 { title = "Plasma" }
        item 4 { title = "Inferno" }
        item 3 { title = "Cool-Warm" }
        item 1 { title = "Jet" }

        
      }
    }
      Field rangeMin {type=Double value=0}
      Field rangeMax{type=Double value=1}
      Field applyNewRange{type=Trigger}
    
  }
}
Window{ 
  Box Colormap {
    ComboBox LUTsel {
      editable = False
    }
  }
}

Commands {
  source = $(LOCAL)/LUTselector.py
  Fieldlistener LUTsel{command = "*py: ctx.field("LUTSelect.row").setValue(int(ctx.field("LUTsel").value))*" }
  FieldListener rangeMin {command = "*py: ctx.field("RangeMinHolder.arg1X").setValue(ctx.field("rangeMin").value)*"}
  FieldListener rangeMax {command = "*py: ctx.field("RangeMaxHolder.arg1X").setValue(ctx.field("rangeMax").value);*"}
  FieldListener applyNewRange {command = "*py:ctx.field("RangeMinHolder.arg1X").setValue(ctx.field("rangeMin").value); ctx.field("RangeMaxHolder.arg1X").setValue(ctx.field("rangeMax").value);ctx.field("applyNewRangeHolder.input0").touch();*"}

}