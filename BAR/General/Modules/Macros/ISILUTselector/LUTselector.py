# ----------------------------------------------------------------------------

# 
#  \file    LUTselector.py
#  \author  K.M. van Hespen
#  \date    2019-01-15
#
#  Lets you pick a predefined LUT, without the need to create one yourself

# ----------------------------------------------------------------------------

from mevis import *
