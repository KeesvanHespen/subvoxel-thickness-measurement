# ----------------------------------------------------------------------------

# 
#  \file    Anonymize_dicom.py
#  \author  K.M. van Hespen
#  \date    2018-11-26
#
#  Tool for anonymizing folders of Dicoms

# ----------------------------------------------------------------------------
#!/usr/bin/env python

"""
Python script employing Lasagne and Theano to calculate the vessel wall 
thickness from image patches at any given resolution."""
from mevis import *
import numpy as np
import h5py
import scipy.io as sio
import os

def init():
  ctx.field("DirectDicomImport.progress").setValue(0)
  ctx.field("DirectDicomImport.clearResultCache").touch()
  ctx.field("DirectDicomImport.clearVolumeList").touch()
  ctx.field("StringUtils2.string1").connectFrom(ctx.field("FileInformation2.dirname"))
  ctx.field("ImageSave.filename").connectFrom(ctx.field("StringUtils6.result"))
  
def anonymize_dicom():
  if ctx.field("MessageBox.returnCode").value==1:
    ctx.field("Outputlog").setStringValue('')
    ctx.field("FileInformation.path").setStringValue(ctx.field("dicom_folder_location").value)
    isdir=1
    
    if ctx.field("FileInformation.isDirectory").value==False:
      #if only a single dicom is given
      ctx.field("DirectDicomImport.source").setStringValue(ctx.field("FileInformation.dirname").stringValue())
      isdir=0
    else:
      ctx.field("DirectDicomImport.source").setStringValue(ctx.field("dicom_folder_location").value)
    
    if ctx.field("anonymization_tag").value:
      
      ctx.field("Outputlog").setStringValue("Anonymizing with tag: " + ctx.field("anonymization_tag").value )
      ctx.field("Outputlog").setStringValue(ctx.field("Outputlog").stringValue()+"\nLoading Dicoms... ")
      ctx.field("DirectDicomImport.dplImport").touch()
      ctx.field("Outputlog").setStringValue(ctx.field("Outputlog").stringValue()+"\nLoading complete... ")
      #Set anonymization ID etc
      ctx.field("StringUtils.string3").setStringValue(ctx.field("anonymization_tag").value)
     
      ctx.field("AnonymizeMacro.patientsName").setStringValue(ctx.field("anonymization_tag").value)
      ctx.field("AnonymizeMacro.patientID").setStringValue(ctx.field("anonymization_tag").value)
      #Create subfolder
      if ctx.field("subfoldersave").value==True and not os.path.exists(ctx.field("StringUtils3.result").value):
        os.mkdir(ctx.field("StringUtils3.result").value)
      # Loop over all available dicoms
      for volume_nr in range(0,ctx.field("DirectDicomImport.numVolumes").value):
        ctx.field("DirectDicomImport.outVolume").setValue(volume_nr)
        #Check if image has already been anonymized, or if override is active
        if ctx.field("StringUtils5.boolResult").value==False or ctx.field("ImageSave.allowOverwrite").value==True:
          
          if isdir or (not isdir and ctx.field("StringUtils1.boolResult").boolValue()==True):
            #Check if file is an image.
            if ctx.field("AnonymizeMacro.status").value=='No image': 
              ctx.field("Outputlog").setStringValue(ctx.field("Outputlog").stringValue()+"\nFile: "+ ctx.field("StringUtils.string1").stringValue() +"is not an image, skipping saving")
            else:
              #Save anonymized image
              ctx.field("ImageSave.save").touch()  
              #Perform extra steps for cleaning up old image file
              if ctx.field("clean_up_named_files").value==True:
                os.remove(ctx.field("FileInformation1.path").value[:-1])
                os.rename(ctx.field("ImageSave.filename").value,ctx.field("ImageSave.filename").value.replace('_tmp',''))
              #Check if saving worked
              if  "Target file exists" in ctx.field("ImageSave.status").value:
                ctx.field("Outputlog").setStringValue(ctx.field("Outputlog").stringValue()+"\nFile: "+ ctx.field("ImageSave.filename").stringValue()+" already exists, skipping saving. For override, check the options.")
              else:
                ctx.field("Outputlog").setStringValue(ctx.field("Outputlog").stringValue()+"\nSaving: "+ ctx.field("StringUtils.string1").stringValue() +"as: " + ctx.field("ImageSave.filename").stringValue().replace('_tmp.dcm','.dcm'))
        else:
          ctx.field("Outputlog").setStringValue(ctx.field("Outputlog").stringValue()+"\nFile: "+ ctx.field("StringUtils.string1").stringValue() +"already anonymized. Check options for override")
        ctx.field("prbar").setValue((volume_nr+1)/ctx.field("DirectDicomImport.numVolumes").value)
    else:
      ctx.field("Outputlog").setStringValue(ctx.field("Outputlog").stringValue()+"Aborting process. No anonymization tag is given.\n")
  else:
    ctx.field("Outputlog").setStringValue('')
    ctx.field("Outputlog").setStringValue(ctx.field("Outputlog").stringValue()+"Aborting process. User cancelled")
    
def options():
  window = ctx.showWindow("optionswindow")

def addsubfolder():
  if ctx.field("subfoldersave").value==True:
    ctx.field("StringUtils2.string1").connectFrom(ctx.field("StringUtils3.result"))
  else:
    ctx.field("StringUtils2.string1").connectFrom(ctx.field("FileInformation2.dirname"))
def set_anonymization_tag():
  ctx.field("StringUtils4.string2").setStringValue(ctx.field("anonymization_tag").value)
  ctx.field("MessageBox.show").touch()
  
def clean_up():
  if ctx.field("clean_up_named_files").value==True:
    ctx.field("ImageSave.filename").connectFrom(ctx.field("StringUtils6.result"))
  else:
    ctx.field("ImageSave.filename").connectFrom(ctx.field("StringUtils2.result"))