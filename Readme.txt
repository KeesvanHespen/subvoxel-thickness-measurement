Code used for MEDIA paper 

run_NN_lowres, runs the neural network with the low resolution images.
run_NN_sample_single_scan runs the neural network for a single test scan, e.g. an intracranial aneurysm images
run_NN_test_set_augmentation, performs test set augmentation, and run_NN_test_set_dropout runs the network with test time dropout.

Supporting scripts and functions contains some scripts to e.g. load the neural network .npy files into matlab for visualization purposes (loadresultsperscan.m), and to correct the vessel wall lumen of the low resolution magnitude images (Correct_lumen).
Junk files are not used in the paper.

Visualization scripts were used to visualize the high resolution and low resolution image patches, and to visualize the neural network kernel weights.
Other visualization tools are mainly located in the repository of the FWHM thickness estimation paper.

-----KM van Hespen, 2020, UMC Utrecht-----