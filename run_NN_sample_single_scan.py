# -*- coding: utf-8 -*-
# pylint: disable=C0301
"""
Python script employing Lasagne and Theano to calculate the vessel wall
thickness from image patches for a single scan (e.g. an aneurysm vessel wall image)
"""

from __future__ import print_function
import datetime
import lasagne
from lasagne .utils import as_theano_expression
import nibabel as nib
import numpy as np
import os
import pickle
import random
import scipy
import scipy.stats
import dicom
import theano
import theano.tensor as T

global ROIdcmname, comments, errorfunction, targetvoxelsize, patchsz, optionsdict, dateandtime, datadir, samplelocations, resultdir, curtime, FULLdcmname
global alpha, beta, nrofbestnetworks, dateandtime, nrofsample, targetvoxelsize, sampledata, patchsz, excludelargerthan, selectionrange, num_epochs, optionsdict, lr, blurstrength, epochdif, excludesmallerthan, trainset, trainvalratio, testset, mailupdatefrequency, mailadress

"""
datadir:            Directory of the image patch data'
loadresults:        Suffix of the neural network results to be loaded in, with format: '0.4000370037_acq_0_2019-02-07_12;25;38_'
sampledata:           If 1, samples are sampled regardless of existence of sample .p file
samplelocations:      Nifti mask file containing locations where to sample image patches. Patches will be sampled centered around the True locations.
FULLdcmname:          Location and name of the high resolution dicom image.
patchsz:              Size of the patches in voxels. Can be non isotropic.
FULLniiname:          Location and name of the segmented vessel wall image. This image should contain vessel walls + noise background.
resultdir:            Directory where all results are stored.
lr:                   learning rate
batchsize:            Batchsize, for neural network optimization
errorfunction:        if 0 use MSE, if 1 use MAE, if 2 use weighted MSE, if 3 use weighted MAE.
alpha, beta:          Target values larger than alpha, get weight beta in the error function. Values below alpha, get weight 1
"""

datadir = 'D:/6. Working Data-FWHM-NN/2. NN method/3. Data/'
loadresults = '0.4000370037_acq_0_2019-02-07_12;25;38_'
samplelocations = ['C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/V3060/sample_locations.nii']
FULLdcmname = ["C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/V3060/V3060_05_01_3D T1w MPIR-TSE.dcm"]
FULLniiname = ["C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/V3060/extracted_vessels_aneurysmfix.nii"]
resultdir = 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/4. Results/'
targetvoxelsize = 0.4
patchsz = 19
lr = 0.00001
batchsize = 16
errorfunction = 0
alpha, beta = 0.45, 35

# Do not change values below
dateandtime = datetime.datetime.now().strftime('%Y-%m-%d_%H;%M;%S')
curtime = dateandtime
optionsdict = {'nrofsample':np.inf, 'targetvoxelsize':targetvoxelsize, 'sampledata':FULLniiname, 'sampledateandtime': dateandtime, 'patchsz':patchsz, 'excludesmallerthan':0, 'excludelargerthan':100, 'num_epochs':0, 'selectionrange':np.array([0, 0]), 'learningrate':0, 'blurstrength':0, 'scanorder':FULLniiname, 'trainset':np.array([-1]), 'valset':np.array([-1]), 'testset':np.array([0]), 'batchsize':0, 'errorfunction':-1, 'alpha':0, 'beta':0, 'trainvalratio':0, 'trainvalperms':0, 'nrofbestnetworks':0}


def samplefunction(samplelocations, imagelocation):
    """
    ########################################
    #                                      #
    #   Samples patches                    #
    #                                      #
    ########################################
    INPUT: samplelocations: Mask with True values being locations where patches need to be sampled around.
           imagelocation:   Equal to FULLniiname


    OUTPUT:samples:         Numpy array containing all samples
           labels:          Numpy array containing all labels

    """
    info = dicom.read_file(FULLdcmname[0])
    try:
        np.array(info.ImagePositionPatient)
    except:
        instance = 0
        foundfirstslice = 0
        while not foundfirstslice:
            if info.PerFrameFunctionalGroupsSequence[instance].FrameContentSequence[0].InStackPositionNumber == 1:
                foundfirstslice = 1
            else:
                instance += 1
    try:
        pixelspacing = info.PixelSpacing
    except:
        pixelspacing = info.PerFrameFunctionalGroupsSequence[instance].PixelMeasuresSequence[0].PixelSpacing
    dx = pixelspacing[0]
    dy = pixelspacing[1]
    dz = info.SpacingBetweenSlices
    sourcevoxelsize = np.array([dx, dy, dz])
    samples = []
    labels = []
    scannamelist_current = []


    doaugment = 0
    maskedthick = samplelocations
    thicknessmaplist = np.concatenate((maskedthick[np.nonzero(maskedthick)][..., None], np.array(np.nonzero(maskedthick)).T, 0 * np.ones((maskedthick[np.nonzero(maskedthick)].shape[0], 1))), axis=1)

    issamplearray = np.zeros((1, thicknessmaplist.shape[0]))
    readimage = nib.load(imagelocation).get_data()
    imgsize_current = np.array([readimage.shape[0], readimage.shape[1], readimage.shape[2]])

    issamplearray += thicknessmaplist[..., 4] == 0
    scannamelist_current.append(imagelocation)
    issamplearray = np.transpose(issamplearray, (1, 0)) > 0
    thicknessmap_current = thicknessmaplist
    roi_to_full_wmat = np.eye(4)
    # sample samples or load data if sample file exists
    samples, labels, patchorigin = samplingfunction(thicknessmap_current, scannamelist_current, imgsize_current, np.array([np.inf]), targetvoxelsize, sourcevoxelsize, patchsz, roi_to_full_wmat, '', datetime.datetime.now(), doaugment)
    samples = np.array(np.transpose(samples[..., np.newaxis], (0, 4, 1, 2, 3)).astype('float16'))
    labels = np.squeeze(labels.astype('f'))
    networkno = 0
    sampleset = '_test'
    data = {'samples':samples, 'labels':labels, 'options':optionsdict}
    pickle.dump(data, open(os.path.join(datadir, 'samples_' + str(targetvoxelsize) + '_acq_' + str(networkno) + sampleset + '_' + dateandtime + '_invivo.p'), 'wb'), protocol=4)
    data = []

    np.save(os.path.join(datadir, 'samples_' + str(targetvoxelsize) + '_acq_' + str(networkno) + sampleset + '_' + dateandtime + '_invivo'), samples.astype('float32'))
    np.save(os.path.join(datadir, 'samples_' + str(targetvoxelsize) + '_acq_' + str(networkno) + '_opts' + sampleset + '_' + dateandtime + '_invivo'), optionsdict)
    np.save(os.path.join(datadir, 'samples_' + str(targetvoxelsize) + '_acq_' + str(networkno) + sampleset + 'coords+origin_' + dateandtime + '_invivo.npy'), patchorigin)
    return samples, labels, patchorigin


def samplingfunction(thicknessmap, scannamelist, imgsize, nrofsamples, target_voxelsize, source_voxelsize, patchsize, roi_to_full_wmat, angle='', seed=datetime.datetime.now(), doaugment=1):
    """
    ########################################
    #                                      #
    #   Samples patches equally over the   #
    #    range of possible thicknesses     #
    #                                      #
    ########################################
    INPUT: thicknessmap:     can be a list of nD thickness maps. If input is a numpy array, sampling is equalized accross the range of thickness values given in.
                             If a numpy array is given, first three columns represent the x,y,z coordinates. The fourth column is the thickness values, the fifth is the number corresponding to the scan number
           scannamelist      List containing all scan names corresponding to the unique sorted scan numbers in thicknessmap
           nrofsamples:      number of samples that need to be sampled
           target_voxelsize: Target voxelsize. If int, is assumed to be isotropic in all directions
           source_voxelsize: Source voxelsize. If int, is assumed to be isotropic in all directions
           patchsize:        patchsize. If int, patchsize is assumed same in all directions. Value should be uneven!
           roi_to_full_wmat: World coordinate transformation matrix, from roi coordinates (thicknessmap) to full image coordinates. Only applicable if the thickness maps are ROIs within the images from scannamelist. Give indentity matrix if both images are in same space.
           angle:            Sampling angle. If not given, samples are sampled with random angle
           seed:             optional seed for angle generation
           doaugment:        If doaugment==1, make rotated patches, if 0, do not rotate


    OUTPUT:samples:         Numpy array containing all samples
           labels:          Numpy array containing all labels

    """
    # Set variables
    random.seed(seed)
    target_voxelsize = np.array(target_voxelsize)
    source_voxelsize = np.array(source_voxelsize)
    patchsize = np.array(patchsize)
    patches = []
    nrofsamples = nrofsamples[0]
    infinitesamples = 0
    allusedspecimens = np.sort([0])
    if nrofsamples == np.inf:
        infinitesamples = 1
    nrofsamples = nrofsamples.astype('int')
    if target_voxelsize.size == 1:
        target_voxelsize = np.array([target_voxelsize, target_voxelsize, target_voxelsize])
    if source_voxelsize.size == 1:
        source_voxelsize = np.array([source_voxelsize, source_voxelsize, source_voxelsize])
    if patchsize.size == 1:
        patchsize = np.array([patchsize, patchsize, patchsize])

    #generate samples from all non zero elements in the HR image
    scanindices = np.unique(thicknessmap[..., 4]).astype('int')
    idx_in_short_list = np.where(allusedspecimens == scanindices[0])[0][0]
    scancoords = (thicknessmap[..., 1:4][thicknessmap[..., 4] == scanindices[0]]).astype('uint16')
    large_image_coords = [np.dot(roi_to_full_wmat[idx_in_short_list], np.concatenate((np.array(scancoords.T), np.ones((1, np.array(scancoords.T).shape[1])))))[:3, ...]]


    idx_in_short_list = np.where(allusedspecimens == scanindices[0])[0][0]
    scancoords = (thicknessmap[..., 1:4][thicknessmap[..., 4] == scanindices[0]]).astype('uint16')
    large_image_coords = np.transpose(scancoords)
    #get thickness values as labels from the selected samples
    equalize = 1
    if infinitesamples:
        equalize = 0
        nrofsamples = thicknessmap.shape[0]
    labels = np.zeros((nrofsamples, 1), dtype='float32')
    patch = np.zeros((nrofsamples, patchsize[0], patchsize[1], patchsize[2]), dtype='float16')
    patchorigin = []
    patchoriginindex = np.squeeze(np.zeros((nrofsamples, 1), dtype='int32')) - 1
    patches = np.zeros((nrofsamples, 3, patchsize[0], patchsize[1], patchsize[2]), dtype='float32')
    samples = 0

    print('Sampling patches...')
    random.seed(datetime.datetime.now())
    #do the sorting beforehand, may save time
    bins = 20
    binsize = (np.max(thicknessmap[:, 0]) - np.min(thicknessmap[:, 0])) / bins
    binnedthicknesses = []
    for curbin in range(0, bins):
        binnedthicknesses.append(thicknessmap[np.logical_and(thicknessmap[:, 0] >= (binsize * curbin + np.min(thicknessmap[:, 0])), thicknessmap[:, 0] <= (binsize*(curbin + 1) + np.min(thicknessmap[:, 0])))])
    got_here = 0
    while samples < nrofsamples:
        try:
            if equalize:
                usebin = random.randint(0, bins - 1)
                randidx = np.random.choice(np.where(np.invert(np.isinf(binnedthicknesses[usebin][:, 0])) == True)[0])
                selected_sample = binnedthicknesses[usebin][randidx, :]
                randomthvalue = selected_sample[0]
                if randomthvalue == np.inf:
                    print('Got an inf value somewhere')
                randomsampleindices = np.squeeze(np.equal(selected_sample, thicknessmap))
                randomsampleindices = np.where(np.sum(randomsampleindices, 1) == selected_sample.size)[0][0]
            else:
                randomsampleindices = samples

            samplecoords = np.squeeze(large_image_coords[..., randomsampleindices].T)
            usemap = thicknessmap[randomsampleindices, 4].astype(int)

            sampletry = 0
        except Exception:
            print('somehow got here. Not good. Probably rerun')
            break

        # test sample x times with a random orientation. If voxels from the patch fall outside the image we eventually discard the voxel.
        while sampletry < 3:
            if angle == '' and doaugment == 1:
                sampleangle = np.array([random.randrange(0, 360), random.randrange(0, 360), random.randrange(0, 360)])
            elif angle == '' and doaugment != 1:
                sampleangle = np.array([0, 0, 0])
            else:
                sampleangle = angle
                if isinstance(sampleangle, int):
                    sampleangle = np.array([sampleangle, sampleangle, sampleangle])

            x = np.linspace(-target_voxelsize[0] / source_voxelsize[0] * (patchsize[0] / 2 - 0.5) + samplecoords[0], target_voxelsize[0] / source_voxelsize[0] * (patchsize[0] / 2 - 0.5) + samplecoords[0], patchsize[0])
            y = np.linspace(-target_voxelsize[1] / source_voxelsize[1] * (patchsize[1] / 2 - 0.5) + samplecoords[1], target_voxelsize[1] / source_voxelsize[1] * (patchsize[1] / 2 - 0.5) + samplecoords[1], patchsize[1])
            z = np.linspace(-target_voxelsize[2] / source_voxelsize[2] * (patchsize[2] / 2 - 0.5) + samplecoords[2], target_voxelsize[2] / source_voxelsize[2] * (patchsize[2] / 2 - 0.5) + samplecoords[2], patchsize[2])

            patchcoords = np.array(np.meshgrid(x, y, z))
            patchcoordszerocentered = patchcoords - samplecoords[..., None, None, None].astype(int)

            patchcoordsrotatedzerocentered = np.dot(np.concatenate((patchcoordszerocentered, np.zeros([1, len(x), len(y), len(z)])), 0).T, rotation_matrix(1, sampleangle)).T
            patchcoordsrotated = patchcoordsrotatedzerocentered[0:3, ...] + samplecoords[..., None, None, None].astype(int)

            sampletry += 1
            # save patch coordinates that fall into image
            if not any(singlecoord < 0 for singlecoord in np.array([np.min(patchcoordsrotated[0, ...]), np.min(patchcoordsrotated[1, ...]), np.min(patchcoordsrotated[2, ...])])) and not any(np.array([np.max(patchcoordsrotated[0, ...]), np.max(patchcoordsrotated[1, ...]), np.max(patchcoordsrotated[2, ...])]) - imgsize[np.where(scanindices == usemap)[0][0]] > 0):
                patches[samples, :, :, :, :] = patchcoordsrotated[np.newaxis, np.newaxis, ...]
                patchorigin.append(usemap)
                sampletry = np.inf
                patchoriginindex[samples] = randomsampleindices
                samples += 1
        # delete coordinate that fails to fit a patch (doesnt update the weights. should do that)
        if not sampletry == np.inf:
            got_here += 1
            if got_here < 20:
                print('Getting here shouldnt occur. For this voxel, measured in the ROI, falls out of bounds in the large dicom')
                print(str(samples))
            if doaugment:
                binnedthicknesses[usebin][randidx, 0] = np.inf
            else:
                large_image_coords = np.delete(large_image_coords, randomsampleindices, 1)
                thicknessmap = np.delete(thicknessmap, randomsampleindices, 0)

    print("Number of deleted samples: " + str(got_here))

    binnedthicknesses = ''
    large_image_coords = ''
    patchoriginindex = patchoriginindex[:samples]
    patches = patches[0:samples, ...]
    patch = patch[0:samples, ...]
    # get thickness label and patch intensity values`
    print('Interpolating patch intensity values...')
    for scan in range(len(scanindices)):
        print('Sampling from scan ' + str(scan + 1) + ' out of ' + str(len(scanindices)))
        selectedpatches = np.transpose(np.array(np.where(np.array(patchorigin) == scanindices[scan])))
        selectedpatchescoords = np.squeeze(patches[selectedpatches])
        if len(selectedpatchescoords.shape) < 5:
            selectedpatchescoords = selectedpatchescoords[np.newaxis, ...]
        if selectedpatches.size:
            niioutputImage = nib.load(FULLniiname[0])
            loadedscan = niioutputImage.get_data()
            niioutputImage = ''
            patch[selectedpatches[:, 0]] = scipy.ndimage.interpolation.map_coordinates(loadedscan,
                                                                                       np.array([np.array(selectedpatchescoords)[:, 0, ...], np.array(selectedpatchescoords)[:, 1, ...], np.array(selectedpatchescoords)[:, 2, ...]]), order=3)
            loadedscan = ''
        labels = thicknessmap[:, 0][patchoriginindex]
    print('Sampling complete!')
    return patch, np.array(labels), thicknessmap[patchoriginindex]

def rotation_matrix(axis, theta):
    """np.array(patches[1:100])[:,2,...].shape
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    theta = np.array(theta) / 360 * np.pi
    if len(np.array(theta)) == 1:
        theta = np.array([theta, theta, theta])

    rotationmatrix_x = np.array([[np.cos(theta[0]), -np.sin(theta[0]), 0, 0], [np.sin(theta[0]), np.cos(theta[0]), 0, 0], [0, 0, 1, 0], [0, 0, 0, 1]])
    rotationmatrix_y = np.array([[1, 0, 0, 0], [0, np.cos(theta[1]), -np.sin(theta[1]), 0], [0, np.sin(theta[1]), np.cos(theta[1]), 0], [0, 0, 0, 1]])
    rotationmatrix_z = np.array([[np.cos(theta[2]), 0, np.sin(theta[2]), 0], [0, 1, 0, 0], [-np.sin(theta[2]), 0, np.cos(theta[2]), 0], [0, 0, 0, 1]])

    rotationmatrix = np.dot(np.dot(rotationmatrix_x, rotationmatrix_y), rotationmatrix_z)
    return np.array(rotationmatrix)

def main():
    """
    ########################################
    #                                      #
    #    Main script that runs the entire  #
    #    processing pipeline               #
    #                                      #
    ########################################
    Inputs are described at the beginning of the script file.

    """
    data = nib.load(samplelocations[0]).get_data()

    X_test, y_test, testset = samplefunction(data, FULLniiname[0])

    input_var = T.tensor5('inputs', dtype='float32')
    target_var = T.fvector('targets')

    # Create neural network model (depending on first command line parameter)
    print("Building model and compiling functions...")
    network = build_mlp(input_var)
    holdweights = np.load(os.path.join(resultdir, 'network_weights_vsize_' + loadresults + 'LR.npy'))
    lasagne.layers.set_all_param_values(network, holdweights)

    lasagne.layers.count_params(network)
    global networkstring
    networkstring = [(layer.__class__, layer.__dict__) for layer in lasagne.layers.get_all_layers(network)]

    # Create a loss expression for training, i.e., a scalar objective we want
    # to minimize
    prediction = lasagne.layers.get_output(network)
    test_prediction = lasagne.layers.get_output(network, deterministic=True)
    if errorfunction == 0:
        loss = mean_squared_error(prediction, target_var)
        test_loss = mean_squared_error(test_prediction, target_var)
    elif errorfunction == 1:
        loss = mean_absolute_error(prediction, target_var)
        test_loss = mean_absolute_error(test_prediction, target_var)
    elif errorfunction == 2:
        loss = weighted_squared_error(prediction, target_var)
        test_loss = weighted_squared_error(test_prediction, target_var)
    elif errorfunction == 3:
        loss = weighted_absolute_error(prediction, target_var)
        test_loss = weighted_absolute_error(test_prediction, target_var)
    loss = loss.mean()
    test_loss = test_loss.mean()

    MSE = mean_squared_error(test_prediction, target_var)
    MSE = MSE.mean()
    MSE_fn = theano.function([input_var, target_var], MSE)

    networkno = 0

    # Compile a second function computing the validation loss:
    val_fn = theano.function([input_var, target_var], [test_loss])
    pred_fn = theano.function([input_var], test_prediction)

    # Finally, launch the training loop. If you want to load some results, only start the validation loop.
    if loadresults and os.path.isfile(os.path.join(resultdir, 'network_weights_vsize_' + loadresults + 'LR.npy')):
        holdepoch = 0
        epochhold = np.array([0])
        print("Starting testing...")
        holdweights = np.load(os.path.join(resultdir, 'network_weights_vsize_' + loadresults +'LR.npy'))
        lasagne.layers.set_all_param_values(network, holdweights)
        val_err = 0
        val_batches = 0
        MSerrsummed = 0
        predshold = []
        for batch in iterate_minibatches(X_test, y_test, batchsize, shuffle=False):
            inputs, targets = batch
            err = val_fn(inputs, targets)
            MSerr = MSE_fn(inputs, targets)
            MSerrsummed += MSerr
            val_err += err
            val_batches += 1

            predstest = pred_fn(inputs)
            predshold.append(predstest)
       # Check if validation error is lower than that of previous iteration
        predshold = np.concatenate(predshold)

    # After training, we compute and print the test error:
    epochhold[holdepoch] *= -1
    lasagne.layers.set_all_param_values(network, holdweights)
    test_err = 0
    test_batches = 0
    meandif = 0
    res = np.array([[np.nan], [np.nan]])
    global targetstesthold, predstesthold
    targetstesthold = []
    predstesthold = []
    MSerrsummed = 0
    for batch in iterate_minibatches(X_test, y_test, batchsize, shuffle=False):
        inputs, targets = batch
        err = val_fn(inputs, targets)
        test_err += err
        test_batches += 1
        predstest = pred_fn(inputs)
        res = np.append(res, np.squeeze(np.array([[predstest], [targets[..., None]]])), axis=1)
        meandif += np.mean(np.abs(predstest - targets[..., None]))
        targetstest = targets
        targetstesthold.append(targetstest)
        predstesthold.append(predstest)
        MSerrsummed += MSE_fn(inputs, targets)

    print("Results network "+str(networkno)+':')
    print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
    print("  mean difference thickness:\t\t{:.6f} mm".format(
        meandif / test_batches))
    print("  Mean squared error:\t{:.6f} mm".format(
        MSerrsummed / test_batches))
    predstesthold = np.concatenate(predstesthold)
    holdtestpredictions = np.zeros([y_test.size, 1])
    holdtestpredictions[0:predstesthold.size] = predstesthold
    targetstesthold = np.concatenate(targetstesthold)
    np.save(os.path.join(resultdir, 'results_vsize_' + str(targetvoxelsize) + '_acq_' + str(networkno) + '_' + str(curtime) + '_invivo.npy'), np.concatenate((predstesthold, targetstesthold[..., None]), axis=1))
    testsetappended = np.concatenate((testset, holdtestpredictions), axis=1)
    np.save(os.path.join(resultdir, 'results_vsize_' + str(targetvoxelsize)+'_acq_' + str(networkno)+ '_' + str(curtime) + '_testsetresults_invivo.npy'), testsetappended)


def align_targets(predictions, targets):
    """Helper function turning a target 1D vector into a column if needed.
    This way, combining a network of a single output unit with a target vector
    works as expected by most users, not broadcasting outputs against targets.
    Parameters
    ----------
    predictions : Theano tensor
        Expression for the predictions of a neural network.
    targets : Theano tensor
        Expression or variable for corresponding targets.
    Returns
    -------
    predictions : Theano tensor
        The predictions unchanged.
    targets : Theano tensor
        If `predictions` is a column vector and `targets` is a 1D vector,
        returns `targets` turned into a column vector. Otherwise, returns
        `targets` unchanged.
    """
    if (getattr(predictions, 'broadcastable', None) == (False, True) and
            getattr(targets, 'ndim', None) == 1):
        targets = as_theano_expression(targets).dimshuffle(0, 'x')
    return predictions, targets

def weighted_squared_error(a, b):
    """Computes the element-wise squared difference between two tensors.
    .. math:: L = (p - t)^2
    Parameters
    ----------
    a, b : Theano tensor
        The tensors to compute the squared difference between.
    Returns
    -------
    Theano tensor
        An expression for the element-wise squared difference.
    Notes
    -----
    This is the loss function of choice for many regression problems
    or auto-encoders with linear output units.
    """
    global alpha, beta
    a, b = align_targets(a, b)
    newb = T.where(b > alpha, beta, 1)
    return theano.tensor.tensordot(theano.tensor.square(a - b), newb, axes=0)

def weighted_absolute_error(a, b):
    """Computes the element-wise squared difference between two tensors.
    .. math:: L = (p - t)^2
    Parameters
    ----------
    a, b : Theano tensor
        The tensors to compute the squared difference between.
    Returns
    -------
    Theano tensor
        An expression for the element-wise squared difference.
    Notes
    -----
    This is the loss function of choice for many regression problems
    or auto-encoders with linear output units.
    """
    global alpha, beta
    a, b = align_targets(a, b)
    newb = T.where(b > alpha, beta, 1)
    return theano.tensor.tensordot(theano.tensor.abs_(a - b), newb, axes=0)

def mean_absolute_error(a, b):
    """Computes the element-wise squared difference between two tensors.
    .. math:: L = (p - t)^2
    Parameters
    ----------
    a, b : Theano tensor
        The tensors to compute the squared difference between.
    Returns
    -------
    Theano tensor
        An expression for the element-wise squared difference.
    Notes
    -----
    This is the loss function of choice for many regression problems
    or auto-encoders with linear output units.
    """
    a, b = align_targets(a, b)
    return theano.tensor.abs_(a - b)

def mean_squared_error(a, b):
    """Computes the element-wise absolute error.
    .. math:: L = (p - t)^2
    Parameters
    ----------
    a, b : Theano tensor
        The tensors to compute the squared difference between.
    Returns
    -------
    Theano tensor
        An expression for the element-wise squared difference.
    Notes
    -----
    This is the loss function of choice for many regression problems
    or auto-encoders with linear output units.
    """
    a, b = align_targets(a, b)
    return theano.tensor.square(a - b)

def regression_slope(a, b):
    """Computes the element-wise absolute error.
    .. math:: L = (p - t)^2
    Parameters
    ----------
    a, b : Theano tensor
        x and y
    Returns
    -------
    Theano tensor
        An expression for the element-wise squared difference.
    Notes
    -----
    This is the loss function of choice for many regression problems
    or auto-encoders with linear output units.
    """
    a, b = align_targets(a, b)

    a_centered, b_centered = a - a.mean(), b - b.mean()

    return abs(1 - a_centered.dot(b_centered) / T.sum(T.square(a_centered)))[0]

def iterate_minibatches(inputs, targets, batchsze, shuffle=False):
    """ This is just a simple helper function iterating over training data in
    mini-batches of a particular size, optionally in random order. It assumes
    data is available as numpy arrays. For big datasets, you could load numpy
    arrays as memory-mapped files (np.load(..., mmap_mode='r')), or write your
    own custom data iteration function. For small datasets, you can also copy
    them to GPU at once for slightly improved performance. This would involve
    several changes in the main program, though, and is not demonstrated here.
    Notice that this function returns only mini-batches of size `batchsize`.
    If the size of the data is not a multiple of `batchsize`, it will not
    return the last (remaining) mini-batch.
    """
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsze + 1, batchsze):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsze]
        else:
            excerpt = slice(start_idx, start_idx + batchsze)
        yield inputs[excerpt], targets[excerpt]

def build_mlp(input_var=None):
    """ Builds the neural network model. Returns the final layer output of the network"""
    if 1:
        # Input layer, specifying the expected input shape of the network
        network = lasagne.layers.InputLayer(shape=(None, 1, patchsz, patchsz, patchsz),
                                            input_var=input_var)
        shortcut1 = network

        #first parallel layers
        # Convolution and batchnorm
        network = lasagne.layers.Conv3DLayer(network, 16, 7, 1, nonlinearity=lasagne.nonlinearities.linear)
        network = lasagne.layers.batch_norm(lasagne.layers.prelu(network))
        network = lasagne.layers.DropoutLayer(network, p=0.0)

        # Convolution and batchnorm
        network = lasagne.layers.Conv3DLayer(network, 32, 5, 1, nonlinearity=lasagne.nonlinearities.linear)
        network = lasagne.layers.batch_norm(lasagne.layers.prelu(network))
        network = lasagne.layers.DropoutLayer(network, p=0.0)

        # Convolution and batchnorm
        network = lasagne.layers.Conv3DLayer(network, 64, 3, 1, nonlinearity=lasagne.nonlinearities.linear)
        network = lasagne.layers.batch_norm(lasagne.layers.prelu(network))
        network = lasagne.layers.DropoutLayer(network, p=0.0)

        # Convolution and batchnorm
        network = lasagne.layers.Conv3DLayer(network, 128, 3, 1, nonlinearity=lasagne.nonlinearities.linear)
        network = lasagne.layers.batch_norm(lasagne.layers.prelu(network))
        network = lasagne.layers.DropoutLayer(network, p=0.0)

        #Finishing up with a fully connected layer.
        network = lasagne.layers.DenseLayer(network, num_units=300,
                                            nonlinearity=lasagne.nonlinearities.linear,
                                            W=lasagne.init.GlorotUniform())
        network = lasagne.layers.prelu(network)
        network = lasagne.layers.DropoutLayer(network, p=0.5)

        #second parallel layers
        # Convolution and batchnorm
        network2 = lasagne.layers.Conv3DLayer(shortcut1, 16, 5, 1, nonlinearity=lasagne.nonlinearities.linear)
        network2 = lasagne.layers.batch_norm(lasagne.layers.prelu(network2))
        network2 = lasagne.layers.DropoutLayer(network2, p=0.0)

        # Convolution and batchnorm
        network2 = lasagne.layers.Conv3DLayer(network2, 32, 5, 1, nonlinearity=lasagne.nonlinearities.linear)
        network2 = lasagne.layers.batch_norm(lasagne.layers.prelu(network2))
        network2 = lasagne.layers.DropoutLayer(network2, p=0.0)

        # Convolution and batchnorm
        network2 = lasagne.layers.Conv3DLayer(network2, 64, 5, 1, nonlinearity=lasagne.nonlinearities.linear)
        network2 = lasagne.layers.batch_norm(lasagne.layers.prelu(network2))
        network2 = lasagne.layers.DropoutLayer(network2, p=0.0)

        # Convolution and batchnorm
        network2 = lasagne.layers.Conv3DLayer(network2, 64, 5, 1, nonlinearity=lasagne.nonlinearities.linear)
        network2 = lasagne.layers.batch_norm(lasagne.layers.prelu(network2))
        network2 = lasagne.layers.DropoutLayer(network2, p=0.0)

        # Convolution and batchnorm
        network2 = lasagne.layers.Conv3DLayer(network2, 128, 3, 1, nonlinearity=lasagne.nonlinearities.linear)
        network2 = lasagne.layers.batch_norm(lasagne.layers.prelu(network2))
        network2 = lasagne.layers.DropoutLayer(network2, p=0.0)

        #Finishing up with a fully connected layer.
        network2 = lasagne.layers.DenseLayer(network2, num_units=300,
                                             nonlinearity=lasagne.nonlinearities.linear,
                                             W=lasagne.init.GlorotUniform())
        network2 = lasagne.layers.prelu(network2)
        network2 = lasagne.layers.DropoutLayer(network2, p=0.5)

        #Finishing up with a fully connected layer.
        network4 = lasagne.layers.ConcatLayer([network, network2])
        network4 = lasagne.layers.DenseLayer(network4, num_units=300,
                                             nonlinearity=lasagne.nonlinearities.linear,
                                             W=lasagne.init.GlorotUniform())
        network4 = lasagne.layers.prelu(network4)
        network4 = lasagne.layers.DropoutLayer(network4, p=0.5)

        # Each layer is linked to its incoming layer(s), so we only need to pass
        # the output layer to give access to a network in Lasagne:
        network4 = lasagne.layers.DenseLayer(
            network4, num_units=1, nonlinearity=lasagne.nonlinearities.linear,
            W=lasagne.init.GlorotUniform())
    return network4

if __name__ == '__main__':
    main()
    