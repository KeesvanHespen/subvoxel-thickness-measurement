#!/usr/bin/env python

"""

"""

from __future__ import print_function

import datetime
import sys
import os
import random
import time

import dicom
import numpy as np
import theano
import theano.tensor as T
import SimpleITK
import scipy
import scipy.io as sio
from sklearn.preprocessing import normalize
import lasagne
import pickle
import matplotlib.pyplot as plt

from construct_network import construct_network
from line_profiler import LineProfiler
global ROIdcmname

ROIresultsname = [
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S005/results_ROI_0042-S005_FP020_PF015_selection',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S007/results_ROI_0042R-S007_FP020_PF015_redo_selection',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S001/results_ROI_0030-S001_2_FP020_PF015_selection',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S004/results_ROI_0043-S004_FP020_PF015_selection',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S006/results_ROI_0037-S006_FP020_PF015_selection',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S011/results_ROI_0037-S011_v5_FP020_PF015_selection',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160324_S012_S010/ROI-S010/results_ROI_0042R-S010_FP020_PF015_selection',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S002/results_ROI_0049-S002_FP020_PF015_selection',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S003/results_ROI_0049R-S003_FP020_PF015_selection',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S008/results_ROI_0031R-S008_FP020_PF015_selection',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S009/results_ROI_0031-S009_FP020_PF015_selection'
              ]
lowresROIdcmname = [
             "C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S005/ROI_0038-S005.dcm",
             "C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S007/ROI_0038R-S007.dcm",
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S001/ROI_0026-S001.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S004/ROI_0039-S004.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S006/ROI_0033-S006.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S011/ROI_0033-S011.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160324_S012_S010/ROI-S010/ROI_0038R-S010.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S002/ROI_0045-S002.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S003/ROI_0045R-S003.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S008/ROI_0027R-S008.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S009/ROI_0027-S009.dcm']

maskname = [
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S005/MCA_R_mask_ROI_0042-S005_2',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S007/MCA_mask_ROI_0042R-S007_2',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S001/MCA_R_mask_ROI_0030-S001_2',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S004/MCA_R_mask_ROI_0043-S004_2',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S006/MCA_mask_ROI_0037-S006_2',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S011/MCA_mask_ROI_0037-S011',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160324_S012_S010/ROI-S010/MCA_R_mask_ROI_0042R-S010_2'
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/3. Data/20160324_S012_S010/ROI-S012/MCA_mask_ROI_0042-S012',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S002/MCA_R_mask_ROI_0049-S002',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S003/MCA_mask_ROI_0049R-S003',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S008/MCA_R_mask_ROI_0031R-S008',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S009/MCA_mask_ROI_0031-S009'
         ]

ROIdcmname = [
             "C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S005/ROI_0042-S005.dcm",
             "C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S007/ROI_0042R-S007.dcm",
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S001/ROI_0030-S001_2.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S004/ROI_0043-S004.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S006/ROI_0037-S006.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S011/ROI_0037-S011.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160324_S012_S010/ROI-S010/ROI_0042R-S010.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S002/ROI_0049-S002.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S003/ROI_0049R-S003.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S008/ROI_0031R-S008.dcm',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S009/ROI_0031-S009.dcm']
    
FULLdcmname = [
             "C:/Users/khespen/Documents/1. Vessel wall thickness measurements/4. Data backup/20160307_S007-S005/DICOM/DICOM/IM_0038",
             "C:/Users/khespen/Documents/1. Vessel wall thickness measurements/4. Data backup/20160307_S007-S005/DICOM/DICOM/IM_0038",
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/4. Data backup/20160619_S001-S004/DICOM/DICOM/IM_0026',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/4. Data backup/20160619_S001-S004/DICOM/DICOM/IM_0026',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/4. Data backup/20160222_S011-S006/DICOM/DICOM/IM_0033',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/4. Data backup/20160222_S011-S006/DICOM/DICOM/IM_0033',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/4. Data backup/20160324_S012-S010/DICOM/DICOM/IM_0038',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/4. Data backup/20160618_S002-S003/DICOM/DICOM/IM_0045',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/4. Data backup/20160618_S002-S003/DICOM/DICOM/IM_0045',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/DICOM/DICOM/IM_0027',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/DICOM/DICOM/IM_0027',
  ]

datadir = 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/2. Data/'
resultdir = 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/2. Data/'
loadresults = ''
global nrofsample, targetvoxelsize, sampledata, patchsz, excludelargerthan, selectionrange, num_epochs, optionsdict, lr, blurstrength, epochdif, excludesmallerthan, usemagormod
    
nrofsample = 200000                 # nr of sample patches taken
targetvoxelsize = 0.800246          # target voxelsize of downsampled image
sampledata = 0                        # if 1, samples are sampled regardless of existence of .p file
patchsz = 11                         # size of the patches in pixels
excludelargerthan = 1.5               # exclude voxels with thickness larger/smaller than excludelargerthan/excludesmallerthan. 
excludesmallerthan = 0.2                # To turn off, set it at np.inf/0 respectively.
num_epochs = 300                     # max number of epochs
selectionrange = np.array([0.3,0.4])  # Select thickness range voxels for analysis
lr = 0.001                       # learning rate
blurstrength = 0.5                    # blur strength of Gaussian, used when sampling
epochdif = 20
usemagormod = 1                 # if 0, use magnitude, if 1, use modulus

optionsdict = {'nrofsample':nrofsample,'targetvoxelsize':targetvoxelsize,'sampledata':sampledata,'patchsz':patchsz,'excludesmallerthan':excludesmallerthan,'excludelargerthan':excludelargerthan,'num_epochs':num_epochs,'selectionrange':selectionrange,'learningrate':lr, 'blurstrength':blurstrength, 'scanorder':ROIdcmname, 'usemagormod':usemagormod}

def do_profile(follow=[]):
    def inner(func):
        def profiled_func(*args, **kwargs):
            try:
                profiler = LineProfiler()
                profiler.add_function(func)
                for f in follow:
                    profiler.add_function(f)
                profiler.enable_by_count()
                return func(*args, **kwargs)
            finally:
                profiler.print_stats()
        return profiled_func
    return inner

def get_number():
    for x in range(5000000):
        yield x
        
def gettransformmatrix(info):
    """
    ########################################
    #                                      #
    # compute world matrix from dicom info #
    #      works for 2D and 3D data.       #
    #                                      #
    ########################################
    INPUT: info:  dicom info dict
    OUTPUT: Wtransmat: World transformation matrix
    """

    try:
        T1 = np.array(info.ImagePositionPatient)
    except:
        T1 = np.array(info.PerFrameFunctionalGroupsSequence[0].PlanePositionSequence[0].ImagePositionPatient)      
    T1 = T1[...,np.newaxis]
    try:
        pixelspacing = info.PixelSpacing
    except:
        pixelspacing = info.PerFrameFunctionalGroupsSequence[0].PixelMeasuresSequence[0].PixelSpacing
    dx = pixelspacing[0]
    dX = np.array([1, 1, 1]) * dx
    dy = pixelspacing[1]
    dY = np.array([1, 1, 1]) * dy
    dz = info.SpacingBetweenSlices
    dZ = np.array([1, 1, 1]) * dz
    try:
        Orientation = info.ImageOrientationPatient
    except:
        Orientation = info.PerFrameFunctionalGroupsSequence[0].PlaneOrientationSequence[0].ImageOrientationPatient
        
    dircosX = np.array(Orientation[0:3])
    dircosY = np.array(Orientation[3:6])
    dircosZ = np.cross(dircosX,dircosY)


    #all dircos together
    dimensionmixing = np.transpose(np.array([dircosX, dircosY, dircosZ]))
    
    #all spacing together
    dimensionscaling = np.transpose(np.array([dX, dY, dZ]))
    
    #mixing and spacing of dimensions together
    R = dimensionmixing * dimensionscaling
    
    lastvec = np.array([0, 0, 0, 1])
    lastvec = lastvec[None, ...]
    Wtransmat = np.concatenate((R, T1), 1)
    Wtransmat = np.concatenate((Wtransmat, lastvec))
    

    return Wtransmat

#@do_profile(follow=[get_number])   
def samplingfunction(thicknessmap, scannamelist, nrofsamples, patchsize, roi_to_full_wmat, angle='', seed=datetime.datetime.now()):
    """
    ########################################
    #                                      #
    #   Samples patches equally over the   #
    #    range of possible thicknesses     #
    #                                      #
    ########################################
    INPUT: thicknessmap:     can be a list of nD thickness maps. If input is a numpy array, sampling is equalized accross the range of thickness values given in.
                             If a numpy array is given, first three columns represent the x,y,z coordinates. The fourth column is the thickness values, the fifth is the number corresponding to the scan number
           scannamelist:     A list containing the locations of all scans included in thicknessmap
           nrofsamples:      number of samples that need to be sampled
           target_voxelsize: Target voxelsize. If int, is assumed to be isotropic in all directions
           source_voxelsize: Source voxelsize. If int, is assumed to be isotropic in all directions
           patchsize:        patchsize. If int, patchsize is assumed same in all directions. Value should be uneven!
           roi_to_full_wmat: World coordinate transformation matrix, from roi coordinates (thicknessmap) to full image coordinates (scannamelist). Only applicable if the thickness maps are ROIs within the images from scannamelist. Give indentity matrix if both images are in same space.
           angle:            Sampling angle. If not given, samples are sampled with random angle
           seed:             optional seed for angle generation
           
    OUTPUT:samples:         Numpy array containing all samples
           labels:          Numpy array containing all labels
           
    """
    # Set variables   
    equalize = not isinstance(thicknessmap, list)
    multimaps = len(thicknessmap)>1
    random.seed(seed)
    target_voxelsize = np.array([1])
    source_voxelsize = np.array([1])
    patchsize = np.array(patchsize)
    patches = []
    imgsize = []

    for scan in range(len(scannamelist)):
        readimage = dicom.read_file(scannamelist[scan]).pixel_array.shape
        imgsize.append(np.array([readimage[1], readimage[2], readimage[0]]))
        readimage = None
    if target_voxelsize.size == 1:
        target_voxelsize = np.array([target_voxelsize, target_voxelsize, target_voxelsize])
    if source_voxelsize.size == 1:
        source_voxelsize = np.array([source_voxelsize, source_voxelsize, source_voxelsize])
    if patchsize.size == 1:
        patchsize = np.array([patchsize, patchsize, patchsize])
        
    #generate samples from all non zero elements in the HR image
    if equalize:
        scancoords = thicknessmap[..., 1:4][thicknessmap[..., 4] == 0]

        large_image_coords =  [np.round(np.dot(roi_to_full_wmat[0], np.concatenate((np.array(scancoords.T), np.ones((1, np.array(scancoords.T).shape[1]))))))[:3, ...]]

        small_image_coords = [thicknessmap[..., 1:4].T]
        for index in range(1, np.max(thicknessmap[..., 4]).astype(int) + 1):
            scancoords = thicknessmap[..., 1:4][thicknessmap[...,4] == index]
            large_image_coords.append(np.round(np.dot(roi_to_full_wmat[index], np.concatenate((np.array(scancoords.T), np.ones((1, np.array(scancoords.T).shape[1]))))))[:3, ...])
        large_image_coords = np.hstack(large_image_coords).astype('int16')
    else:
        random.seed(seed)
        if multimaps:
            large_image_coords = [np.round(np.dot(roi_to_full_wmat[0], np.concatenate((np.array(np.nonzero(thicknessmap[0])), np.ones((1, np.array(np.nonzero(thicknessmap[0])).shape[1]))))))[:3, ...].astype('int16')]
            small_image_coords = [np.array(np.nonzero(thicknessmap[0]))]
            weights = np.array(np.nonzero(thicknessmap[0])).shape[1]
            index=0
            for lst in thicknessmap[1:]:
                index += 1
                large_image_coords.append(np.round(np.dot(roi_to_full_wmat[index], np.concatenate((np.nonzero(lst), np.ones((1, np.array(np.nonzero(lst)).shape[1]))))))[:3, ...].astype('int16'))
                small_image_coords.append(np.array(np.nonzero(lst)))
                weights = np.append(weights,np.array(np.nonzero(lst)).shape[1])
            weights = normalize(weights.reshape(-1,1),norm ='l1',axis=0)
            
        else:
            large_image_coords =  [np.round(np.dot(roi_to_full_wmat[0], np.concatenate((np.array(np.nonzero(thicknessmap[0])), np.ones((1, np.array(np.nonzero(thicknessmap[0])).shape[1]))))))[:3,...]]
            small_image_coords = [np.array(np.nonzero(thicknessmap[0]))]

    
    #get thickness values as labels from the selected samples
    labels = np.zeros((nrofsamples, 1))
    patch = np.zeros((nrofsamples, patchsize[0], patchsize[1], patchsize[2]),dtype='float32')

    patchorigin = []
    patchoriginindex = []
    global samples2
    samples = 0
    totaltries = 0
    global binsav
    binsav = np.zeros((nrofsamples, 1))
    bins = 11
    binsize=(np.max(thicknessmap[:, 0]) - np.min(thicknessmap[:, 0])) / bins
    patchcoordszerocentered = np.zeros([4,patchsize[0],patchsize[1],patchsize[2]],dtype='float32')
    print('Sampling patches...')
    while samples < nrofsamples:
        samples2=samples
        try:
            if equalize:
                usebin = random.randint(0, bins - 1)
                binsav[samples]=usebin
                randomthvalue = np.random.choice(thicknessmap[:, 0][np.logical_and(thicknessmap[:, 0] >= (binsize*usebin+np.min(thicknessmap[:, 0])), thicknessmap[:, 0] <= (binsize*(usebin + 1) + np.min(thicknessmap[:, 0])))])
                randomsampleindices = np.squeeze(np.where(thicknessmap[:, 0]==(randomthvalue)))
                if randomsampleindices.size>1:
                    randomsampleindices = np.random.choice(randomsampleindices)
                    
                samplecoords = np.squeeze(large_image_coords[..., randomsampleindices].T)
                usemap = thicknessmap[randomsampleindices, 4].astype(int)
            else:#todo: fix binning. fix getting values
                if multimaps:
                    usemap = np.random.choice(np.arange(len(large_image_coords)),p = weights[:, 0])
                else: 
                    usemap = 0
                    
                randomsampleindices = np.random.choice(np.array(large_image_coords[usemap]).shape[1], 1)
                samplecoords = large_image_coords[usemap][..., randomsampleindices[0]].astype(int)
            sampletry = 0
        except Exception:
            print('somehow got here. Not good. Probably rerun')
            break
            
        totaltries += 1
        if totaltries > nrofsamples * 10:
           print(len(patches))
           raise Exception('Number of sampled data points exceeds 20 times the nr of requested points. Probably you need to decrease the patch size') 
        # test sample x times with a random orientation. If voxels from the patch fall outside the image we eventually discard the voxel.
        while sampletry < 10:
            if angle == '':
                sampleangle = np.array([random.randrange(0, 360), random.randrange(0, 360), random.randrange(0, 360)])
            else:
                sampleangle = angle
        
            x = np.linspace(-target_voxelsize[0] / source_voxelsize[0] * (patchsize[0] / 2 - 0.5) + samplecoords[0], target_voxelsize[0] / source_voxelsize[0] * (patchsize[0] / 2 - 0.5) + samplecoords[0], patchsize[0])
            y = np.linspace(-target_voxelsize[1] / source_voxelsize[1] * (patchsize[1] / 2 - 0.5) + samplecoords[1], target_voxelsize[1] / source_voxelsize[1] * (patchsize[1] / 2 - 0.5) + samplecoords[1], patchsize[1])
            z = np.linspace(-target_voxelsize[2] / source_voxelsize[2] * (patchsize[2] / 2 - 0.5) + samplecoords[2], target_voxelsize[2] / source_voxelsize[2] * (patchsize[2] / 2 - 0.5) + samplecoords[2], patchsize[2])
            
            patchcoords = np.array(np.meshgrid(x, y, z))
            
            patchcoordszerocentered[0:3,...] = patchcoords - samplecoords[..., None, None, None]
            
            patchcoordsrotatedzerocentered = np.dot(patchcoordszerocentered.T, rotation_matrix(1, sampleangle)).T
            patchcoordsrotated = patchcoordsrotatedzerocentered[0:3, ...] + samplecoords[..., None, None, None].astype(int)

            sampletry += 1
            # save patch coordinates that fall into image
            if not any(singlecoord < 0 for singlecoord in np.array([np.min(patchcoordsrotated[0, ...]), np.min(patchcoordsrotated[1, ...]), np.min(patchcoordsrotated[2, ...])])) and not any(np.array([np.max(patchcoordsrotated[0, ...]), np.max(patchcoordsrotated[1, ...]), np.max(patchcoordsrotated[2, ...])]) - imgsize[usemap] > 0):
                patches.append(patchcoordsrotated.astype('float32'))
                patchorigin.append(usemap)
                sampletry = np.inf;
                samples += 1
                patchoriginindex.append(randomsampleindices.astype(int))
        if sampletry==10:
            print('Sample could not be sampled')
        # delete coordinate that fails to fit a patch (doesnt update the weights. should do that)        
        if not equalize:
            if not sampletry == np.inf and sampletry == 50:
                 large_image_coords[usemap] = np.delete(large_image_coords[usemap], randomsampleindices[0], 1)
            if len(large_image_coords[usemap].T) == 0:
                 weights[usemap] = 0
                 normalize(weights.reshape(-1, 1), norm = 'l1', axis = 0)

    patches = np.array(patches)
    # save original center coords and scannr
    selectedoriginalcoords = thicknessmap[np.array(patchoriginindex)].astype('float32')
    np.save(os.path.join(resultdir, 'samples' + str(targetvoxelsize) + '_origcoords.npy'), selectedoriginalcoords)
   
    # get thickness label and patch intensity values
    print('Interpolating patch intensity values...')    
    for scan in range(len(scannamelist)):
         print('Loading scan ' + str(scan + 1) + ' out of ' + str(len(scannamelist)))      
         selectedpatches = np.transpose(np.array(np.where(np.array(patchorigin) == scan)))
         if 0:
             selectedthicknessmap = thicknessmap[thicknessmap[...,4]==scan,...]
             selectedthicknessmap[...,1:4]=np.round(np.dot(roi_to_full_wmat[scan], np.concatenate((np.array(selectedthicknessmap[...,1:4].T), np.ones((1, np.array(selectedthicknessmap[...,1:4].T).shape[1]))))))[:3, ...].T
             holdslice=np.inf
             firstorend=0
         if selectedpatches.size:
             curlowresimage = dicom.read_file(scannamelist[scan])
             # Fix the rescaleintercept and slope, by setting the first image rescale intercept and slope (magnitude image) to that of the modulus image. Doing this will yield the correct pixel values when ITK loads the image further down.
             curlowresimage.PerFrameFunctionalGroupsSequence[0].PixelValueTransformationSequence[0].RescaleSlope = curlowresimage.PerFrameFunctionalGroupsSequence[np.round(curlowresimage.pixel_array.shape[2]/2).astype('int')].PixelValueTransformationSequence[0].RescaleSlope
             curlowresimage.PerFrameFunctionalGroupsSequence[0].PixelValueTransformationSequence[0].RescaleIntercept = curlowresimage.PerFrameFunctionalGroupsSequence[np.round(curlowresimage.pixel_array.shape[2]/2).astype('int')].PixelValueTransformationSequence[0].RescaleIntercept
             pixdat = curlowresimage.PerFrameFunctionalGroupsSequence[np.round(curlowresimage.pixel_array.shape[2]/2).astype('int')].PixelValueTransformationSequence[0].RescaleSlope * scipy.ndimage.filters.gaussian_filter(np.transpose((curlowresimage.pixel_array[100:200,...]).astype('int'), (2, 1, 0)), blurstrength * target_voxelsize / source_voxelsize*0.001) + curlowresimage.PerFrameFunctionalGroupsSequence[np.round(curlowresimage.pixel_array.shape[2]/2).astype('int')].PixelValueTransformationSequence[0].RescaleIntercept
             
             curmed = np.zeros([100,1])
             for sliceno in range(0,pixdat.shape[2]):
                 curslicedat = pixdat[...,sliceno]
                 curmed[sliceno] = np.median(curslicedat[np.where(curslicedat !=curslicedat[0,0])])
             grad = np.diff(curmed,axis=0)
             [gradind,unused] = np.array(np.where(np.abs(grad)>np.std(curmed[~np.isnan(curmed)])))
             curind = 0
             if gradind:
                 for indices in gradind:
                     if grad[indices]<0:
                         pixdat[...,curind:indices+1]*=-1
                         curind = indices
                     else:
                        curind = indices
                        if np.max(gradind)==indices:
                             pixdat[...,indices+1:]*=-1 
                        else: 
                             pixdat[...,indices+1:]*=-1 
             curimage = pixdat.astype('float32')
             pixdat = (pixdat - curlowresimage.PerFrameFunctionalGroupsSequence[np.round(curlowresimage.pixel_array.shape[2]/2).astype('int')].PixelValueTransformationSequence[0].RescaleIntercept)/curlowresimage.PerFrameFunctionalGroupsSequence[np.round(curlowresimage.pixel_array.shape[2]/2).astype('int')].PixelValueTransformationSequence[0].RescaleSlope
                
             curlowresimage.pixel_array[100:200,...] = np.transpose(pixdat).astype('uint16')  
             curlowresimage.PixelData = curlowresimage.pixel_array.tostring()
             curlowresimage.save_as(os.path.join(datadir,'temp.dcm'))
             pixdat=None
             curlowresimage=None
             # CARE: LINE BELOW, ONLY IF THE PHASE IS HALF WAY IN THE CURLOWRESIMAGE.PIXEL_ARRAY!!!!!!
             if usemagormod:
                 if 0:
                     curimage = curlowresimage.PerFrameFunctionalGroupsSequence[np.round(curlowresimage.pixel_array.shape[2]/2).astype('int')].PixelValueTransformationSequence[0].RescaleSlope * scipy.ndimage.filters.gaussian_filter(np.transpose((curlowresimage.pixel_array[100:200,...]).astype('int'), (2, 1, 0)), blurstrength * target_voxelsize / source_voxelsize*0.001) + curlowresimage.PerFrameFunctionalGroupsSequence[np.round(curlowresimage.pixel_array.shape[2]/2).astype('int')].PixelValueTransformationSequence[0].RescaleIntercept
                     for sliceno in range(0,curimage.shape[2]):
                         selectedskeletonvoxels = selectedthicknessmap[selectedthicknessmap[...,3]==sliceno,1:4]
                         
                         currentvalues = curimage[selectedskeletonvoxels[...,0].astype('int'),selectedskeletonvoxels[...,1].astype('int'),selectedskeletonvoxels[...,2].astype('int')]
                         if np.count_nonzero(currentvalues<0)>0.5*currentvalues.size:
                             if sliceno==min(selectedthicknessmap[...,3]):
                                 firstorend+=1
                             if sliceno==max(selectedthicknessmap[...,3]):
                                 firstorend+=2
                             holdslice = sliceno   
                     if not holdslice==np.inf and firstorend==1:
                         curimage[...,0:min([curimage.shape[2]-1,holdslice+8])]*=-1
                     if not holdslice==np.inf and firstorend==2:
                         curimage[...,max([0,holdslice-8]):]*=-1        
                     if not holdslice==np.inf and firstorend==3:
                         curimage[...,max([0,min(selectedthicknessmap[...,3]).astype('int')-8]):min([curimage.shape[2]-1,max(selectedthicknessmap[...,3]).astype('int')+8])]*=-1                 

                 reader=SimpleITK.ImageFileReader()
                 reader.SetFileName(os.path.join(datadir,'temp.dcm'))

                 itk_image = reader.Execute()
                 #remove temporary dicom
                 #os.remove(os.path.join(datadir,'temp.dcm'))
                 #crop dicom to modulus
                 cropp = SimpleITK.CropImageFilter()
                 cropp.SetUpperBoundaryCropSize([0,0,100])
                 cropp.SetLowerBoundaryCropSize([0,0,100])
                 croppedimg = cropp.Execute(itk_image)
                 #apply otsu
                 filterotsu = SimpleITK.OtsuMultipleThresholdsImageFilter()
                 filterotsu.SetNumberOfThresholds(3)
                 filterotsu.Execute(croppedimg)
                 imagethresholds = filterotsu.GetThresholds()
                 #clean some stuff
                 itk_image=None
                 croppedimg=None
                 reader=None
                 cropp=None
                 
                 selcurimage = curimage[curimage<imagethresholds[0]]
                 meanselcurimg = np.mean(selcurimage)
                 stdselcurimg = np.std(selcurimage)
                 curimage[curimage<imagethresholds[0]]=imagethresholds[0]
                 selcurimage=None
                 os.remove(os.path.join(datadir,'temp.dcm'))
             else:
                 curimage = curlowresimage.PerFrameFunctionalGroupsSequence[np.round(curlowresimage.pixel_array.shape[2]/4).astype('int')].PixelValueTransformationSequence[0].RescaleSlope * scipy.ndimage.filters.gaussian_filter(np.transpose((curlowresimage.pixel_array[0:100,...]).astype('int'), (2, 1, 0)), blurstrength * target_voxelsize / source_voxelsize*0.001) + curlowresimage.PerFrameFunctionalGroupsSequence[np.round(curlowresimage.pixel_array.shape[2]/4).astype('int')].PixelValueTransformationSequence[0].RescaleIntercept
       
             plt.figure()
             plt.imshow(curimage[...,50])
             selectedpatchescoords = np.squeeze(patches[selectedpatches])
             print('Interpolating image values at sampled locations for scan ' + str(scan + 1) + ' out of ' + str(len(scannamelist)))
             patch[selectedpatches[:, 0]] = scipy.ndimage.interpolation.map_coordinates(curimage,
                                                                            np.array([np.array(selectedpatchescoords)[:, 0, ...], np.array(selectedpatchescoords)[:, 1, ...], np.array(selectedpatchescoords)[:, 2, ...]]), order=3).astype('float32')

         curimage=None
         if equalize:
             labels = thicknessmap[:, 0][patchoriginindex]
         else:    
             labels[selectedpatches[:, 0]] = thicknessmap[scan][small_image_coords[scan][0, patchoriginindex[selectedpatches]], small_image_coords[scan][1, patchoriginindex[selectedpatches]], small_image_coords[scan][2, patchoriginindex[selectedpatches]]]
                
    print('Sampling complete!')       
    return patch, np.array(labels)

def rotation_matrix(axis, theta):
    """np.array(patches[1:100])[:,2,...].shape
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    theta = np.array(theta) / 360 * np.pi
    if len(np.array(theta)) == 1:
        theta = np.array([theta, theta, theta])
        
    rotationmatrix_x = np.array([[np.cos(theta[0]), -np.sin(theta[0]), 0, 0], [np.sin(theta[0]), np.cos(theta[0]), 0, 0],[0, 0, 1, 0], [0, 0, 0, 1]])
    rotationmatrix_y = np.array([[1, 0, 0, 0], [0, np.cos(theta[1]), -np.sin(theta[1]), 0], [0, np.sin(theta[1]), np.cos(theta[1]), 0], [0, 0, 0, 1]])
    rotationmatrix_z = np.array([[np.cos(theta[2]), 0, np.sin(theta[2]), 0], [0, 1, 0, 0], [-np.sin(theta[2]), 0, np.cos(theta[2]), 0], [0, 0, 0, 1]])
    
    rotationmatrix = np.dot(np.dot(rotationmatrix_x, rotationmatrix_y), rotationmatrix_z)
    return np.array(rotationmatrix)


def load_dataset():
    # We first define a download function, supporting both Python 2 and 3.    
    thicknessmap = []
    thicknessmaplist = []
    roi_to_full_wmat = []
    tic = time.clock();
    biglist = []
    global patchsz
    #read thickness maps and store them in a list
    if sampledata or not os.path.isfile(os.path.join(datadir, 'samples' + str(targetvoxelsize) + '.p')):
        for scan in range(len(maskname)):
            
            resultshr = sio.loadmat(ROIresultsname[scan])
            thicknessmap.append(np.transpose(resultshr['results']['thicknessmapselection'][0][0], (1, 0, 2)))
        
            #resultslr = sio.loadmat(matnamelr)
    
            mask = sio.loadmat(maskname[scan])['mask']
            thmap = np.array(resultshr['results']['thicknessmapselection'][0][0])
            thmap[np.isinf(thmap)] = 0
            thmap[np.isnan(thmap)] = 0
            thmap[thmap>excludelargerthan] = 0
            thmap[thmap<excludesmallerthan] = 0
            thicknessmaplist.append(np.transpose(thmap * mask, (1, 0, 2)))
            maskedthick=np.transpose(thmap * mask,(1, 0, 2))

            WtransmathighresROI = gettransformmatrix(dicom.read_file(ROIdcmname[scan]))  
            
            biglist.append(np.concatenate((maskedthick[np.nonzero(maskedthick)][..., None], np.array(np.nonzero(maskedthick)).T, scan * np.ones((maskedthick[np.nonzero(maskedthick)].shape[0], 1))), axis = 1))
            #compute world transform matrix

            WtransmatFULL = gettransformmatrix(dicom.read_file(FULLdcmname[scan]))
            roi_to_full_wmat.append(np.dot(np.linalg.inv(WtransmatFULL), WtransmathighresROI))
        # sample samples or load data if sample file exists
        thicknessmaplist = np.vstack(biglist)
        samples, labels = samplingfunction(thicknessmaplist, FULLdcmname, nrofsample, patchsz, roi_to_full_wmat, '')
        samples = np.transpose(samples[..., np.newaxis], (0, 4, 1, 2, 3)).astype('f')
        labels = np.squeeze(labels.astype('f'))
        data = {'samples':np.array(samples), 'labels':labels, 'options':optionsdict}
        pickle.dump(data,open(os.path.join(datadir, 'samples' + str(targetvoxelsize) + '.p'), 'wb'), protocol = 2)
        np.save(os.path.join(datadir, 'samples' + str(targetvoxelsize)), np.array(samples))
        np.save(os.path.join(datadir, 'samples' + str(targetvoxelsize) + '_opts'), optionsdict)
    else:
        loadsamples = pickle.load(open(os.path.join(datadir, 'samples' + str(targetvoxelsize) + '.p'), 'rb'))
        loadsamplesopts = np.load(os.path.join(datadir, 'samples' + str(targetvoxelsize) + '_opts.npy')).item()
        samples = loadsamples['samples'].astype('f')
        patchsz = samples.shape[-1]
        optionsdict['nrofsample'] = loadsamplesopts['nrofsample']
        try:
            optionsdict['excludesmallerthan'] = loadsamplesopts['excludesmallerthan']
        except:
            optionsdict['excludesmallerthan'] = 0
        optionsdict['excludelargerthan'] = loadsamplesopts['excludelargerthan']
        optionsdict['patchsz'] = patchsz
        
        labels = np.squeeze(loadsamples['labels'].astype('f'))
        
    toc = time.clock()
    print(toc - tic)
    global minlbl, maxlbl
    maxlbl = np.max(labels)
    minlbl = np.min(labels)
    labels = 1 * (labels - minlbl) / ( maxlbl - minlbl)     #https://stats.stackexchange.com/questions/26144/how-to-get-real-valued-continous-output-from-neural-network
    
    #data normalization
    samples=(samples - np.mean(samples)) / (np.std(samples) + 1*10E-8)
        
    # We can now download and read the training and test set images and labels.
    addition = random.randint(0,  len(labels) - nrofsample)
    X_train, y_train = samples[addition:addition + np.round(3 / 5 * nrofsample).astype(int)], labels[addition:addition + np.round(3 / 5 * nrofsample).astype(int)]
    X_val, y_val = samples[addition + np.round(3 / 5* nrofsample + 1).astype(int):addition + np.round(4 / 5 * nrofsample).astype(int)], labels[addition + np.round(3 / 5 * nrofsample + 1).astype(int):addition + np.round(4 / 5 * nrofsample).astype(int)]
    X_test, y_test = samples[addition + np.round(4 / 5 * nrofsample + 1).astype(int):addition + np.round(5 / 5 * nrofsample).astype(int)], labels[addition + np.round(4 / 5 * nrofsample + 1).astype(int):addition + np.round(5 / 5 * nrofsample).astype(int)]
    global originalcoords
    originalcoords = np.load(os.path.join(datadir, 'samples' + str(targetvoxelsize) + '_origcoords.npy'))
    originalcoords = originalcoords[addition + np.round(4 / 5 * nrofsample + 1).astype(int):addition + np.round(5 / 5 * nrofsample).astype(int)]
    
    
    # We just return all the arrays in order, as expected in main().
    # (It doesn't matter how we do this as long as we can read them again.)
    return X_train, y_train, X_val, y_val, X_test, y_test

'''
# ##################### Build the neural network model #######################
# This script supports three types of models. For each one, we define a
# function that takes a Theano variable representing the input and returns
# the output layer of a neural network model built in Lasagne.
'''
def build_mlp(input_var = None):
    # This creates an MLP of two hidden layers of 800 units each, followed by
    # a softmax output layer of 10 units. It applies 20% dropout to the input
    # data and 50% dropout to the hidden layers.

    # Input layer, specifying the expected input shape of the network
    # (unspecified batchsize, 1 channel, 28 rows and 28 columns) and
    # linking it to the given Theano variable `input_var`, if any:
    network = lasagne.layers.InputLayer(shape = (None, 1, patchsz, patchsz, patchsz),
                                     input_var = input_var)

    # Convolution and dropout
    network = lasagne.layers.Conv3DLayer(network, 128, 3, 1, nonlinearity = lasagne.nonlinearities.linear)
    network = lasagne.layers.DropoutLayer(network, p = 0.1)  
    
    # Convolution and dropout
    network = lasagne.layers.Conv3DLayer(network, 64, 3, 1, nonlinearity = lasagne.nonlinearities.linear)
    network = lasagne.layers.DropoutLayer(network, p = 0.1)    
   
    # Convolution, max pool and dropout
    network = lasagne.layers.Conv3DLayer(network, 64, 2, 1, nonlinearity = lasagne.nonlinearities.linear)
    network = lasagne.layers.MaxPool3DLayer(network, 2, 2)
    network = lasagne.layers.DropoutLayer(network, p = 0.1)
    
    # Convolution and dropout
    network = lasagne.layers.Conv3DLayer(network, 64, 1, 1, nonlinearity = lasagne.nonlinearities.linear)
    network = lasagne.layers.DropoutLayer(network, p=0.1) 
    
    # Add a fully-connected layer of 800 units, using the linear rectifier, and
    # initializing weights with Glorot's scheme (which is the default anyway):
    network = lasagne.layers.DenseLayer(
            network, num_units=400,
            nonlinearity = lasagne.nonlinearities.linear,
            W=lasagne.init.GlorotUniform())
    network = lasagne.layers.DropoutLayer(network, p = 0.6)

    if 0:
        network2 = lasagne.layers.InputLayer(shape = (None, 1, patchsz, patchsz, patchsz),
                                         input_var = input_var)
        
        # Convolution and dropout
        network2 = lasagne.layers.Conv3DLayer(network2, 128, 5, 1, nonlinearity = lasagne.nonlinearities.linear)
        network2 = lasagne.layers.DropoutLayer(network2, p = 0.1)  
        
        # Convolution, max pool and dropout
        network2 = lasagne.layers.Conv3DLayer(network2, 64, 3, 1, nonlinearity = lasagne.nonlinearities.linear)
        #network2 = lasagne.layers.MaxPool3DLayer(network2, 2, 2)
        network2 = lasagne.layers.DropoutLayer(network2, p = 0.1)
        
        network2 = lasagne.layers.DenseLayer(
                network2, num_units=200,
                nonlinearity = lasagne.nonlinearities.linear,
                W=lasagne.init.GlorotUniform())
        network2 = lasagne.layers.DropoutLayer(network2, p = 0.6)
    
        # Finally, we'll add the fully-connected output layer, of 10 softmax units:
    
    if 1:
        network3 = lasagne.layers.InputLayer(shape = (None, 1, patchsz, patchsz, patchsz),
                                         input_var = input_var)
        
        # Convolution and dropout
        network3 = lasagne.layers.Conv3DLayer(network3, 128, 7, 1, nonlinearity = lasagne.nonlinearities.linear)
        network3 = lasagne.layers.MaxPool3DLayer(network3, 2, 2) 
        network3 = lasagne.layers.DropoutLayer(network3, p = 0.1)  

        network3 = lasagne.layers.DenseLayer(
                network3, num_units=200,
                nonlinearity = lasagne.nonlinearities.linear,
                W=lasagne.init.GlorotUniform())
        network3 = lasagne.layers.DropoutLayer(network3, p = 0.6)
    if 1:
        network5 = lasagne.layers.InputLayer(shape = (None, 1, patchsz, patchsz, patchsz),
                                         input_var = input_var)
        
        # Convolution and dropout
        network5 = lasagne.layers.Conv3DLayer(network5, 128, 9, 1, nonlinearity = lasagne.nonlinearities.linear)
        network5 = lasagne.layers.MaxPool3DLayer(network5, 2, 2) 
        network5 = lasagne.layers.DropoutLayer(network5, p = 0.1)  

        network5 = lasagne.layers.DenseLayer(
                network5, num_units=200,
                nonlinearity = lasagne.nonlinearities.linear,
                W=lasagne.init.GlorotUniform())
        network5 = lasagne.layers.DropoutLayer(network5, p = 0.6)    
        # Finally, we'll add the fully-connected output layer, of 10 softmax units:
    network4 = lasagne.layers.ConcatLayer([network,network3,network5])
    
    network4 = lasagne.layers.DenseLayer(
            network, num_units = 1, nonlinearity = lasagne.nonlinearities.linear,
            W=lasagne.init.GlorotUniform())

    # Each layer is linked to its incoming layer(s), so we only need to pass
    # the output layer to give access to a network in Lasagne:
    return network4



"""
# ############################# Batch iterator ###############################
# This is just a simple helper function iterating over training data in
# mini-batches of a particular size, optionally in random order. It assumes
# data is available as numpy arrays. For big datasets, you could load numpy
# arrays as memory-mapped files (np.load(..., mmap_mode='r')), or write your
# own custom data iteration function. For small datasets, you can also copy
# them to GPU at once for slightly improved performance. This would involve
# several changes in the main program, though, and is not demonstrated here.
# Notice that this function returns only mini-batches of size `batchsize`.
# If the size of the data is not a multiple of `batchsize`, it will not
# return the last (remaining) mini-batch.
"""
def iterate_minibatches(inputs, targets, batchsize, shuffle = False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsize + 1, batchsize):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsize]
        else:
            excerpt = slice(start_idx, start_idx + batchsize)
        yield inputs[excerpt], targets[excerpt]

"""
# ############################## Main program ################################
# Everything else will be handled in our main program now. We could pull out
# more functions to better separate the code, but it wouldn't make it any
# easier to read.
"""
def main(nrofsample = 10000, targetvoxelsize = 0.8, sampledata = 0,patchsz = 17, excludelargerthan = np.inf, num_epochs = 75, selectionrange = np.array([0.35,0.45]), learningrate = 0.0005, blurstrength = 0.5, excludesmallerthan = 0, scanorder='', usemagormod = 0):
    # Load the dataset
    print("Loading data...")
    X_train, y_train, X_val, y_val, X_test, y_test = load_dataset()

    # Prepare Theano variables for inputs and targets
    input_var = T.tensor5('inputs')
    target_var = T.fvector('targets')
    # Create neural network model (depending on first command line parameter)
    print("Building model and compiling functions...")
    if loadresults:
        network = build_mlp(input_var)
        #network_arch = np.load(os.path.join(resultdir, 'network_vsize_' + loadresults +'.npy'))
        #network = construct_network(input_var,network_arch)
        
    else:
        network = build_mlp(input_var)
    
    global networkstring
    networkstring=[(layer.__class__, layer.__dict__) for layer in lasagne.layers.get_all_layers(network)]

    # Create a loss expression for training, i.e., a scalar objective we want
    # to minimize (for our multi-class problem, it is the cross-entropy loss):
    prediction = lasagne.layers.get_output(network)
    loss = lasagne.objectives.squared_error(prediction, target_var)
    loss = loss.mean()
    # We could add some weight decay as well here, see lasagne.regularization.

    # Create update expressions for training, i.e., how to modify the
    # parameters at each training step. Here, we'll use Stochastic Gradient
    # Descent (SGD) with Nesterov momentum, but Lasagne offers plenty more.
    params = lasagne.layers.get_all_params(network, trainable = True)
    updates = lasagne.updates.rmsprop(
            loss, params, learning_rate = lr)

    # Create a loss expression for validation/testing. The crucial difference
    # here is that we do a deterministic forward pass through the network,
    # disabling dropout layers.
    test_prediction = lasagne.layers.get_output(network, deterministic = True)
    test_loss = lasagne.objectives.squared_error(test_prediction,
                                                            target_var)
    test_loss = test_loss.mean()
    # As a bonus, also create an expression for the classification accuracy:
    test_acc = T.mean(T.eq(T.argmax(test_prediction, axis = 1), target_var),
                      dtype = theano.config.floatX)

    # Compile a function performing a training step on a mini-batch (by giving
    # the updates dictionary) and returning the corresponding training loss:
    train_fn = theano.function([input_var, target_var], loss, updates = updates)

    # Compile a second function computing the validation loss and accuracy:
    val_fn = theano.function([input_var, target_var], [test_loss, test_acc])
    pred_fn = theano.function([input_var], test_prediction)
    # Finally, launch the training loop.
    if loadresults:
        holdepoch = 0
        epochhold=np.array([0])
        print("Starting testing...")
        holdweights = np.load(os.path.join(resultdir,'network_weights_vsize_'+loadresults+'.npy'))
    else:
        print("Starting training...")
        # We iterate over epochs:
        global valerror
        valerror = []
        trainerrorhold = []
        epochhold = []
        meandifhold = []
        medshold = []
        stdevhold  = []
        iqrrhold = []
        meandifrnghold = []
        medsrnghold = []
        stdrnghold = []
        iqrrnghold = []
        inrange = np.array([[np.nan], [np.nan]])
        targetsvalhold = []
        predsvalhold = []
        epoch = -1 
        holdepoch = 0
        currentminvalerror = np.inf
    
        while epoch <num_epochs:
            epoch+=1
            # In each epoch, we do a full pass over the training data:
            train_err = 0
            train_batches = 0
            start_time = time.time()
            for batch in iterate_minibatches(X_train, y_train, 256, shuffle = True):
                inputs, targets = batch
                train_err += train_fn(inputs, targets)
                train_batches += 1
    
            # And a full pass over the validation data:
            val_err = 0
            val_acc = 0
            val_batches = 0
            meandif = 0
            stdev = 0
            iqrr = 0
            meds = 0
            thinrange = 0
            thinrangestd = 0
            thinrangeavg = 0
            thinrangeiqr = 0
            res = np.array([[np.nan], [np.nan]])
            targetsvalhold = []
            predsvalhold = []
            for batch in iterate_minibatches(X_val, y_val, 256, shuffle = False):
                inputs, targets = batch
                err, acc = val_fn(inputs, targets)
                preds = pred_fn(inputs)
    
                res = np.append(res, np.squeeze(np.array([[preds], [targets[..., None]]])), axis = 1)
    
                targets = targets[..., None]
                selection = np.logical_and((selectionrange[0] - minlbl) / (maxlbl  - minlbl)  < targets, targets < (selectionrange[1] - minlbl) / (maxlbl - minlbl)) == True
                targets = targets * (maxlbl - minlbl) + minlbl
                preds=preds * (maxlbl - minlbl) + minlbl
                inrange = np.append(inrange, np.squeeze(np.array([[preds[selection]], [targets[selection]]])), axis = 1)
                thinrangeavg += np.median(preds[selection])
                thinrange += np.mean((preds - targets)[selection])
                thinrangestd += np.std(((preds - targets)[selection]))
                thinrangeiqr += np.subtract(*np.percentile((preds - targets)[selection], [75, 25]))
                meandif += np.mean(np.abs(preds - targets))
                stdev += np.std(preds - targets)
                meds += np.median(preds - targets)
                iqrr += np.subtract(*np.percentile(preds - targets, [75, 25]))
                val_err += err
                val_acc += acc
                val_batches += 1
                targetsval = targets
                targetsvalhold.append(targetsval)
                predsvalhold.append(preds)
           # Check if validation error is lower than that of previous iteration
            if epoch>0:
                if val_err / val_batches < currentminvalerror:
                    holdweights = lasagne.layers.get_all_param_values(network)
                    holdepoch = epoch
                    currentminvalerror = val_err / val_batches
    
            # Then we print the results for this epoch:
            print("Epoch {} of {} took {:.3f}s".format(
                epoch + 1, num_epochs, time.time() - start_time))
            print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
            print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
            print("  mean difference thickness:\t{:.6f} mm".format(
                meandif / val_batches))
            print("  median difference thickness:\t{:.6f} mm".format(
                meds / val_batches))
            print("  stdev difference thickness:\t{:.6f} mm".format(
                stdev / val_batches))
            print("  iqr difference thickness:\t\t{:.6f} mm".format(
                iqrr / val_batches))
            print("  mean difference thickness rnge:\t{:.6f} mm".format(
                thinrange / val_batches))
            print("  median thickness rnge:\t\t{:.6f} mm".format(
                thinrangeavg / val_batches))
            print("  stdev thickness rnge:\t{:.6f} mm".format(
                thinrangestd / val_batches))
            print("  iqr thickness rnge:\t{:.6f} mm".format(
                thinrangeiqr / val_batches))
            valerror.append(val_err / val_batches)
            trainerrorhold.append(train_err / train_batches)
            epochhold.append(epoch)
            meandifhold.append( meandif / val_batches)
            medshold.append( meds / val_batches)
            stdevhold.append(stdev / val_batches)
            iqrrhold.append( iqrr / val_batches)
            meandifrnghold.append( thinrange / val_batches)
            medsrnghold.append(thinrangeavg / val_batches)
            stdrnghold.append(thinrangestd / val_batches)
            iqrrnghold.append( thinrangeiqr / val_batches)
            # exit loop if within ten epochs the validation error didnt decrease
            if epoch>holdepoch+epochdif:
                epoch = num_epochs       
            else:
                print('iterations till stop: ' +str(epochdif - (epoch - holdepoch)))
       
    # After training, we compute and print the test error:
    epochhold[holdepoch] *= -1
    lasagne.layers.set_all_param_values(network, holdweights)
    test_err = 0
    test_acc = 0
    test_batches = 0
    meandif = 0
    res = np.array([[np.nan], [np.nan]])
    global targetstesthold, predstesthold
    targetstesthold = []
    predstesthold = []
    for batch in iterate_minibatches(X_test, y_test, 250, shuffle = False):
        inputs, targets = batch
        err, acc = val_fn(inputs, targets)
        test_err += err
        test_acc += acc
        test_batches += 1
        predstest = pred_fn(inputs)
        res = np.append(res,np.squeeze(np.array([[predstest], [targets[..., None]]])), axis = 1)
        predstest = predstest * (maxlbl - minlbl) + minlbl
        targets = targets * (maxlbl - minlbl) + minlbl
        meandif += np.mean(np.abs(predstest - targets))
        targetstest = targets
        targetstesthold.append(targetstest)
        predstesthold.append(predstest)
    print("Final results:")
    print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
    print("  mean difference thickness:\t\t{:.6f} mm".format(
            meandif / test_batches))
    targetstesthold = np.concatenate(targetstesthold)
    predstesthold = np.concatenate(predstesthold)
    plt.scatter(targetstesthold,predstesthold)
    holdtestpredictions = np.zeros([originalcoords.shape[0],1])
    
    holdtestpredictions[0:predstesthold.size]=predstesthold
    originalcoordsappended = np.concatenate((originalcoords,holdtestpredictions),axis=1)
    if not loadresults:
        plt.figure()
        plt.plot(np.array(valerror))
        
        inputs = ''
        valerror.append(test_err / test_batches)
        trainerrorhold.append(test_err / test_batches)
        epochhold.append(-1)
        meandifhold.append(    meandif / test_batches)
        medshold.append(0)
        stdevhold.append(0)
        iqrrhold.append(0)
        meandifrnghold.append(0)
        medsrnghold.append(0)
        stdrnghold.append(0)
        iqrrnghold.append(0)
        curtime = datetime.datetime.now().strftime('%Y-%m-%d_%H;%M;%S')
        plt.figure()

        # Save results in order: error statistics for each epoch(.txt), test results(.npy), network shape(.npy), and network weights (.npy and .p)
        np.savetxt(os.path.join(resultdir, 'results_vsize_' + str(targetvoxelsize) + '_' + str(curtime) + '.txt'), np.array(np.concatenate((np.array(epochhold)[..., None].astype(int),
                                                               np.array(trainerrorhold)[..., None], np.array(valerror)[..., None], np.array(meandifhold)[..., None],
                                                               np.array( medshold)[..., None], np.array(stdevhold)[..., None], np.array(iqrrhold)[..., None], np.array(meandifrnghold)[..., None],
                                                               np.array(medsrnghold)[..., None], np.array(stdrnghold)[..., None], np.array(iqrrnghold)[..., None]), axis = 1)), header = 'epoch\t\t\t\ttrain loss\t\t\tval loss\t\t\tmean dif thness\t\tmed dif thness\t\tstdev diff thness\tiqr diff thness\t\tmean dif thnssrange\tmed thnessrange\t\tstdev thnssrange\tiqr thnssrange', footer = 'Validation error for all epochs, last value is test error. ' + str(optionsdict),fmt = '%1.5f', delimiter = '\t\t\t\t')
    
        np.save(os.path.join(resultdir, 'results_vsize_' + str(targetvoxelsize) + '_' + str(curtime) + '.npy'), np.concatenate((predstesthold, targetstesthold[..., None]), axis = 1))
        np.save(os.path.join(resultdir, 'network_vsize_' + str(targetvoxelsize) + '_' + str(curtime) + '.npy'), np.array(networkstring))
        np.save(os.path.join(resultdir, 'network_weights_vsize_' + str(targetvoxelsize) + '_' + str(curtime) + '.npy'), holdweights)
        pickle.dump(network, open(os.path.join(resultdir,'network_las_form_vsize_' + str(targetvoxelsize) + '_' + str(curtime) + '.p'), 'wb'), protocol = 2)
        scipy.io.savemat(os.path.join(resultdir, 'network_weights_vsize_' + str(targetvoxelsize) + '_' + str(curtime) + '.mat'),dict(w=holdweights))
    #save for further analysis in Matlab
    if not loadresults:
        np.save(os.path.join(datadir, 'samples' + str(targetvoxelsize) + '_' + str(curtime) + '_appendedorigcoords.npy'),originalcoordsappended)    
    else:
        np.save(os.path.join(datadir, 'samples_' + loadresults + '_appendedorigcoords.npy'),originalcoordsappended)    
        
    print(np.corrcoef(targetstesthold.T, predstesthold.T))
    network=''
    axes = plt.gca()
    axes.set_xlim([0, 1.4])
    axes.set_ylim([0, 1.4])
    targets=''


if __name__ == '__main__':
    if ('--help' in sys.argv) or ('-h' in sys.argv):
        print("Trains a neural network on MNIST using Lasagne.")
        print("Usage: %s [MODEL [EPOCHS]]" % sys.argv[0])
        print()
        print("MODEL: 'mlp' for a simple Multi-Layer Perceptron (MLP),")
        print("       'custom_mlp:DEPTH,WIDTH,DROP_IN,DROP_HID' for an MLP")
        print("       with DEPTH hidden layers of WIDTH units, DROP_IN")
        print("       input dropout and DROP_HID hidden dropout,")
        print("       'cnn' for a simple Convolutional Neural Network (CNN).")
        print("EPOCHS: number of training epochs to perform (default: 500)")
    else:
        kwargs = {}
        if len(sys.argv) > 1:
            kwargs['nrofsample'] = sys.argv[1]
        if len(sys.argv) > 2:
            kwargs['targetvoxelsize'] = sys.argv[2]
        if len(sys.argv) > 3:
            kwargs['sampledata'] = sys.argv[3]
        if len(sys.argv) > 4:
            kwargs['patchsz'] = sys.argv[4]
        if len(sys.argv) > 5:
            kwargs['excludelargerthan'] = sys.argv[5]
        if len(sys.argv) > 6:
            kwargs['num_epochs'] = sys.argv[6]
        if len(sys.argv) > 7:
            kwargs['eselectionrange'] = sys.argv[7]
        if len(sys.argv) > 8:
            kwargs['learningrate'] = sys.argv[8]
        if len(sys.argv) > 9:
            kwargs['blurstrength'] = sys.argv[9]
        if len(sys.argv) > 10:
            kwargs['excludesmallerthan'] = sys.argv[10]
        if len(sys.argv) > 11:
            kwargs['scanorder'] = sys.argv[11]
        if len(sys.argv) > 12:
            kwargs['usemagormod'] = sys.argv[12]
        if not optionsdict:
            main(**kwargs)
        else:
            main(**optionsdict)
