# -*- coding: utf-8 -*-
"""
Created on Fri May  4 16:11:54 2018

@author: khespen
"""

-------------------------------
in conv.py
-----------------------------

- tekst uit het oorspronkelijke bericht weergeven -
        filter_flip=True corresponds to conv_mode='conv' in cuDNN, 
        and filter_flip=False corresponds to conv_mode='corr'.
                
- tekst uit het oorspronkelijke bericht weergeven -
    def convolve(self, input, **kwargs):
        # we perform a convolution backward pass wrt weights,
        # passing kernels as output gradient
        print ('calling convolve by Dilated 3D layer')
        imshp = self.input_shape
        kshp = self.output_shape
        print ('type(kshp) = {}'.format(type(kshp)))
        # only works with int64
        kshp_64 = np.asarray(kshp)
        kshp_64 = kshp_64.astype(np.int64)
        # swapping
        channels = kshp[1]
        batchsize = kshp[0]
        
        kshp_64[0] = channels
        kshp_64[1] = batchsize
        print ('shape of kshp_64 = {}'.format(kshp_64))#kshp = (2, 1, 15, 15, 15)
        print ('shape of kshp = {}'.format(kshp))
        # and swapping channels and batchsize
        imshp = (imshp[1], imshp[0]) + imshp[2:]
        #kshp = (kshp[1], kshp[0]) + kshp[2:]
        print ('should be 1D tensor, shape of kshp = {}'.format(kshp))
       
        output_size = self.output_shape[2:]
        if any(s is None for s in output_size):
            output_size = self.get_output_shape_for(input.shape)[2:]

        conved = dnn_gradweight3D(input.transpose(1, 0, 2, 3, 4), self.W, imshp, kshp_64,
                                  self.dilation)
        return conved.transpose(1, 0, 2, 3, 4)
--------------------------------------------------------------------------------------------------------------------------------------------
in cuda dnn.py
--------------------------------------------------------------------------
def dnn_gradweight3D(img, topgrad, imshp, kshp,
            subsample, border_mode='valid',
            filter_flip=False):
    print ('now inside dnn_gradweight3D')        
    """
    GPU convolution gradient with respect to weight using cuDNN from NVIDIA.

    The memory layout to use is 'bc01', that is 'batch', 'channel',
    'first dim', 'second dim' in that order.

    :warning: The cuDNN library only works with GPU that have a compute
      capability of 3.0 or higer.  This means that older GPU will not
      work with this Op.
    """
    
    if filter_flip:
        conv_mode = 'conv'
    else:
        conv_mode = 'cross'

    img = gpu_contiguous(img)
    topgrad = gpu_contiguous(topgrad)
    #Many tensor Ops run their arguments through this function as pre-processing.
    #It passes through TensorVariable instances,
    #and tries to wrap other objects into TensorConstant.
    kerns_shp = theano.tensor.as_tensor_variable(kshp)
    
    print ('kshp = {}'.format(kshp))
    print ('type = {}'.format(type(kshp)))
    print ('kerns_shp (1D shape tensor ?) = {}'.format(kerns_shp))
    print (' kerns_shp.ndim = {}'.format(kerns_shp.ndim))
    print (' kern_shape.type.dtype (int64?)= {}'.format(kerns_shp.type.dtype))
#    desc = GpuDnnConvDesc(border_mode=border_mode, subsample=subsample,
#                          conv_mode=conv_mode)(img.shape, kerns_shp)
#    desc = GpuDnnConvDesc(border_mode='valid', subsample=(1, 1),
#                              conv_mode='cross', precision=precision)(img.shape,
#                                                                      out.shape)
#    
    desc = GpuDnnConvDesc(border_mode=border_mode, subsample=subsample,
                          conv_mode=conv_mode)(img.shape, kerns_shp)
    out = gpu_alloc_empty(*kerns_shp)
    return GpuDnnConv3dGradW()(img, topgrad, out,
                                      desc)

-----------------------------------------------------------------------------
test code
--------------------------------------------------------------------------
    minibatch_size = 2
    tree_side = 17
    num_filters = 1
    input_data = np.random.rand(minibatch_size, 1, tree_side,tree_side,tree_side)
    # GPU takes floatX/ 32
    input_data = input_data.astype(theano.config.floatX)
    net = {}
    
    net['input'] = L.layers.InputLayer((minibatch_size,
                                            1,
                                            tree_side,tree_side,tree_side),
                                           input_var=input_data)
 
        
    net['normal'] = lasagne.layers.dnn.Conv3DDNNLayer(net['input'], num_filters,
                                                             filter_size=(3,3,3),
                                                        W=L.init.GlorotUniform(),
                                                        b=L.init.Constant(0.),
                                                        nonlinearity=rectify,
                                                            pad='valid')
    # convole input
    convol_output = L.layers.get_output(net['normal']).eval()
    # get filter and bias
    fil = lasagne.layers.get_all_params(net['normal'])
    # feed the same filter and bias
    net['dilated'] = L.layers.DilatedConv3DLayer(net['input'], num_filters,
                                                             filter_size=(3,3,3),
                                                            dilation=(2, 2, 2),
                                                        W=fil[0].get_value(),
                                                        b=fil[1].get_value(),
                                                        nonlinearity=rectify,
                                                            pad=0)
    
    dilated_output = L.layers.get_output(net['dilated']).eval()
    
    # expect outputs are equal
    # becasue dilated convolution of factor 1 = normal convolution
    print (np.allclose(dilated_output, convol_output, atol=1e-07))