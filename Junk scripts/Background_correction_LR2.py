#!/usr/bin/env python

from mevis import *
import numpy as np

def execute():
  for ind in range(0,ctx.field("FieldIterator.numValues").intValue()):
    ctx.field("FieldIterator.curIndex").setIntValue(ind)
    ctx.field("itkImageFileWriter.save").touch()