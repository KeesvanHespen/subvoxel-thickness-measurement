# -*- coding: utf-8 -*-
"""
Created on Fri Sep  8 09:06:55 2017

@author: khespen https://groups.google.com/forum/#!topic/lasagne-users/UnIXLU3HPyg
"""
import lasagne
#from nolearn.lasagne import NeuralNet
#from nolearn.lasagne import BatchIterator
#from nolearn.lasagne import TrainSplit

def PrintLasagneNetInfo(lasagneNetwork):

    nolearnlayer=[(layer.__class__,layer.__dict__) for layer in lasagne.layers.get_all_layers(lasagneNetwork)]

    #run through nolearnlayer and set names to something different from none and replace names that have been changed in newer versions of lasagne
    i=0
    for layer_factory, layer_kw in nolearnlayer:
        if 'name' in layer_kw and layer_kw['name'] == None or 'name' not in layer_kw:
            # if layer_kw['name'] == None:
            layer_kw['name']=layer_factory.__name__ + str(i)
            i+=1
            
        if 'input_layer' in layer_kw: #replace input by new key-word: incoming
            layer_kw['incoming']=layer_kw['input_layer']
            del layer_kw['input_layer']
        if 'input_layers' in layer_kw: #replace input by new key-word: incoming
            layer_kw['incomings']=layer_kw['input_layers']
            del layer_kw['input_layers']
        if 'incoming' in layer_kw:
            layer_kw['incoming']=layer_kw['incoming'].name
        if 'incomings' in layer_kw:
            layer_kw['incomings']=[x.name for x in layer_kw['incomings']]
        if 'merge_function' in layer_kw:
            del layer_kw['merge_function']
        if 'params' in layer_kw:
            del layer_kw['params']
        if 'input_shape' in layer_kw:
            del layer_kw['input_shape']
        if 'input_shapes' in layer_kw:
            del layer_kw['input_shapes']
        if 'mode' in layer_kw: #for pooling layer
            del layer_kw['mode']
        if '_srng' in layer_kw: #for pooling layer
            del layer_kw['_srng']
            
    #Run through layers in lasagneNetwork
 #   nolearnNetwork=NeuralNet(
 #   layers=nolearnlayer,
 #   max_epochs=1,
 #   update=lasagne.updates.nesterov_momentum,
 #   update_momentum=0.9,
  #  update_learning_rate=0.001,
  #  batch_iterator_train=BatchIterator(50),
 #   train_split=TrainSplit(eval_size=0.25),
 #   )
#    nolearnNetwork.initialize()

    #Print layerinfo
 #   import nolearn
 #   layer_info = nolearn.lasagne.PrintLayerInfo()
#    print(layer_info._get_greeting(nolearnNetwork))
 #   layer_info, legend = layer_info._get_layer_info_conv(nolearnNetwork)
  #  print(layer_info)
   # print(legend)
    return nolearnlayer