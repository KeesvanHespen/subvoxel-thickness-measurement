#!/usr/bin/env python

"""
Python script employing Lasagne and Theano to calculate the vessel wall 
thickness from image patches at any given resolution."""

from __future__ import print_function

import datetime
import sys
import os
import random
import time


import dicom
import nibabel as nib
import numpy as np
import theano
import theano.tensor as T
from lasagne .utils import as_theano_expression
from theano.compile.debugmode import DebugMode
import scipy
import scipy.io as sio


import lasagne
import pickle
import matplotlib.pyplot as plt

import h5py
import win32com.client as win32

from gettransformmatrix import gettransformmatrix
from normalize_input import normalize_input

# ################## Download and prepare the MNIST dataset ##################
# This is just some way of getting the MNIST dataset from an online location
# and loading it into numpy arrays. It doesn't involve Lasagne at all.
global ROIdcmname, comments, errorfunction

ROIresultsname = ['C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S005/results_ROI_0042-S005_FP020_PF015_selection.mat',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S007/results_ROI_0042R-S007_FP020_PF015_selection-v2.mat',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S001/results_ROI_0030-S001_2_FP020_PF015_selection.mat',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S004/results_ROI_0043-S004_FP020_PF015_selection.mat',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S006-3/results_ROI_0037-S006_selection.mat',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S011-3/results_ROI_0037-S011-3_selection.mat',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160324_S012_S010/ROI-S010/results_ROI_0042R-S010_FP020_PF015_selection.mat',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S002-3/results_ROI_0049-S002_selection.mat',
            'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S003-3/results_ROI_0049-S003_selection.mat',
             'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S008-3/results_ROI_0031-S008_selection.mat',
            'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S009-3/results_ROI_0031-S009_selection.mat',
           # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S006-2/results_ROI_0037-S006-2_selection-v2.mat',
       #     'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S002-2/results_ROI_0049-S002-2_selection-v2.mat',
          #  'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S003-2/results_ROI_0049-S003-2_selection',
           #  'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S001-2/results_ROI_0030-S001-2_selection',
           #  'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S004-2/results_ROI_0030-S004-2_selection.mat',
             'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S021/results_ROI_0091-S021_selection.mat',
            # 'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S021/results_ROI_0091-S021-2_selection-v2.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20171711_S017_S018/S017/results_ROI_0061-S017-2_selection-v2.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20170412_S023_S024/S024/results_ROI_0121-S024_selection-v2.mat',
            #   'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S009-2/results_ROI_0031R-S009-2_selection-v2.mat',
           #    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S008-2/results_ROI_0031R-S008-2_selection-v2.mat',
             #  'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S011-2/results_ROI_0037-S011-2_selection-v2.mat',
       
            #   'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S021/results_ROI_0091-S021-3_selection-v2.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/22022018_S033_S034/S034/results_ROI_0017-S034-33_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01032018_S035_S036/S035/results_ROI_0024-S035_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20172710_S015_S016/S015/results_ROI_0074-S015_selection.mat',
              # 'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20172710_S015_S016/S015/results_ROI_0074-S015-2_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01032018_S035_S036/S036/results_ROI_0024-S036_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08032018_S037_S038/S037/results_ROI_0024-S037_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08032018_S037_S038/S038/results_ROI_0024-S038_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01022018_S029_S030/S029/results_ROI_0046-S029_normalized_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01022018_S029_S030/S030/results_ROI_0046-S030_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/07022018_S031_S032/S031/results_ROI_0059-S031_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/07022018_S031_S032/S032/results_ROI_0059-S032_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/11012018_S025_S026/S026/results_ROI_0062-S026_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/18012018_S027_S028/S027/results_ROI_0071-S027_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/18012018_S027_S028/S028/results_ROI_0071-S028_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S022/results_ROI_0091-S022_selection.mat',
               
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08092017_S013_S014/S013/results_ROI_0000-S013_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08092017_S013_S014/S014/results_ROI_0000-S014_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/24052018_S041_S042/S042/results_ROI_0024-S042_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/24052018_S041_S042/S041/results_ROI_0024-S041_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/05072018_S047_S048/S048/results_ROI_0024-S048_selection.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/05072018_S047_S048/S047/results_ROI_0024-S047_selection.mat'     
                       #    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/25112017_S019_S020/S019/results_ROI_0055_S019_selection.mat'
    
              ]

maskname = ['C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S005/MCA_R_mask_ROI_0042-S005_2',
           'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S007/MCA_mask_ROI_0042R-S007_2',
           'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S001/MCA_R_mask_ROI_0030-S001_2',
          'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S004/MCA_R_mask_ROI_0043-S004_2',
          'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S006-3/MCA_mask_ROI_0037-S006',
           'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S011-3/MCA_R_mask_0037-S011-3',
          'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160324_S012_S010/ROI-S010/MCA_R_mask_ROI_0042R-S010_2',
          # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/3. Data/20160324_S012_S010/ROI-S012/MCA_mask_ROI_0042-S012',
           'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S002-3/MCA_ROI_0049-S002',
           'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S003-3/MCA_ROI_0049-S003',
           'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S008-3/MCA_ROI_0031-S008',
           'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S009-3/MCA_ROI_0031-S009',
           #'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S006-2/MCA_R_mask_ROI_0037-S006-2',
           # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S002-2/MCA_R_mask_ROI_0049-S002-2',
            #'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S003-2/MCA_R_mask_ROI_0049-S003-2',
            # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S001-2/MCA_R_mask_ROI_0030-S001-2',
            # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S004-2/MCA_R_mask_ROI_0030-S004-2',
             'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S021/MCA_ROI_0091-S021.mat',
            #    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S021/MCA_R_mask_0091-S021-2.mat',
                'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20171711_S017_S018/S017/MCA_R_mask_0061-S017-2.mat',
                'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20170412_S023_S024/S024/MCA_R_mask_0121-S024.mat',
             #   'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S009-2/MCA_R_mask_0031R-S009-2.mat',
               # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S008-2/MCA_R_mask_0031R-S008-2.mat',
              #  'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S011-2/MCA_R_mask_0037-S011-2.mat',

               # 'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S021/MCA_R_mask_0091-S021-3.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/22022018_S033_S034/S034/MCA_R_mask_0017-S034-33.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01032018_S035_S036/S035/MCA_R_mask_0024-S035.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20172710_S015_S016/S015/MCA_R_mask_0074-S015.mat',
               #'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20172710_S015_S016/S015/MCA_R_mask_0074-S015-2.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01032018_S035_S036/S036/MCA_R_mask_0024-S036.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08032018_S037_S038/S037/MCA_R_mask_0024-S037.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08032018_S037_S038/S038/MCA_R_mask_0024-S038.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01022018_S029_S030/S029/MCA_R_mask_0046-S029.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01022018_S029_S030/S030/MCA_R_mask_0046-S030.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/07022018_S031_S032/S031/MCA_R_mask_0059-S031.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/07022018_S031_S032/S032/MCA_R_mask_0059-S032.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/11012018_S025_S026/S026/MCA_R_mask_0062-S026.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/18012018_S027_S028/S027/MCA_R_mask_0071-S027.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/18012018_S027_S028/S028/MCA_R_mask_0071-S028.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S022/MCA_R_mask_0091-S022.mat',
               
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08092017_S013_S014/S013/MCA_R_mask_0000-S013.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08092017_S013_S014/S014/MCA_R_mask_0000-S014.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/24052018_S041_S042/S042/MCA_R_mask_0024-S042.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/24052018_S041_S042/S041/MCA_R_mask_0024-S041.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/05072018_S047_S048/S048/MCA_R_mask_0024-S048.mat',
               'C:/Users/khespen/Documents/5. Hypertension/1. Working data/05072018_S047_S048/S047/MCA_R_mask_0024-S047.mat'     
             #  'C:/Users/khespen/Documents/5. Hypertension/1. Working data/25112017_S019_S020/S019/MCA_R_mask_0055_S019.mat'
                
         ]

ROIdcmname = ["C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S005/ROI_0042-S005.dcm",#0
    "C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S007/ROI_0042R-S007.dcm",#1
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S001/ROI_0030-S001_2.dcm',#2
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S004/ROI_0043-S004.dcm',#3
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S006-3/ROI_0037-S006.dcm',#4
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S011-3/ROI_0037-S011-3.dcm',#5
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160324_S012_S010/ROI-S010/ROI_0042R-S010.dcm',#6
    #'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/3. Data/20160324_S012_S010/ROI-S012/ROI_0042-S012.dcm',
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S002-3/ROI_0049-S002.dcm',#7
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S003-3/ROI_0049-S003.dcm',#8
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S008-3/ROI_0031-S008.dcm',#9
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S009-3/ROI_0031-S009.dcm',#10
    #'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S006-2/ROI_0037-S006-2.dcm',#
    #'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S002-2/ROI_0049-S002-2.dcm',#
   # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S003-2/ROI_0049-S003-2.dcm',#
   # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S001-2/ROI_0030-S001-2.dcm',#
   # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S004-2/ROI_0030-S004-2.dcm',#
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S021/ROI_0091-S021.dcm',#11
   # 'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S021/ROI_0091-S021-2.dcm',#
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20171711_S017_S018/S017/ROI_0061-S017-2.dcm',#12
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20170412_S023_S024/S024/ROI_0121-S024.dcm',
   # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S009-2/ROI_0031R-S009-2.dcm',#
   # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S008-2/ROI_0031R-S008-2.dcm',
   # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S011-2/ROI_0037-S011-2.dcm',#

#    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S021/ROI_0091-S021-3.dcm',#
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/22022018_S033_S034/S034/ROI_0017-S034-33.dcm',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01032018_S035_S036/S035/ROI_0024-S035.dcm',#15
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20172710_S015_S016/S015/ROI_0074-S015.dcm',
   # 'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20172710_S015_S016/S015/ROI_0074-S015-2.dcm',#
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01032018_S035_S036/S036/ROI_0024-S036.dcm',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08032018_S037_S038/S037/ROI_0024-S037.dcm',#18
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08032018_S037_S038/S038/ROI_0024-S038.dcm',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01022018_S029_S030/S029/ROI_0046-S029.dcm',#20
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01022018_S029_S030/S030/ROI_0046-S030.dcm',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/07022018_S031_S032/S031/ROI_0059-S031.dcm',#22
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/07022018_S031_S032/S032/ROI_0059-S032.dcm',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/11012018_S025_S026/S026/ROI_0062-S026.dcm',#24
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/18012018_S027_S028/S027/ROI_0071-S027.dcm',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/18012018_S027_S028/S028/ROI_0071-S028.dcm',#26
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S022/ROI_0091-S022.dcm',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08092017_S013_S014/S013/ROI_0000-S013.dcm',#28
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08092017_S013_S014/S014/ROI_0000-S014.dcm',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/24052018_S041_S042/S042/ROI_0024-S042.dcm',#30
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/24052018_S041_S042/S041/ROI_0024-S041.dcm',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/05072018_S047_S048/S048/ROI_0024-S048.dcm',#32
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/05072018_S047_S048/S047/ROI_0024-S047.dcm'  
   # 'C:/Users/khespen/Documents/5. Hypertension/1. Working data/25112017_S019_S020/S019/ROI_0055_S019.dcm'#
 
   ]

if 1:
    FULLdcmname = ["D:/1. Original Data FWHM/20160307_S007-S005/DICOM/DICOM/IM_0038_mag.dcm",#0
      'D:/1. Original Data FWHM/20160619_S001-S004/DICOM/DICOM/IM_0026_mag.dcm',#1 
      'D:/1. Original Data FWHM/20160222_S011-S006/DICOM/DICOM/IM_0033_mag.dcm',#2
      'D:/1. Original Data FWHM/20160324_S012-S010/DICOM/DICOM/IM_0038_mag.dcm',#3
      'D:/1. Original Data FWHM/20160618_S002-S003/DICOM/DICOM/IM_0045_mag.dcm',#4
      'D:/1. Original Data FWHM/20160627_S008-S009/DICOM/DICOM/IM_0027_mag.dcm',#5
    #["C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/IM_0042_cropped",#0
    #  'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/IM_0030_cropped',#1 
    #  'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/IM_0037_cropped',#2
     # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160324_S012_S010/IM_0042_cropped',#3
     # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/IM_0049_cropped',#4
    #  'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/IM_0031_cropped',#5
       'D:/3. Original Data-Hypertension/30112017_S021_S022/DICOM/IM_0056_mag.dcm',#6
       'D:/3. Original Data-Hypertension/20171711_S017_S018/DICOM/IM_0047_mag.dcm',#7
       'D:/3. Original Data-Hypertension/20170412_S023_S024/DICOM/IM_0112_mag.dcm',#8
        'D:/3. Original Data-Hypertension/22022018_S033_S034/DICOM/24_mag.dcm',#9
        'D:/3. Original Data-Hypertension/01032018_S035_S036/DICOM/IM_0022_mag.dcm',#10
        'D:/3. Original Data-Hypertension/08032018_S037_S038/DICOM/IM_0017_mag.dcm',#11
        'D:/3. Original Data-Hypertension/20172710_S015_S016/DICOM/IM_0063_mag.dcm',#12
        'D:/3. Original Data-Hypertension/01022018_S029_S030/DICOM/IM_0037_mag.dcm',#13
        'D:/3. Original Data-Hypertension/07022018_S031_S032/DICOM/IM_0050_mag.dcm',
        'D:/3. Original Data-Hypertension/11012018_S025_S026/DICOM/IM_0053_mag.dcm',
        'D:/3. Original Data-Hypertension/18012018_S027_S028/DICOM/IM_0056_mag.dcm']
else:
    FULLdcmname = ["D:/1. Original Data FWHM/20160307_S007-S005/DICOM/DICOM/IM_0042_kspace_resampled_mag.dcm",#0
      'D:/1. Original Data FWHM/20160619_S001-S004/DICOM/DICOM/IM_0030_kspace_resampled_mag.dcm',#1 
      'D:/1. Original Data FWHM/20160222_S011-S006/DICOM/DICOM/IM_0037_kspace_resampled_mag.dcm',#2
      'D:/1. Original Data FWHM/20160324_S012-S010/DICOM/DICOM/IM_0042_kspace_resampled_mag.dcm',#3
      'D:/1. Original Data FWHM/20160618_S002-S003/DICOM/DICOM/IM_0049_kspace_resampled_mag.dcm',#4
      'D:/1. Original Data FWHM/20160627_S008-S009/DICOM/DICOM/IM_0031_kspace_resampled_mag.dcm',#5
    #["C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/IM_0042_cropped",#0
    #  'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/IM_0030_cropped',#1 
    #  'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/IM_0037_cropped',#2
     # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160324_S012_S010/IM_0042_cropped',#3
     # 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/IM_0049_cropped',#4
    #  'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/IM_0031_cropped',#5
       'D:/3. Original Data-Hypertension/30112017_S021_S022/DICOM/IM_0091_kspace_resampled_mag.dcm',#6
       'D:/3. Original Data-Hypertension/20171711_S017_S018/DICOM/IM_0061_kspace_resampled_mag.dcm',#7
       'D:/3. Original Data-Hypertension/20170412_S023_S024/DICOM/IM_0121_kspace_resampled_mag.dcm',#8
        'D:/3. Original Data-Hypertension/22022018_S033_S034/DICOM/17_kspace_resampled_mag.dcm',#9
        'D:/3. Original Data-Hypertension/01032018_S035_S036/DICOM/IM_0024_kspace_resampled_mag.dcm',#10
        'D:/3. Original Data-Hypertension/08032018_S037_S038/DICOM/IM_0024_kspace_resampled_mag.dcm',#11
        'D:/3. Original Data-Hypertension/20172710_S015_S016/DICOM/IM_0074_kspace_resampled_mag.dcm',#12
        'D:/3. Original Data-Hypertension/01022018_S029_S030/DICOM/IM_0046_kspace_resampled_mag.dcm',#13
        'D:/3. Original Data-Hypertension/07022018_S031_S032/DICOM/IM_0059_kspace_resampled_mag.dcm',#14
        'D:/3. Original Data-Hypertension/11012018_S025_S026/DICOM/IM_0062_kspace_resampled_mag.dcm',#15
        'D:/3. Original Data-Hypertension/18012018_S027_S028/DICOM/IM_0071_kspace_resampled_mag.dcm',#16
        ]
#Fulldcmname index per scan is given below. Only the unique scans go in FULLdcmname
Corresponding_Full_DCM_idx = np.array([0, 0, 1, 1, 2, 2, 3, 4, 4, 5, 5, 6, 7, 8, 9, 10, 12, 10, 11, 11, 13, 13, 14, 14, 15, 16, 16, 6])

ANTSdir = '"C:\\Program Files\\ANTs\\Release\\N4BiasFieldCorrection"'
datadir = 'D:/5. NN/3. Data/'
resultdir = 'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/4. Results/'
loadresults = ''
global nrofbestnetworks, dateandtime, nrofsample, targetvoxelsize, sampledata, patchsz, excludelargerthan, selectionrange, num_epochs, optionsdict, lr, blurstrength, epochdif, excludesmallerthan, trainset, trainvalratio, testset, mailupdatefrequency, mailadress

'''
nrofsample:           Number of samples for training validation and test sets. Numpy array.
targetvoxelsize:      Target voxelsize of downsampled image
sampledata:           If 1, samples are sampled regardless of existence of sample .p file
patchsz:              Size of the patches in voxels. Can be non isotropic.
excludelargerthan:    exclude voxels with thickness larger/smaller than excludelargerthan/excludesmallerthan. 
excludesmallerthan:   To turn off, set it at np.inf/0 respectively.
num_epochs:           Max number of epochs the network can be trained
selectionrange:       Additional range that can be chosen, for which extra statistics, such as mean deviation, iqr and stdev are calculated. To turn off set to numpy.array([0,numpy.inf])
learningrate:         learning rate
blurstrength:         blur strength of Gaussian, used when downsampling. Prevents artifacts caused by downsampling      
scanorder:            List of all ROIdcmnames.
trainset:             List of numbers corresponding to the order of FULLdcmname list
valset:               List of numbers corresponding to the order of FULLdcmname list
testset:              List of numbers corresponding to the order of FULLdcmname list
batchsize:            Batchsize, for neural network optimization
errorfunction:        if 0 use MSE, if 1 use MAE, if 2 use weighted MSE, if 3 use weighted MAE. 
alpha, beta:          Target values larger than alpha, get weight beta in the error function. Values below alpha, get weight 1
trainvalratio:        The ratio of dividing, training and validation specimens. Set to 2, 66% of the specimens will be randomly assigned to Training.
trainvalperms:        Number of times the network is run, with a different training-validation specimen assignment.
nrofbestnetworks:     Number of networks that are linearly combined to create an ensemble learner. If trainvalperms=10 and nrofbestnetworks=3, the three networks with the lowest validation error are ensembled to the final network.
'''
   
nrofsample = np.array([300000,100000,np.inf])                  # nr of sample patches taken
targetvoxelsize = 0.4004001                                     # set to 0 if you dont want any fancy stuff happening4
sampledata = 0                                                 # if 1, samples are sampled regardless of existence of .p file
dateandtime = ''                                               # used to load in existing samples. String in the format '2018-07-09_09;04;42'
patchsz = 15                                                  # size of the patches in pixels
excludelargerthan = 1.4                                       # exclude voxels with thickness larger/smaller than excludelargerthan/excludesmallerthan. 
excludesmallerthan = 0.25                                       # To turn off, set it at np.inf/0 respectively.
num_epochs = 1000                                              # max number of epochs
selectionrange = np.array([0.3,0.4])                           # Select thickness range voxels for analysis
lr = 0.00001                                                     # learning rate
blurstrength = 0.5                                             # blur strength of Gaussian, used when sampling. Doesnt work if targetvoxelsize = 0
epochdif = 50
batchsize = 16
errorfunction = 0
trainset=np.array([1,2,3,4,5,6,8,9,11,12,15,16,17,19,21,22,24,25,26])   
testset=np.array([0,7,10,13,14,18,20,23,27])
valset=''
trainvalratio = 2
trainvalperms = 1
nrofbestnetworks = 1
optionsdict = {'nrofsample':nrofsample,'targetvoxelsize':targetvoxelsize,'sampledata':sampledata, 'sampledateandtime': dateandtime,'patchsz':patchsz,'excludesmallerthan':excludesmallerthan,'excludelargerthan':excludelargerthan,'num_epochs':num_epochs,'selectionrange':selectionrange,'learningrate':lr, 'blurstrength':blurstrength, 'scanorder':ROIdcmname, 'trainset':trainset, 'valset':valset, 'testset':testset,'batchsize':batchsize, 'trainvalratio':trainvalratio, 'trainvalperms':trainvalperms, 'nrofbestnetworks':nrofbestnetworks}
mailupdatefrequency = trainvalperms
mailadress = 'keesvanhespen@hotmail.com'
comments = 'different normalized s029-30 '


#do not edit values below
chosennetworkindices = -1

if not os.path.exists(resultdir):
    os.makedirs(resultdir)

  
def load_dataset():
    # We first define a download function, supporting both Python 2 and 3. 
    global dateandtime, curtime, sourcevoxelsize
    thicknessmaplist = []
    roi_to_full_wmat = []
    tic = time.clock();
    biglist = []
    imgsize=[]
    allusedspecimens=np.sort(np.append(trainset,testset))
    allusedspecimens_unique = np.unique(Corresponding_Full_DCM_idx[allusedspecimens])
    scannamelist = [FULLdcmname[i] for i in Corresponding_Full_DCM_idx[allusedspecimens]]  
    scannamelist_unique = [FULLdcmname[i] for i in allusedspecimens_unique]  
    #pixdat_norm = [np.zeros((1000,1000,500),dtype='float32')]*allusedspecimens.size
    #read thickness maps and store them in a list
    if dateandtime=='':
        dateandtime=datetime.datetime.now().strftime('%Y-%m-%d_%H;%M;%S')
        curtime = dateandtime
        
    if sampledata or not os.path.isfile(os.path.join(datadir, 'samples_'+ str(targetvoxelsize)+'_acq_'+str(trainvalperms-1)+'_val_'+str(dateandtime)+'_LR.p')):
        for idx_un,scan_un in zip(allusedspecimens_unique,scannamelist_unique):
             if not os.path.isfile(scan_un.replace('.dcm','') + '_lumencorrected_bgcorrected.nii'):
             # normalization could be better with more well defined masks. Use the mevislab tool in script Hessian_vessel_tracking-. ctrl+R -> normalize selected image
                 normalize_input(scan_un.replace('.dcm',''),ANTSdir)
             readimage = dicom.read_file(scan_un).pixel_array.shape
             imgsize.append(np.array([readimage[2], readimage[1], readimage[0]]))
             readimage = None
        imgsize = [imgsize[i] for i in Corresponding_Full_DCM_idx[allusedspecimens]] ## image size in Z is incorrect due to addition of the phase images?
        imgsize = np.squeeze(np.array([imgsize]))  
    
        for idx, scan in zip(allusedspecimens,scannamelist):
            print('Loading scan, with index: ' + str(idx))    
           
            try:
                resultshr = sio.loadmat(ROIresultsname[idx])
            except:
                f = h5py.File(ROIresultsname[idx],'r')
                resultshr = f['results']
                
            mask = sio.loadmat(maskname[idx])['mask']
            try:
                try:
                    thmap = np.array(resultshr['results']['thicknessmapselection'][0][0])
                except:
                    thmap = np.array(resultshr['results']['thicknessmap_selection'][0][0])
            except:
                try:
                    thmap = np.array(resultshr['thicknessmapselection']) 
                except:
                    thmap = np.array(resultshr['thicknessmap_selection'])
            resultshr=''
            thmap[np.isinf(thmap)] = 0
            thmap[np.isnan(thmap)] = 0
            thmap[thmap>excludelargerthan] = 0
            thmap[thmap<excludesmallerthan] = 0
            try:
                thicknessmaplist.append(np.transpose(thmap * mask, (1, 0, 2)))
                maskedthick=np.transpose(thmap * mask,(1, 0, 2))
            except:
                thicknessmaplist.append(np.transpose(np.transpose(thmap,(2,1,0)) * mask, (1, 0, 2)))   
                maskedthick=np.transpose(np.transpose(thmap,(2,1,0)) * mask, (1, 0, 2))
            biglist.append(np.concatenate((maskedthick[np.nonzero(maskedthick)][..., None], np.array(np.nonzero(maskedthick)).T, idx * np.ones((maskedthick[np.nonzero(maskedthick)].shape[0], 1))), axis = 1))
            #compute world transform matrix
            WtransmatROI = gettransformmatrix(dicom.read_file(ROIdcmname[idx]))
            WtransmatFULL= gettransformmatrix(dicom.read_file(FULLdcmname[Corresponding_Full_DCM_idx[allusedspecimens[idx]]]))
            roi_to_full_wmat.append(np.dot(np.linalg.inv(WtransmatFULL), WtransmatROI))
        #assuming equal pixel spacings in all images
        info = dicom.read_file(FULLdcmname[Corresponding_Full_DCM_idx[allusedspecimens[0]]])
        try:
            T1 = np.array(info.ImagePositionPatient)
        except:
            instance = 0
            foundfirstslice=0
            while not foundfirstslice:
                if info.PerFrameFunctionalGroupsSequence[instance].FrameContentSequence[0].InStackPositionNumber==1:
                    foundfirstslice=1
                else:
                    instance +=1
        try:
            pixelspacing = info.PixelSpacing
        except:
            pixelspacing = info.PerFrameFunctionalGroupsSequence[instance].PixelMeasuresSequence[0].PixelSpacing
        dx = pixelspacing[0]
        dy = pixelspacing[1]
        dz = info.SpacingBetweenSlices
        sourcevoxelsize = np.array([dx,dy,dz])
        thicknessmaplist =(np.vstack(biglist)).astype('float32')
        biglist=''
        
    toc = time.clock()
    print(toc - tic)
    return thicknessmaplist, scannamelist, roi_to_full_wmat, imgsize
   

def sampling_wrapper(thicknessmaplist, scannamelist, imgsize, roi_to_full_wmat):
    global patchsz, curtime, dateandtime, sourcevoxelsize,targetvoxelsize
    
    if targetvoxelsize==0:
        targetvoxelsize=sourcevoxelsize
    if networkno>0:
        nameset = ['_train','_val']
    else:
        global X_testhold, y_testhold
        nameset = ['_train','_val','_test']
    mx, mn = np.array(0), np.array(1000)
    if sampledata or not os.path.isfile(os.path.join(datadir, 'samples_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+'_val_'+dateandtime+'_LR.p')):
        for sampleset in nameset:
            samples=[]
            labels=[]
            scannamelist_current=[]
            if sampleset=='_train':
                samplesetindices = trainsubset
                nrofsampleindex=0
                doaugment=1
            elif sampleset=='_val':
                samplesetindices = valsubset
                nrofsampleindex=1
                if nrofsample[nrofsampleindex]==np.inf:
                    doaugment=0
                else:
                    doaugment=1
            elif sampleset=='_test':
                samplesetindices = testset
                nrofsampleindex=2
                doaugment=0
            
           # for scanidx in samplesetindices:    
           #     imgsize_current.append(np.array(pixdat_norm[scanidx].shape))
          #  imgsize_current = np.squeeze(np.array([imgsize_current]))   
            issamplearray = np.zeros((1,thicknessmaplist.shape[0]))
            imgsize_current = []
            for i in samplesetindices:
                imgsize_current.append(imgsize[Corresponding_Full_DCM_idx[i]])
                issamplearray+=thicknessmaplist[...,4]==i
                scannamelist_current.append(scannamelist[i])
            issamplearray = np.transpose(issamplearray,(1,0))>0  
            thicknessmap_current = thicknessmaplist[np.squeeze(issamplearray),...]
            
            # sample samples or load data if sample file exists
            samples, labels, patchorigin = samplingfunction(thicknessmap_current, scannamelist_current, imgsize_current, nrofsample[nrofsampleindex], targetvoxelsize, sourcevoxelsize, patchsz, roi_to_full_wmat, '',datetime.datetime.now(), doaugment)
            samples = np.array(np.transpose(samples[..., np.newaxis], (0, 4, 1, 2, 3)).astype('f'))
            labels = np.squeeze(labels.astype('f'))


            data = {'samples':samples, 'labels':labels, 'options':optionsdict}
            pickle.dump(data,open(os.path.join(datadir, 'samples_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+ sampleset + '_'+dateandtime+'_LR.p'), 'wb'), protocol = 4)
            data=[]
            optionsdict['trainset'] = trainsubset
            optionsdict['valset'] = valsubset

            np.save(os.path.join(datadir, 'samples_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+sampleset+'_'+dateandtime+'_LR'), samples)
            np.save(os.path.join(datadir, 'samples_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+'_opts'+sampleset+ '_'+dateandtime+'_LR'), optionsdict)
            np.save(os.path.join(datadir, 'samples_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+sampleset+'coords+origin_'+dateandtime+'_LR.npy'), patchorigin)
            # sort train to training set, val to validation set, etc.
            if sampleset=='_train':
                X_train, y_train = samples, labels
            elif sampleset=='_val':
                X_val, y_val = samples, labels
            elif sampleset=='_test':
                X_testhold, y_testhold = samples, labels
    else:
        for sampleset in nameset:
            loadsamples = pickle.load(open(os.path.join(datadir, 'samples_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+sampleset + '_' + dateandtime + '_LR.p'), 'rb'))
            loadsamplesopts = np.load(os.path.join(datadir, 'samples_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+'_opts'+ sampleset +'_'+dateandtime + '_LR.npy')).item()
            samples = loadsamples['samples'].astype('f')
            patchsz = samples.shape[-1]
            optionsdict['nrofsample'] = loadsamplesopts['nrofsample']
            try:
                optionsdict['excludesmallerthan'] = loadsamplesopts['excludesmallerthan']
            except:
                optionsdict['excludesmallerthan'] = 0
            optionsdict['targetvoxelsize'] = loadsamplesopts['targetvoxelsize']
            optionsdict['excludelargerthan'] = loadsamplesopts['excludelargerthan']
            optionsdict['trainset'] = loadsamplesopts['trainset']
            optionsdict['valset'] = loadsamplesopts['valset']
            optionsdict['testset'] = loadsamplesopts['testset']
            optionsdict['patchsz'] = patchsz
            
            labels = np.squeeze(loadsamples['labels'].astype('f'))
            
            if sampleset=='_train':
                X_train, y_train = samples, labels
            elif sampleset=='_val':
                X_val, y_val = samples, labels
            elif sampleset=='_test':
                X_testhold, y_testhold = samples, labels
            mx = np.append(mx,np.max(labels))
            mn = np.append(mn,np.min(labels))
    if 0:                
        #https://stats.stackexchange.com/questions/26144/how-to-get-real-valued-continous-output-from-neural-network
        y_test = 8*(y_testhold-np.min(mn))/(np.max(mx)-np.min(mn))-4
        y_val = 8*(y_val-np.min(mn))/(np.max(mx)-np.min(mn))-4
        y_train =8*(y_train-np.min(mn))/(np.max(mx)-np.min(mn))-4
        
        y_test = 1/(1+np.exp(-y_test))
        y_val = 1/(1+np.exp(-y_val))
        y_train = 1/(1+np.exp(-y_train))
    y_test = y_testhold
    X_test = X_testhold
    #data normalization
    #X_train=(X_train - np.mean(sampleshold)) / (np.std(sampleshold) + 1*10E-8)
    #X_test=(X_test - np.mean(sampleshold)) / (np.std(sampleshold) + 1*10E-8)
    #X_val=(X_val - np.mean(sampleshold)) / (np.std(sampleshold) + 1*10E-8) 
    # We can now download and read the training and test set images and labels.
   
    
    # We just return all the arrays in order, as expected in main().
    # (It doesn't matter how we do this as long as we can read them again.)
    return X_train, y_train, X_val, y_val, X_test, y_test    

def samplingfunction(thicknessmap, scannamelist, imgsize, nrofsamples, target_voxelsize, source_voxelsize, patchsize, roi_to_full_wmat, angle='', seed=datetime.datetime.now(), doaugment=1):
    """
    ########################################
    #                                      #
    #   Samples patches equally over the   #
    #    range of possible thicknesses     #
    #                                      #
    ########################################
    INPUT: thicknessmap:     can be a list of nD thickness maps. If input is a numpy array, sampling is equalized accross the range of thickness values given in.
                             If a numpy array is given, first three columns represent the x,y,z coordinates. The fourth column is the thickness values, the fifth is the number corresponding to the scan number
           scannamelist      List containing all scan names corresponding to the unique sorted scan numbers in thicknessmap
           nrofsamples:      number of samples that need to be sampled
           target_voxelsize: Target voxelsize. If int, is assumed to be isotropic in all directions
           source_voxelsize: Source voxelsize. If int, is assumed to be isotropic in all directions
           patchsize:        patchsize. If int, patchsize is assumed same in all directions. Value should be uneven!
           roi_to_full_wmat: World coordinate transformation matrix, from roi coordinates (thicknessmap) to full image coordinates. Only applicable if the thickness maps are ROIs within the images from scannamelist. Give indentity matrix if both images are in same space.
           angle:            Sampling angle. If not given, samples are sampled with random angle
           seed:             optional seed for angle generation
           doaugment:        If doaugment==1, make rotated patches, if 0, do not rotate
         
           
    OUTPUT:samples:         Numpy array containing all samples
           labels:          Numpy array containing all labels
           
    """
    # Set variables   
    random.seed(seed)
    target_voxelsize = np.array(target_voxelsize)
    source_voxelsize = np.array(source_voxelsize)
    patchsize = np.array(patchsize)
    patches = []
    infinitesamples = 0
    if nrofsamples==np.inf:
        infinitesamples = 1
    nrofsamples = nrofsamples.astype('int')
    if target_voxelsize.size == 1:
        target_voxelsize = np.array([target_voxelsize, target_voxelsize, target_voxelsize])
    if source_voxelsize.size == 1:
        source_voxelsize = np.array([source_voxelsize, source_voxelsize, source_voxelsize])
    if patchsize.size == 1:
        patchsize = np.array([patchsize, patchsize, patchsize])
        
    #generate samples from all non zero elements in the HR image
    scanindices = np.unique(thicknessmap[..., 4]).astype('int')
    scancoords = (thicknessmap[..., 1:4][thicknessmap[..., 4] == scanindices[0]]).astype('uint16')
    large_image_coords =  [np.round(np.dot(roi_to_full_wmat[scanindices[0]], np.concatenate((np.array(scancoords.T), np.ones((1, np.array(scancoords.T).shape[1]))))))[:3, ...]]

    for index in range(1,len(scanindices)):
        scancoords = (thicknessmap[..., 1:4][thicknessmap[...,4] == scanindices[index]]).astype('uint16')
        large_image_coords.append(np.round(np.dot(roi_to_full_wmat[scanindices[index]], np.concatenate((np.array(scancoords.T), np.ones((1, np.array(scancoords.T).shape[1]))))))[:3, ...])
    large_image_coords = (np.hstack(large_image_coords)).astype('uint16')


    #get thickness values as labels from the selected samples
    equalize=1
    if infinitesamples:
        equalize=0
        nrofsamples=thicknessmap.shape[0]
    labels = np.zeros((nrofsamples, 1),dtype='float32')
    patch = np.zeros((nrofsamples, patchsize[0], patchsize[1], patchsize[2]),dtype='float32')
    patchorigin = []
    patchoriginindex = np.squeeze(np.zeros((nrofsamples,1),dtype='int32'))-1
    patches=np.zeros((nrofsamples,3,patchsize[0],patchsize[1],patchsize[2]),dtype='float32')
    samples = 0

    print('Sampling patches...')
    random.seed(datetime.datetime.now())
    #do the sorting beforehand, may save time
    bins = 15
    binsize=(np.max(thicknessmap[:, 0]) - np.min(thicknessmap[:, 0])) / bins
    binnedthicknesses=[]
    for curbin in range(0,bins):
        binnedthicknesses.append(thicknessmap[np.logical_and(thicknessmap[:, 0] >= (binsize*curbin+np.min(thicknessmap[:, 0])), thicknessmap[:, 0] <= (binsize*(curbin + 1) + np.min(thicknessmap[:, 0])))])
    got_here=0
    while samples < nrofsamples:
        try:
            if equalize:
                usebin = random.randint(0, bins - 1)
                randidx = np.random.choice(np.where(np.invert(np.isinf(binnedthicknesses[usebin][:,0]))==True)[0])
                selected_sample = binnedthicknesses[usebin][randidx,:]
                randomthvalue = selected_sample[0]
                if randomthvalue==np.inf:
                    print('Got an inf value somewhere')
                randomsampleindices = np.squeeze(np.equal(selected_sample,thicknessmap))
                randomsampleindices = np.where(np.sum(randomsampleindices,1)==selected_sample.size)[0][0]
            else:
                randomsampleindices =samples
                
            samplecoords = np.squeeze(large_image_coords[..., randomsampleindices].T)
            usemap = thicknessmap[randomsampleindices, 4].astype(int)
                
            sampletry = 0
        except Exception:
            print('somehow got here. Not good. Probably rerun')
            break
        
        # test sample x times with a random orientation. If voxels from the patch fall outside the image we eventually discard the voxel.
        while sampletry < 10:
            if angle == '' and doaugment==1:
                sampleangle = np.array([random.randrange(0, 360), random.randrange(0, 360), random.randrange(0, 360)])
            elif angle == '' and not doaugment==1:
                sampleangle = np.array([0,0,0])
            else:
                sampleangle = angle
                if isinstance(sampleangle, int):
                    sampleangle=np.array([sampleangle,sampleangle,sampleangle])
        
            x = np.linspace(-target_voxelsize[0] / source_voxelsize[0] * (patchsize[0] / 2 - 0.5) + samplecoords[0], target_voxelsize[0] / source_voxelsize[0] * (patchsize[0] / 2 - 0.5) + samplecoords[0], patchsize[0])
            y = np.linspace(-target_voxelsize[1] / source_voxelsize[1] * (patchsize[1] / 2 - 0.5) + samplecoords[1], target_voxelsize[1] / source_voxelsize[1] * (patchsize[1] / 2 - 0.5) + samplecoords[1], patchsize[1])
            z = np.linspace(-target_voxelsize[2] / source_voxelsize[2] * (patchsize[2] / 2 - 0.5) + samplecoords[2], target_voxelsize[2] / source_voxelsize[2] * (patchsize[2] / 2 - 0.5) + samplecoords[2], patchsize[2])
            
            patchcoords = np.array(np.meshgrid(x, y, z))
            patchcoordszerocentered = patchcoords - samplecoords[..., None, None, None].astype(int)
            
            patchcoordsrotatedzerocentered = np.dot(np.concatenate((patchcoordszerocentered, np.zeros([1, len(x), len(y), len(z)])), 0).T, rotation_matrix(1, sampleangle)).T
            patchcoordsrotated = patchcoordsrotatedzerocentered[0:3, ...] + samplecoords[..., None, None, None].astype(int)

            sampletry += 1
            # save patch coordinates that fall into image
            if 0 or not any(singlecoord < 0 for singlecoord in np.array([np.min(patchcoordsrotated[0, ...]), np.min(patchcoordsrotated[1, ...]), np.min(patchcoordsrotated[2, ...])])) and not any(np.array([np.max(patchcoordsrotated[0, ...]), np.max(patchcoordsrotated[1, ...]), np.max(patchcoordsrotated[2, ...])]) - imgsize[np.where(scanindices==usemap)[0][0]] > 0):
                patches[samples,:,:,:,:]=patchcoordsrotated[np.newaxis,np.newaxis,...]
                patchorigin.append(usemap)
                sampletry = np.inf;
                patchoriginindex[samples]=randomsampleindices
                samples += 1
        # delete coordinate that fails to fit a patch (doesnt update the weights. should do that)        

        if not sampletry == np.inf:
            got_here+=1
            if got_here<20:
                print('Getting here shouldnt occur. For this voxel, measured in the ROI, falls out of bounds in the large dicom')
                print(str(samples))
            if doaugment:
                binnedthicknesses[usebin][randidx,0]=np.inf
            else:
                large_image_coords = np.delete(large_image_coords, randomsampleindices, 1)
                thicknessmap = np.delete(thicknessmap, randomsampleindices, 0)
         
    print("Number of deleted samples: "+str(got_here)) 
    # save original center coords and scannr
    #selectedoriginalcoords = thicknessmap[patchoriginindex]
    #np.save(os.path.join(datadir, 'samples' + str(targetvoxelsize) + '_origcoords.npy'), selectedoriginalcoords)
    binnedthicknesses=''
    large_image_coords=''
    patchoriginindex = patchoriginindex[:samples+1]
    patches=patches[0:samples+1,...]
    patch = patch[0:samples+1,...]
    # get thickness label and patch intensity values`
    print('Interpolating patch intensity values...')    
    for scan in range(len(scanindices)):
         print('Sampling from scan ' + str(scan + 1) + ' out of ' + str(len(scanindices)))      
         selectedpatches = np.transpose(np.array(np.where(np.array(patchorigin) == scanindices[scan])))
         selectedpatchescoords = np.squeeze(patches[selectedpatches])
         if len(selectedpatchescoords.shape)<5:
             selectedpatchescoords = selectedpatchescoords[np.newaxis,...] 
         if selectedpatches.size:
             niioutputImage = nib.load(scannamelist[scan].replace('.dcm','')+ '_lumencorrected_bgcorrected.nii') 
             loadedscan = niioutputImage.get_data()
             niioutputImage=''
             #plt.figure()
             #plt.imshow(curimage[..., 150])
             patch[selectedpatches[:, 0]] = scipy.ndimage.interpolation.map_coordinates(loadedscan,
                                                                            np.array([np.array(selectedpatchescoords)[:, 0, ...], np.array(selectedpatchescoords)[:, 1, ...], np.array(selectedpatchescoords)[:, 2, ...]]), order=3)

         

         labels = thicknessmap[:, 0][patchoriginindex]
     
    print('Sampling complete!')
    return patch, np.array(labels), thicknessmap[patchoriginindex]
            
'''
# ##################### Build the neural network model #######################
# This script supports three types of models. For each one, we define a
# function that takes a Theano variable representing the input and returns
# the output layer of a neural network model built in Lasagne.
'''
def build_mlp(input_var = None):
    # This creates an MLP of two hidden layers of 800 units each, followed by
    # a softmax output layer of 10 units. It applies 20% dropout to the input
    # data and 50% dropout to the hidden layers.

    # Input layer, specifying the expected input shape of the network
    if 1:
        # Input layer, specifying the expected input shape of the network
        network = lasagne.layers.InputLayer(shape = (None, 1, patchsz, patchsz, patchsz),
                                         input_var = input_var)
        shortcut1= network
        
        #first parallel layers
        # Convolution and dropout
        network = lasagne.layers.Conv3DLayer(network, 16, 5, 1, nonlinearity = lasagne.nonlinearities.linear)
        network = lasagne.layers.prelu(network)
        network = lasagne.layers.DropoutLayer(network, p = 0.0)  
        
        # Convolution and dropout
        network = lasagne.layers.Conv3DLayer(network, 32, 3, 1, nonlinearity = lasagne.nonlinearities.linear)
        network = lasagne.layers.prelu(network)
        network = lasagne.layers.DropoutLayer(network, p = 0.5)  
    
        # Convolution and dropout
       # network2 = lasagne.layers.Conv3DLayer(network, 64, 3, 1, nonlinearity = lasagne.nonlinearities.linear)
       # network = lasagne.layers.DropoutLayer(network2, p = 0.0)    
       
        # Convolution and dropout
        network = lasagne.layers.Conv3DLayer(network, 64, 3, 1, nonlinearity = lasagne.nonlinearities.linear) 
        network = lasagne.layers.prelu(network)
        network = lasagne.layers.DropoutLayer(network, p = 0.0)   
    
        # Convolution and dropout
        network = lasagne.layers.Conv3DLayer(network, 128, 3, 1, nonlinearity = lasagne.nonlinearities.linear)
        network = lasagne.layers.prelu(network)
        network = lasagne.layers.DropoutLayer(network, p = 0.5)
        
        #Finishing up with a few fully connected layers.
        network = lasagne.layers.DenseLayer(network, num_units=200,
                nonlinearity = lasagne.nonlinearities.linear,
                W=lasagne.init.GlorotUniform())
        network = lasagne.layers.prelu(network)
        network = lasagne.layers.DropoutLayer(network, p = 0.5)    
    
        #second parallel layers
        # Convolution and dropout
        if 1:
            network2 = lasagne.layers.Conv3DLayer(shortcut1, 16, 3, 1, nonlinearity = lasagne.nonlinearities.linear)
            network2 = lasagne.layers.prelu(network2)
            network2 = lasagne.layers.DropoutLayer(network2, p = 0.0)  
            
            # Convolution and dropout
            network2 = lasagne.layers.Conv3DLayer(network2, 32, 3, 1, nonlinearity = lasagne.nonlinearities.linear)
            network2 = lasagne.layers.prelu(network2)
            network2 = lasagne.layers.DropoutLayer(network2, p = 0.5)  
        
            # Convolution and dropout
            network2 = lasagne.layers.Conv3DLayer(network2, 64, 3, 1, nonlinearity = lasagne.nonlinearities.linear)
            network2 = lasagne.layers.prelu(network2)
            network2 = lasagne.layers.DropoutLayer(network2, p = 0.0)    
           
            # Convolution and dropout
            network2 =lasagne.layers.Conv3DLayer(network2, 64, 3, 1, nonlinearity = lasagne.nonlinearities.linear) 
            network2 = lasagne.layers.prelu(network2)
            network2 = lasagne.layers.DropoutLayer(network2, p = 0.0)   
        
            # Convolution and dropout
            #network2 =lasagne.layers.Conv3DLayer(network2, 128, 3, 1, nonlinearity = lasagne.nonlinearities.linear)
            #network2 = lasagne.layers.prelu(network2)
            #network2 = lasagne.layers.DropoutLayer(network2, p = 0.5)
            network2 = lasagne.layers.DenseLayer(network2, num_units=200,
                    nonlinearity = lasagne.nonlinearities.linear,
                    W=lasagne.init.GlorotUniform())
            network2 = lasagne.layers.prelu(network2)
            network2 = lasagne.layers.DropoutLayer(network2, p = 0.5) 
            
            network4 = lasagne.layers.ConcatLayer([network,network2])
            network4 = lasagne.layers.DenseLayer(network4, num_units=200,
                        nonlinearity = lasagne.nonlinearities.linear,
                        W=lasagne.init.GlorotUniform())
            network4 = lasagne.layers.prelu(network4)
            network4 = lasagne.layers.DropoutLayer(network4, p = 0.5)
        
        # Each layer is linked to its incoming layer(s), so we only need to pass
        # the output layer to give access to a network in Lasagne:
        network4 = lasagne.layers.DenseLayer(
                network4, num_units = 1, nonlinearity = lasagne.nonlinearities.linear,
                W=lasagne.init.GlorotUniform())
    return network4



"""
# ############################# Batch iterator ###############################
# This is just a simple helper function iterating over training data in
# mini-batches of a particular size, optionally in random order. It assumes
# data is available as numpy arrays. For big datasets, you could load numpy
# arrays as memory-mapped files (np.load(..., mmap_mode='r')), or write your
# own custom data iteration function. For small datasets, you can also copy
# them to GPU at once for slightly improved performance. This would involve
# several changes in the main program, though, and is not demonstrated here.
# Notice that this function returns only mini-batches of size `batchsize`.
# If the size of the data is not a multiple of `batchsize`, it will not
# return the last (remaining) mini-batch.
"""
def iterate_minibatches(inputs, targets, batchsze, shuffle = False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsze + 1, batchsze):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsze]
        else:
            excerpt = slice(start_idx, start_idx + batchsze)
        yield inputs[excerpt], targets[excerpt]


def rotation_matrix(axis, theta):
    """np.array(patches[1:100])[:,2,...].shape
    Return the rotation matrix associated with counterclockwise rotation about
    the given axis by theta radians.
    """
    theta = np.array(theta) / 360 * np.pi
    if len(np.array(theta)) == 1:
        theta = np.array([theta, theta, theta])
        
    rotationmatrix_x = np.array([[np.cos(theta[0]), -np.sin(theta[0]), 0, 0], [np.sin(theta[0]), np.cos(theta[0]), 0, 0],[0, 0, 1, 0], [0, 0, 0, 1]])
    rotationmatrix_y = np.array([[1, 0, 0, 0], [0, np.cos(theta[1]), -np.sin(theta[1]), 0], [0, np.sin(theta[1]), np.cos(theta[1]), 0], [0, 0, 0, 1]])
    rotationmatrix_z = np.array([[np.cos(theta[2]), 0, np.sin(theta[2]), 0], [0, 1, 0, 0], [-np.sin(theta[2]), 0, np.cos(theta[2]), 0], [0, 0, 0, 1]])
    
    rotationmatrix = np.dot(np.dot(rotationmatrix_x, rotationmatrix_y), rotationmatrix_z)
    return np.array(rotationmatrix)


"""
# ############################## Main program ################################
# Everything else will be handled in our main program now. We could pull out
# more functions to better separate the code, but it wouldn't make it any
# easier to read.
"""
def main(nrofsample = 10000, targetvoxelsize = 0.8, sampledata = 0, sampledateandtime = '',patchsz = 17, excludelargerthan = np.inf, num_epochs = 75, selectionrange = np.array([0.35,0.45]), learningrate = 0.0005, blurstrength = 0.5, excludesmallerthan = 0, scanorder='',trainset='', valset='', testset='',batchsize=256, errorfunction = 0, trainvalratio=2, trainvalperms=1, nrofbestnetworks = 1):
    # Load the dataset
        print("Loading data...")
        global networkno, networklist, chosennetworkindices, mailadress, comments
        networkerrors=np.zeros((trainvalperms,1))
        networklist = []
        curtime = datetime.datetime.now().strftime('%Y-%m-%d_%H;%M;%S')
        thicknessmaplist, scannamelist, roi_to_full_wmat, imgsize = load_dataset()      
        mailupdatelist = list(range(0,trainvalperms+1,mailupdatefrequency))
        mailupdatelist = mailupdatelist[1:]
        for networkno in range(0, trainvalperms):
            print('Network: '+str(networkno))
            
            trainpreps = np.int(np.round(trainset.size/(1+ trainvalratio)))
            perm = np.random.permutation(trainset)
            global trainsubset, valsubset
            valsubset = np.array(np.sort(perm[0:trainpreps]))
            trainsubset = np.array(np.sort(perm[trainpreps+1:]))

            # load data
            
            # get samples
            X_train, y_train, X_val, y_val, X_test, y_test = sampling_wrapper(thicknessmaplist, scannamelist, imgsize, roi_to_full_wmat)
            # Prepare Theano variables for inputs and targets
            input_var = T.tensor5('inputs',dtype='float32')
            target_var = T.fvector('targets')
            # Create neural network model (depending on first command line parameter)
            print("Building model and compiling functions...")
            if loadresults:
                network = build_mlp(input_var)
                #network_arch = np.load(os.path.join(resultdir, 'network_vsize_' + loadresults +'.npy'))
                #network = construct_network(input_var,network_arch)
                
            else:
                network = build_mlp(input_var)
            #holdweights = np.load(os.path.join(resultdir,'network_weights_vsize_'+'0.8001_0_2018-07-24_10;24;37'.replace("_0_","_"+str(networkno)+"_")+'.npy'))
            #lasagne.layers.set_all_param_values(network, holdweights)            
            global networkstring
            networkstring=[(layer.__class__, layer.__dict__) for layer in lasagne.layers.get_all_layers(network)]
        
            # Create a loss expression for training, i.e., a scalar objective we want
            # to minimize (for our multi-class problem, it is the cross-entropy loss):
            prediction = lasagne.layers.get_output(network)
            test_prediction = lasagne.layers.get_output(network, deterministic = True)
            if errorfunction==0:
                loss = mean_squared_error(prediction, target_var)
                test_loss = mean_squared_error(test_prediction, target_var)
            elif errorfunction ==1:
                loss = mean_absolute_error(prediction, target_var)
                test_loss = mean_absolute_error(test_prediction, target_var)
            elif errorfunction==2:
                loss = weighted_squared_error(prediction, target_var)
                test_loss = weighted_squared_error(test_prediction, target_var)
            elif errorfunction==3:
                loss = weighted_absolute_error(prediction, target_var)
                test_loss = weighted_absolute_error(test_prediction, target_var)
            loss = loss.mean()
            test_loss = test_loss.mean()
            # We could add some weight decay as well here, see lasagne.regularization.
        
            # Create update expressions for training, i.e., how to modify the
            # parameters at each training step. Here, we'll use Stochastic Gradient
            # Descent (SGD) with Nesterov momentum, but Lasagne offers plenty more.
            params = lasagne.layers.get_all_params(network, trainable = True)
            updates = lasagne.updates.adam(
                    loss, params, learning_rate = lr)
        
            # Create a loss expression for validation/testing. The crucial difference
            # here is that we do a deterministic forward pass through the network,
            # disabling dropout layers.

            # As a bonus, also create an expression for the classification accuracy:
            test_acc = T.mean(T.eq(T.argmax(test_prediction, axis = 1), target_var),
                              dtype = theano.config.floatX)
            
    
        
            # Compile a function performing a training step on a mini-batch (by giving
            # the updates dictionary) and returning the corresponding training loss:
            
            train_fn = theano.function([input_var, target_var], loss, updates = updates)
        
            # Compile a second function computing the validation loss and accuracy:
            val_fn = theano.function([input_var, target_var], [test_loss, test_acc])
            pred_fn = theano.function([input_var], test_prediction)
            valset = np.load(os.path.join(datadir, 'samples_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+'_valcoords+origin_'+dateandtime+'_LR.npy'))
            testset = np.load(os.path.join(datadir, 'samples_'+ str(targetvoxelsize)+'_acq_0_testcoords+origin_'+dateandtime+'_LR.npy'))
            # Finally, launch the training loop. If you want to load some results, only start the validation loop.
            if loadresults and os.path.isfile(os.path.join(resultdir,'network_weights_vsize_'+ str(targetvoxelsize)+'_acq_'+loadresults.replace("_0_",str(networkno)+"_")+'LR.npy')):
                holdepoch = 0
                epochhold=np.array([0])
                print("Starting testing...")
                holdweights = np.load(os.path.join(resultdir,'network_weights_vsize_'+ str(targetvoxelsize)+'_acq_'+loadresults.replace("_0_",str(networkno)+"_")+'LR.npy'))
                lasagne.layers.set_all_param_values(network, holdweights)
                val_err = 0
                val_batches = 0 
                for batch in iterate_minibatches(X_val, y_val, batchsize, shuffle = False):
                    inputs, targets = batch
                    err, acc = val_fn(inputs, targets)
                    val_err += err
                    val_batches += 1

               # Check if validation error is lower than that of previous iteration
                networkerrors[networkno] = val_err / val_batches
         
            else:
                print("Starting training...")
                # We iterate over epochs:
                global valerror
                valerror = []
                trainerrorhold = []
                epochhold = []
                meandifhold = []
                medshold = []
                stdevhold  = []
                iqrrhold = []
                meandifrnghold = []
                medsrnghold = []
                stdrnghold = []
                iqrrnghold = []
                targetsvalhold = []
                predsvalhold = []
                epoch = -1 
                holdepoch = 0
                currentminvalerror = np.inf
            
                while epoch <num_epochs:
                    epoch+=1
                    # In each epoch, we do a full pass over the training data:
                    train_err = 0
                    train_batches = 0
                    start_time = time.time()
                    for batch in iterate_minibatches(X_train, y_train, batchsize, shuffle = True):
                        inputs, targets = batch
                        train_err += train_fn(inputs, targets)
                        train_batches += 1
            
                    # And a full pass over the validation data:
                    val_err = 0
                    val_acc = 0
                    val_batches = 0
                    meandif = 0
                    stdev = 0
                    iqrr = 0
                    meds = 0
                    thinrange = 0
                    thinrangestd = 0
                    thinrangeavg = 0
                    thinrangeiqr = 0
                    res = np.array([[np.nan], [np.nan]])
                    targetsvalhold = []
                    predsvalhold = []
        
                    for batch in iterate_minibatches(X_val, y_val, batchsize, shuffle = False):
                        inputs, targets = batch
                        err, acc = val_fn(inputs, targets)
                        preds = pred_fn(inputs)
            
                        res = np.append(res, np.squeeze(np.array([[preds], [targets[..., None]]])), axis = 1)
            
                        targets = targets[..., None]
             
                        selection = np.logical_and(selectionrange[0]  < targets, targets < selectionrange[1] ) == True
                       # inrange = np.append(inrange, np.squeeze(np.array([[preds[selection]], [targets[selection]]])), axis = 1)
                        if np.any(selection):
                            thinrangeavg += np.median(preds[selection])
                            thinrange += np.mean((preds - targets)[selection])
                            thinrangestd += np.std(((preds - targets)[selection]))
                            thinrangeiqr += np.subtract(*np.percentile((preds - targets)[selection], [75, 25]))
                        meandif += np.mean(np.abs(preds - targets))
                        stdev += np.std(preds - targets)
                        meds += np.median(preds - targets)
                        iqrr += np.subtract(*np.percentile(preds - targets, [75, 25]))
                        val_err += err
                        val_acc += acc
                        val_batches += 1
                        targetsval = targets
                        targetsvalhold.append(targetsval)
                        predsvalhold.append(preds)
                   # Check if validation error is lower than that of previous iteration
                    if epoch>0:
                        if val_err / val_batches < currentminvalerror:
                            networkerrors[networkno] = val_err / val_batches
                            holdweights = lasagne.layers.get_all_param_values(network)
                            holdepoch = epoch
                            currentminvalerror = val_err / val_batches
                            predsvalhold=np.concatenate(predsvalhold)
                            predsvalstore=predsvalhold
                            targetsvalstore=np.concatenate(targetsvalhold)
                            holdvalpredictions = np.zeros([y_val.size,1])
                            holdvalpredictions[0:predsvalhold.size]=predsvalhold
                            valsetappended = np.concatenate((valset,holdvalpredictions),axis=1)
            
                    # Then we print the results for this epoch:
                    print("Epoch {} of {} took {:.3f}s".format(
                        epoch + 1, num_epochs, time.time() - start_time))
                    print("  training loss:\t\t{:.6f}".format(train_err / train_batches))
                    print("  validation loss:\t\t{:.6f}".format(val_err / val_batches))
                    print("  mean difference thickness:\t{:.6f} mm".format(
                        meandif / val_batches))
                    print("  median difference thickness:\t{:.6f} mm".format(
                        meds / val_batches))
                    print("  stdev difference thickness:\t{:.6f} mm".format(
                        stdev / val_batches))
                    print("  iqr difference thickness:\t\t{:.6f} mm".format(
                        iqrr / val_batches))
                    print("  mean difference thickness rnge:\t{:.6f} mm".format(
                        thinrange / val_batches))
                    print("  median thickness rnge:\t\t{:.6f} mm".format(
                        thinrangeavg / val_batches))
                    print("  stdev thickness rnge:\t{:.6f} mm".format(
                        thinrangestd / val_batches))
                    print("  iqr thickness rnge:\t{:.6f} mm".format(
                        thinrangeiqr / val_batches))
                    valerror.append(val_err / val_batches)
                    trainerrorhold.append(train_err / train_batches)
                    epochhold.append(epoch)
                    meandifhold.append( meandif / val_batches)
                    medshold.append( meds / val_batches)
                    stdevhold.append(stdev / val_batches)
                    iqrrhold.append( iqrr / val_batches)
                    meandifrnghold.append( thinrange / val_batches)
                    medsrnghold.append(thinrangeavg / val_batches)
                    stdrnghold.append(thinrangestd / val_batches)
                    iqrrnghold.append( thinrangeiqr / val_batches)
                    # exit loop if within ten epochs the validation error didnt decrease
                    if epoch>holdepoch+epochdif:
                        epoch = num_epochs       
                    else:
                        print('iterations till stop: ' +str(epochdif - (epoch - holdepoch)))
            
            test_err = 0
            test_batches = 0
            meandif = 0
            res = np.array([[np.nan], [np.nan]])
            global targetstesthold, predstesthold
            targetstesthold = []
            predstesthold = []

            for batch in iterate_minibatches(X_test, y_test, batchsize, shuffle = False):
                inputs, targets = batch
                err, acc = val_fn(inputs, targets)
                test_err += err
                test_batches += 1
                predstest = pred_fn(inputs)
                res = np.append(res,np.squeeze(np.array([[predstest], [targets[..., None]]])), axis = 1)
                meandif += np.mean(np.abs(predstest - targets[..., None]))
                targetstest = targets
                targetstesthold.append(targetstest)
                predstesthold.append(predstest)
            print("Results network "+str(networkno)+':')
            print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
            print("  mean difference thickness:\t\t{:.6f} mm".format(
                    meandif / test_batches))
            predstesthold = np.concatenate(predstesthold)
        
            holdtestpredictions = np.zeros([y_test.size,1])
            holdtestpredictions[0:predstesthold.size]=predstesthold
            testsetappended = np.concatenate((testset,holdtestpredictions),axis=1)
            np.save(os.path.join(resultdir, 'results_vsize_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+ '_' +str(curtime) +'_testsetresults_LR_lastnetwork.npy'), testsetappended)
            # After training, we compute and print the test error:
            epochhold[holdepoch] *= -1
            lasagne.layers.set_all_param_values(network, holdweights)
            test_err = 0
            test_acc = 0
            test_batches = 0
            meandif = 0
            res = np.array([[np.nan], [np.nan]])

            targetstesthold = []
            predstesthold = []
            for batch in iterate_minibatches(X_test, y_test, batchsize, shuffle = False):
                inputs, targets = batch
                err, acc = val_fn(inputs, targets)
                test_err += err
                test_acc += acc
                test_batches += 1
                predstest = pred_fn(inputs)
                res = np.append(res,np.squeeze(np.array([[predstest], [targets[..., None]]])), axis = 1)
                meandif += np.mean(np.abs(predstest - targets[..., None]))
                targetstest = targets
                targetstesthold.append(targetstest)
                predstesthold.append(predstest)
            print("Results network "+str(networkno)+':')
            print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
            print("  mean difference thickness:\t\t{:.6f} mm".format(
                    meandif / test_batches))
            if not loadresults and not os.path.isfile(os.path.join(resultdir,'network_weights_vsize_'+ str(targetvoxelsize)+'_acq_'+loadresults.replace("_0_","_"+str(networkno)+"_")+'LR.npy')):
                targetstesthold = np.concatenate(targetstesthold)
                predstesthold = np.concatenate(predstesthold)
                # scatter results. First the validation set, and second the test set.
                plt.scatter(targetsvalstore, predsvalstore,label="val")
                plt.scatter(targetstesthold,predstesthold,label="test")
            
                holdtestpredictions = np.zeros([y_test.size,1])
                holdtestpredictions[0:predstesthold.size]=predstesthold
                testsetappended = np.concatenate((testset,holdtestpredictions),axis=1)
            
                plt.figure()
                plt.plot(np.array(valerror),label="val")
                plt.plot(np.array(trainerrorhold),label="train")
                axes = plt.gca()
                axes.set_ylim([0,valerror[-1]*2])
                inputs = ''
                valerror.append(test_err/test_batches)
                trainerrorhold.append(test_err/test_batches)
                epochhold.append(-1)
                meandifhold.append(meandif/test_batches)
                medshold.append(0)
                stdevhold.append(0)
                iqrrhold.append(0)
                meandifrnghold.append(0)
                medsrnghold.append(0)
                stdrnghold.append(0)
                iqrrnghold.append(0)
                
                plt.figure()
        
                # Save results in order: error statistics for each epoch(.txt), test results(.npy), network shape(.npy), and network weights (.npy and .p)
                np.savetxt(os.path.join(resultdir, 'results_vsize_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+'_' + str(curtime) + '_LR.txt'), np.array(np.concatenate((np.array(epochhold)[..., None].astype(int),
                                                                       np.array(trainerrorhold)[..., None], np.array(valerror)[..., None], np.array(meandifhold)[..., None],
                                                                       np.array( medshold)[..., None], np.array(stdevhold)[..., None], np.array(iqrrhold)[..., None], np.array(meandifrnghold)[..., None],
                                                                       np.array(medsrnghold)[..., None], np.array(stdrnghold)[..., None], np.array(iqrrnghold)[..., None]), axis = 1)), header = 'epoch\t\t\t\ttrain loss\t\t\tval loss\t\t\tmean dif thness\t\tmed dif thness\t\tstdev diff thness\tiqr diff thness\t\tmean dif thnssrange\tmed thnessrange\t\tstdev thnssrange\tiqr thnssrange', footer = 'Validation error for all epochs, last value is test error. ' + str(optionsdict)+'\n\n'+str(comments),fmt = '%1.5f', delimiter = '\t\t\t\t')
            
                np.save(os.path.join(resultdir, 'results_vsize_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+'_' + str(curtime) + '_LR.npy'), np.concatenate((predstesthold, targetstesthold[..., None]), axis = 1))
                np.save(os.path.join(resultdir, 'results_val_vsize_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+'_'+ str(curtime) + '_LR.npy'), np.concatenate((predsvalstore, targetsvalstore), axis = 1))
                np.save(os.path.join(resultdir, 'network_vsize_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+'_' + str(curtime) + '_LR.npy'), np.array(networkstring))
                np.save(os.path.join(resultdir, 'network_weights_vsize_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+'_' + str(curtime) + '_LR.npy'), holdweights)
                
                np.save(os.path.join(resultdir, 'results_vsize_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+ '_' +str(curtime) +'_testsetresults_LR.npy'), testsetappended)
                np.save(os.path.join(resultdir, 'results_vsize_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+ '_' +str(curtime) + '_valsetresults_LR.npy'), valsetappended)
                pickle.dump(network, open(os.path.join(resultdir,'network_las_form_vsize_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+'_' + str(curtime) + '_LR.p'), 'wb'), protocol = 2)
                layers=np.array([])
                for layer in networkstring:
                    if not (layer[0].__name__=='InputLayer' or layer[0].__name__=='DropoutLayer' or layer[0].__name__=='OutputLayer' or layer[0].__name__=='MaxPool3DLayer' or layer[0].__name__=='ConcatLayer'):
                        try:
                            layers = np.append(layers,'W:'+layer[0].__name__+str(layer[1]['input_shape']))
                            layers = np.append(layers,'b:'+layer[0].__name__+str(layer[1]['input_shape']))
                        except:    
                            layers = np.append(layers,'W:'+layer[0].__name__+str(layer[1]['input_shapes']))
                            layers = np.append(layers,'b:'+layer[0].__name__+str(layer[1]['input_shapes']))
         
                    
                scipy.io.savemat(os.path.join(resultdir, 'network_weights_vsize_'+ str(targetvoxelsize)+'_acq_'+str(networkno)+'_' + str(curtime) + '.mat'),dict(weights=holdweights, layers=layers))   
            networklist.append(network)
            if networkno in mailupdatelist:
                
                outlook = win32.Dispatch('outlook.application')
                mail = outlook.CreateItem(0)
                mail.To = mailadress
                mail.Subject = 'Spyder notification'
                mail.Body = 'Completed network '+ str(networkno)
                mail.Send()
        
        axes = plt.gca()
        axes.set_xlim([0, 1.4])
        axes.set_ylim([0, 1.4])
        targets=''
        
        sortednetworksonerror = np.argsort(networkerrors[:],axis=0)
        chosennetworkindices = sortednetworksonerror[0:nrofbestnetworks]
        alltestpreds = np.array([0])
        itt=0
        if trainvalperms>1:
            for networkindex in chosennetworkindices:
        
                lasagne.layers.set_all_param_values(network,lasagne.layers.get_all_param_values(networklist[networkindex[0]]))     
                #holdweights = np.load(os.path.join(resultdir,'network_weights_vsize_'+loadresults.replace("_0_","_"+str(networkno)+"_")+'.npy'))
                #lasagne.layers.set_all_param_values(network, holdweights)
                test_err = 0
                test_acc = 0
                test_batches = 0
                meandif = 0
                res = np.array([[np.nan], [np.nan]])
                targetstesthold, predstesthold =[],[]
    
                for batch in iterate_minibatches(X_test, y_test, batchsize, shuffle = False):
                    inputs, targets = batch
                    err, acc = val_fn(inputs, targets)
                    test_err += err
                    test_acc += acc
                    test_batches += 1
                    predstest = pred_fn(inputs)
                    res = np.append(res,np.squeeze(np.array([[predstest], [targets[..., None]]])), axis = 1)
                    meandif += np.mean(np.abs(predstest - targets))
                    targetstest = targets
                    targetstesthold.append(targetstest)
                    predstesthold.append(predstest)
                print("Results best networks (network: "+str(networkindex[0])+')')
                print("  test loss:\t\t\t{:.6f}".format(test_err / test_batches))
                print("  mean difference thickness:\t\t{:.6f} mm".format(
                        meandif / test_batches))
                if itt==0:
                    alltestpreds = np.zeros((np.concatenate(targetstesthold).size,chosennetworkindices.size))
                targetstesthold =np.concatenate(targetstesthold)
                alltestpreds[:,itt]=np.squeeze(np.concatenate(predstesthold))
                #alltestpreds = np.append(alltestpreds,predstesthold)
                itt+=1
    
            alltestpreds = np.sum(alltestpreds,axis=1)/nrofbestnetworks    
            test_err_final = 0
            meandiffinal = 0
            for batch in iterate_minibatches(alltestpreds,targetstesthold, batchsize, shuffle = False):
                preds_batch,target_batch = batch
                test_loss = np.power((preds_batch-target_batch),2)
                test_err_final += np.mean(test_loss)
                meandiffinal += np.mean(np.abs(preds_batch-target_batch))
            print("Final results:")
            print("  test loss:\t\t\t{:.6f}".format(test_err_final / test_batches))
            print("  mean difference thickness:\t\t{:.6f} mm".format(
                            meandiffinal / test_batches))
            plt.scatter(targetstesthold,alltestpreds,label="test")
            np.save(os.path.join(resultdir, 'results_vsize'+ str(targetvoxelsize)+'_acq_final_' + str(curtime) + '_LR.npy'), np.concatenate((alltestpreds[..., None], targetstesthold[..., None]), axis = 1))
        outlook = win32.Dispatch('outlook.application')
        mail = outlook.CreateItem(0)
        mail.To = mailadress
        mail.Subject = 'Spyder notification'
        mail.Body = 'Finished script'
        mail.Send()
        
def align_targets(predictions, targets):
    """Helper function turning a target 1D vector into a column if needed.
    This way, combining a network of a single output unit with a target vector
    works as expected by most users, not broadcasting outputs against targets.
    Parameters
    ----------
    predictions : Theano tensor
        Expression for the predictions of a neural network.
    targets : Theano tensor
        Expression or variable for corresponding targets.
    Returns
    -------
    predictions : Theano tensor
        The predictions unchanged.
    targets : Theano tensor
        If `predictions` is a column vector and `targets` is a 1D vector,
        returns `targets` turned into a column vector. Otherwise, returns
        `targets` unchanged.
    """
    if (getattr(predictions, 'broadcastable', None) == (False, True) and
            getattr(targets, 'ndim', None) == 1):
        targets = as_theano_expression(targets).dimshuffle(0, 'x')
    return predictions, targets

def weighted_squared_error(a, b):
    """Computes the element-wise squared difference between two tensors.
    .. math:: L = (p - t)^2
    Parameters
    ----------
    a, b : Theano tensor
        The tensors to compute the squared difference between.
    Returns
    -------
    Theano tensor
        An expression for the element-wise squared difference.
    Notes
    -----
    This is the loss function of choice for many regression problems
    or auto-encoders with linear output units.
    """
    global alpha, beta
    a, b = align_targets(a, b)
    newb = T.where( b > alpha, beta, 1)
    return theano.tensor.tensordot(theano.tensor.square(a - b),newb,axes=0)

def weighted_absolute_error(a, b):
    """Computes the element-wise squared difference between two tensors.
    .. math:: L = (p - t)^2
    Parameters
    ----------
    a, b : Theano tensor
        The tensors to compute the squared difference between.
    Returns
    -------
    Theano tensor
        An expression for the element-wise squared difference.
    Notes
    -----
    This is the loss function of choice for many regression problems
    or auto-encoders with linear output units.
    """
    global alpha, beta
    a, b = align_targets(a, b)
    newb = T.where( b > alpha, beta, 1)
    return theano.tensor.tensordot(theano.tensor.abs_(a - b),newb,axes=0)

def mean_absolute_error(a, b):
    """Computes the element-wise squared difference between two tensors.
    .. math:: L = (p - t)^2
    Parameters
    ----------
    a, b : Theano tensor
        The tensors to compute the squared difference between.
    Returns
    -------
    Theano tensor
        An expression for the element-wise squared difference.
    Notes
    -----
    This is the loss function of choice for many regression problems
    or auto-encoders with linear output units.
    """
    a, b = align_targets(a, b)
    return theano.tensor.abs_(a - b)
    #return theano.tensor.tensordot(theano.tensor.square(a - b),theano.tensor.exp(theano.tensor.power(b,4)),axes=0)
    #return theano.tensor.tensordot(theano.tensor.square(a - b),400*theano.tensor.power(b,2)-400*b+100.5,axes=0)
    #return theano.tensor.tensordot(theano.tensor.square(a - b),theano.tensor.power(b,6)+1,axes=0)
def mean_squared_error(a, b):
    """Computes the element-wise absolute error.
    .. math:: L = (p - t)^2
    Parameters
    ----------
    a, b : Theano tensor
        The tensors to compute the squared difference between.
    Returns
    -------
    Theano tensor
        An expression for the element-wise squared difference.
    Notes
    -----
    This is the loss function of choice for many regression problems
    or auto-encoders with linear output units.
    """
    a, b = align_targets(a, b)
    return theano.tensor.square(a - b)
    #return theano.tensor.tensordot(theano.tensor.square(a - b),theano.tensor.exp(theano.tensor.power(b,4)),axes=0)
    #return theano.tensor.tensordot(theano.tensor.square(a - b),400*theano.tensor.power(b,2)-400*b+100.5,axes=0)
    #return theano.tensor.tensordot(theano.tensor.square(a - b),theano.tensor.power(b,6)+1,axes=0)
           
if __name__ == '__main__':
    if ('--help' in sys.argv) or ('-h' in sys.argv):
        print("Trains a neural network on vessel wall MRI images using Lasagne.")
    else:
        kwargs = {}
        if len(sys.argv) > 1:
            kwargs['nrofsample'] = sys.argv[1]
        if len(sys.argv) > 2:
            kwargs['targetvoxelsize'] = sys.argv[2]
        if len(sys.argv) > 3:
            kwargs['sampledata'] = sys.argv[3]
        if len(sys.argv) > 4:
            kwargs['patchsz'] = sys.argv[4]
        if len(sys.argv) > 5:
            kwargs['excludelargerthan'] = sys.argv[5]
        if len(sys.argv) > 6:
            kwargs['num_epochs'] = sys.argv[6]
        if len(sys.argv) > 7:
            kwargs['eselectionrange'] = sys.argv[7]
        if len(sys.argv) > 8:
            kwargs['learningrate'] = sys.argv[8]
        if len(sys.argv) > 9:
            kwargs['blurstrength'] = sys.argv[9]
        if len(sys.argv) > 10:
            kwargs['excludesmallerthan'] = sys.argv[10]
        if len(sys.argv) > 11:
            kwargs['scanorder'] = sys.argv[11]
        if len(sys.argv) > 14:
            kwargs['batchsize'] = sys.argv[14]

        if not optionsdict:
            main(**kwargs)
        else:
            main(**optionsdict)
