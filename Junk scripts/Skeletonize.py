#!/usr/bin/env python

"""
Used to perform skeletonization of the vessel wall images. Not used in the final paper."""
from mevis import *


def savestuff():
  ctx.field("itkImageFileWriter.save").touch()
  ctx.field("itkImageFileWriter3.save").touch()
  ctx.field("Counter.autoStep").setBoolValue(True)
  
def deletetext():
  if ctx.field("Counter.currentValue").intValue()==2:
    ctx.field("Counter.autoStep").setBoolValue(False)
    ctx.field("itkImageFileWriter3.info").setStringValue('')
    ctx.field("Counter.reset").touch()
    