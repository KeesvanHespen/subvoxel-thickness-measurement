clear
manualmaskloaded=0;
%file names
maindir = '';
resultloc = 'results_ROI_0037-S006';
maskloc = 'MCA_mask_ROI_0037-S006';
selectcorrectrois = 0;
%% check if _selection already exists
load(fullfile(maindir,maskloc));

try
    load(fullfile(maindir,strcat(resultloc,'_selection')));
    imagebrowse(permute(cat(4,(results.Image - min(results.Image(:)))/(max(results.Image(:)) - min(results.Image(:))),results.thicknessmapselection.*mask),[4 1 2 3]),[0 2]);
    exist=1;
catch
    exist=0;
end
%% draw rois
%code execution
if ~exist
    load(fullfile(maindir,resultloc));
    thmap = results.thicknessmap;
else
    thmap = results.thicknessmapselection;
end

thmap(thmap>1.5)=0;
ROIs=drawROI(permute(cat(4,(results.Image - min(results.Image(:)))/(max(results.Image(:)) - min(results.Image(:))),thmap.*mask),[4 1 2 3]),inf);
if ~selectcorrectrois
    ROIs = ~ROIs;
end
thmap = thmap.*ROIs;

results.thicknessmapselection = thmap;
%%
% save
save(fullfile(maindir,strcat(resultloc,'_selection')),'results','-v7.3')