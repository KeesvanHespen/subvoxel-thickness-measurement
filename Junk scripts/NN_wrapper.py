# -*- coding: utf-8 -*-
"""
Created on Wed Sep 13 15:08:39 2017
Wrapper function for run_NN

@author: khespen
"""


import os
import numpy as np


# Data
ROIresultsname = ['C:/Users/khespen/Documents/DATA-Kees/20160307_S007_S005/ROI-S005/Other_side/results_ROI_0042-S005_FP020_PF015',
             'C:/Users/khespen/Documents/DATA-Kees/20160307_S007_S005/ROI-S007/results_ROI_0042R-S007_FP020_PF015',
             'C:/Users/khespen/Documents/DATA-Kees/20160619_S001_S004/ROI-S001/results_ROI_0030-S001_2_FP020_PF015',
             'C:/Users/khespen/Documents/DATA-Kees/20160619_S001_S004/ROI-S004/results_ROI_0043-S004_FP020_PF015',
             'C:/Users/khespen/Documents/DATA-Kees/20160222_S011_S006/ROI-S006/results_ROI_0037-S006_FP020_PF015',
             'C:/Users/khespen/Documents/DATA-Kees/20160222_S011_S006/ROI-S011/results_ROI_0037-S011_v5_FP020_PF015',
             'C:/Users/khespen/Documents/DATA-Kees/20160324_S012_S010/ROI-S010/Other_side/results_ROI_0042R-S010_FP020_PF015',
            # 'C:/Users/khespen/Documents/DATA-Kees/20160324_S012_S010/ROI-S012/results_ROI_0042-S012_FP020_PF015',
             'C:/Users/khespen/Documents/DATA-Kees/20160618_S002_S003/ROI-S002/results_ROI_0049-S002_FP020_PF015',
             'C:/Users/khespen/Documents/DATA-Kees/20160618_S002_S003/ROI-S003/Otherside2/results_ROI_0049R-S003_FP020_PF015',
             'C:/Users/khespen/Documents/DATA-Kees/20160627_S008_S009/ROI-S008/results_ROI_0031R-S008_FP020_PF015',
             'C:/Users/khespen/Documents/DATA-Kees/20160627_S008_S009/ROI-S009/results_ROI_0031-S009_FP020_PF015'
              ]

maskname = ['C:/Users/khespen/Documents/DATA-Kees/20160307_S007_S005/ROI-S005/Other_side/MCA_R_mask_ROI_0042-S005_2',
           'C:/Users/khespen/Documents/DATA-Kees/20160307_S007_S005/ROI-S007/MCA_mask_ROI_0042R-S007_2',
           'C:/Users/khespen/Documents/DATA-Kees/20160619_S001_S004/ROI-S001/MCA_R_mask_ROI_0030-S001_2',
           'C:/Users/khespen/Documents/DATA-Kees/20160619_S001_S004/ROI-S004/MCA_R_mask_ROI_0043-S004_2',
           'C:/Users/khespen/Documents/DATA-Kees/20160222_S011_S006/ROI-S006/MCA_mask_ROI_0037-S006',
           'C:/Users/khespen/Documents/DATA-Kees/20160222_S011_S006/ROI-S011/MCA_mask_ROI_0037-S011',
           'C:/Users/khespen/Documents/DATA-Kees/20160324_S012_S010/ROI-S010/Other_side/MCA_R_mask_ROI_0042R-S010_2',
          # 'C:/Users/khespen/Documents/DATA-Kees/20160324_S012_S010/ROI-S012/MCA_mask_ROI_0042-S012',
           'C:/Users/khespen/Documents/DATA-Kees/20160618_S002_S003/ROI-S002/MCA_R_mask_ROI_0049-S002',
           'C:/Users/khespen/Documents/DATA-Kees/20160618_S002_S003/ROI-S003/Otherside2/MCA_mask_ROI_0049R-S003',
           'C:/Users/khespen/Documents/DATA-Kees/20160627_S008_S009/ROI-S008/MCA_R_mask_ROI_0031R-S008',
           'C:/Users/khespen/Documents/DATA-Kees/20160627_S008_S009/ROI-S009/MCA_mask_ROI_0031-S009']

ROIdcmname = ["C:/Users/khespen/Documents/DATA-Kees/20160307_S007_S005/ROI-S005/Other_side/ROI_0042-S005.dcm",
"C:/Users/khespen/Documents/DATA-Kees/20160307_S007_S005/ROI-S007/ROI_0042R-S007.dcm",
'C:/Users/khespen/Documents/DATA-Kees/20160619_S001_S004/ROI-S001/ROI_0030-S001_2.dcm',
'C:/Users/khespen/Documents/DATA-Kees/20160619_S001_S004/ROI-S004/ROI_0043-S004.dcm',
'C:/Users/khespen/Documents/DATA-Kees/20160222_S011_S006/ROI-S006/ROI_0037-S006.dcm',
'C:/Users/khespen/Documents/DATA-Kees/20160222_S011_S006/ROI-S011/ROI_0037-S011.dcm',
'C:/Users/khespen/Documents/DATA-Kees/20160324_S012_S010/ROI-S010/Other_side/ROI_0042R-S010.dcm',
#'C:/Users/khespen/Documents/DATA-Kees/20160324_S012_S010/ROI-S012/ROI_0042-S012.dcm',
'C:/Users/khespen/Documents/DATA-Kees/20160618_S002_S003/ROI-S002/ROI_0049-S002.dcm',
 'C:/Users/khespen/Documents/DATA-Kees/20160618_S002_S003/ROI-S003/Otherside2/ROI_0049R-S003.dcm',
 'C:/Users/khespen/Documents/DATA-Kees/20160627_S008_S009/ROI-S008/ROI_0031R-S008.dcm',
 'C:/Users/khespen/Documents/DATA-Kees/20160627_S008_S009/ROI-S009/ROI_0031-S009.dcm']

FULLdcmname = ["C:/Users/khespen/Documents/DATA_KVH/20160307_S007-S005/DICOM/DICOM/IM_0042",
  "C:/Users/khespen/Documents/DATA_KVH/20160307_S007-S005/DICOM/DICOM/IM_0042",
  'C:/Users/khespen/Documents/DATA_KVH/20160619_S001-S004/DICOM/DICOM/IM_0030',
  'C:/Users/khespen/Documents/DATA_KVH/20160619_S001-S004/DICOM/DICOM/IM_0030',
  'C:/Users/khespen/Documents/DATA_KVH/20160222_S011-S006/DICOM/DICOM/IM_0037',
  'C:/Users/khespen/Documents/DATA_KVH/20160222_S011-S006/DICOM/DICOM/IM_0037',
  'C:/Users/khespen/Documents/DATA_KVH/20160324_S012-S010/DICOM/DICOM/IM_0042',
 # 'C:/Users/khespen/Documents/DATA_KVH/20160324_S012-S010/DICOM/DICOM/IM_0042',
  'C:/Users/khespen/Documents/DATA_KVH/20160618_S002-S003/DICOM/DICOM/IM_0049',
  'C:/Users/khespen/Documents/DATA_KVH/20160618_S002-S003/DICOM/DICOM/IM_0049',
  'C:/Users/khespen/Documents/DATA-Kees/20160627_S008_S009/DICOM/DICOM/IM_0031',
  'C:/Users/khespen/Documents/DATA-Kees/20160627_S008_S009/DICOM/DICOM/IM_0031',
  ]


#options (can be numpy arrays)
nrofsample=10000  #nr of sample patches taken
targetvoxelsize=0.4005 #target voxelsize of downsampled image
sampledata=0 #if 1, samples are sampled regardless of existence of .p file
patchsz=17 #size of the patches in pixels
excludelargerthan=1.5#exclude voxels with thickness larger than excludelargerthan. To turn off, set it at np.inf
numepochs=80 #epoch count for NN
selectionrange= np.array([0.95,1.05]) #Select thickness range voxels for analysis
lr=0.0005


#run stuff
tuplist=[nrofsample,targetvoxelsize, sampledata,patchsz, excludelargerthan,numepochs,selectionrange,lr]


for nrofsample,targetvoxelsize, sampledata,patchsz, excludelargerthan,numepochs,selectionrange,lr in tuplist:
    os.system('C:/Users/khespen/Documents/B@R-NN/run_NN.py '+ str(nrofsampleit)+' '+str(targetvoxelsizeit)+' '+str(sampledatait)+' '+str(patchszit)+' '+str(excludeit)+' '+str(numepochsit)+' '+str(selectionit)+' '+str(lrit)

    



