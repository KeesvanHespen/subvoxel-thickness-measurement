# -*- coding: utf-8 -*-
"""
Created on Mon Jul  9 10:24:25 2018

@author: khespen
"""
from __future__ import print_function

import os

import nibabel as nib
import numpy as np
import theano
import theano.tensor as T

import lasagne
print("hier")
# ################## Download and prepare the MNIST dataset ##################
# This is just some way of getting the MNIST dataset from an online location
# and loading it into numpy arrays. It doesn't involve Lasagne at all.

def build_mlp(input_var = None):
    # This creates an MLP of two hidden layers of 800 units each, followed by
    # a softmax output layer of 10 units. It applies 20% dropout to the input
    # data and 50% dropout to the hidden layers.
    global patchsz
    # Input layer, specifying the expected input shape of the network
    # (unspecified batchsize, 1 channel, 28 rows and 28 columns) and
    # linking it to the given Theano variable `input_var`, if any:
    network = lasagne.layers.InputLayer(shape = (None, 1, patchsz, patchsz, patchsz),
                                     input_var = input_var)
    # Convolution and dropout
    network1 = lasagne.layers.Conv3DLayer(network, 16, 3, 1, nonlinearity = lasagne.nonlinearities.leaky_rectify)
    network = lasagne.layers.DropoutLayer(network1, p = 0.2)  
    
    # Convolution and dropout
    network2 = lasagne.layers.Conv3DLayer(network, 32, 3, 1, nonlinearity = lasagne.nonlinearities.leaky_rectify)
    network = lasagne.layers.DropoutLayer(network2, p = 0.2)  

    # Convolution and dropout
    network3 = lasagne.layers.Conv3DLayer(network, 64, 3, 1, nonlinearity = lasagne.nonlinearities.leaky_rectify)
    network = lasagne.layers.DropoutLayer(network3, p = 0.2)    
   
    # Convolution and dropout
    network4 = lasagne.layers.Conv3DLayer(network, 64, 3, 1, nonlinearity = lasagne.nonlinearities.leaky_rectify)
   
    network = lasagne.layers.DropoutLayer(network4, p = 0.2)   
    
    network5 = lasagne.layers.Conv3DLayer(network, 128, 3, 1, nonlinearity = lasagne.nonlinearities.leaky_rectify)
   
    network = lasagne.layers.DropoutLayer(network5, p = 0.2)   

    network = lasagne.layers.DenseLayer(
            network, num_units=200,
            nonlinearity = lasagne.nonlinearities.leaky_rectify,
            W=lasagne.init.GlorotUniform())
    network = lasagne.layers.DropoutLayer(network, p = 0.1)    
    
    # Convolution and dropout
    #network = lasagne.layers.Conv3DLayer(network, 32, 1, 1, nonlinearity = lasagne.nonlinearities.linear)
    #network = lasagne.layers.DropoutLayer(network, p=0.5) 
    
    
    network6 = lasagne.layers.DenseLayer(
                network, num_units=400,
                nonlinearity = lasagne.nonlinearities.leaky_rectify,
                W=lasagne.init.GlorotUniform())
    network6 = lasagne.layers.DropoutLayer(network6, p = 0.1)
    network6 = lasagne.layers.DenseLayer(
            network6, num_units = 1, nonlinearity = lasagne.nonlinearities.linear,
            W=lasagne.init.GlorotUniform())

    # Each layer is linked to its incoming layer(s), so we only need to pass
    # the output layer to give access to a network in Lasagne:
    return  network1, network2, network3, network4, network5, network6
def iterate_minibatches(inputs, targets, batchsze, shuffle = False):
    assert len(inputs) == len(targets)
    if shuffle:
        indices = np.arange(len(inputs))
        np.random.shuffle(indices)
    for start_idx in range(0, len(inputs) - batchsze + 1, batchsze):
        if shuffle:
            excerpt = indices[start_idx:start_idx + batchsze]
        else:
            excerpt = slice(start_idx, start_idx + batchsze)
        yield inputs[excerpt], targets[excerpt]

if __name__ == '__main__':
    global patchsz
    print("hier")
    patchsz = 11
    layer = 2-1
    holdw_loc = 1
    holdweights = np.load('C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/4. Results/network_weights_vsize_0.8001_0_2018-06-08_13;23;48.npy')
    inpt = nib.load('D:/1. Original Data FWHM/20160222_S011-S006/DICOM/DICOM/IM_0033_mag_lumencorrected_normalized.nii')
    inpt = inpt.get_fdata()
    inpt = inpt[152:185,152:185,18:51]
    inpt = inpt[np.newaxis,...]
    splitrow=np.split(inpt,3,axis=2)
    splitz=['','','']
    for x in range(0,3):
        splitz[x]=np.array(np.split(splitrow[x],3,axis=3))
    y = np.concatenate(splitz,axis=0)    
    splitcol =  np.split(y,3,axis=2)      
    inpt = np.concatenate(splitcol,axis=0)  
    
    savedir=  'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/4. Results'
    input_var = T.tensor5('inputs',dtype='float32')
    target_var = T.fvector('targets')
    # Create neural network model (depending on first command line parameter)
    print("Building model and compiling functions...")
    network1, network2, network3, network4, network5, network6 = build_mlp(input_var)
    network = [network1, network2, network3, network4, network5, network6 ]
    np.save(os.path.join(savedir,'featmaps_layer_'+'0'+'.npy'),inpt)
    lasagne.layers.set_all_param_values(network[-1], holdweights)
    
    test_prediction = lasagne.layers.get_output(network[layer], deterministic = True)
    
    pred_fn = theano.function([input_var], test_prediction)
    preds=[]
    inputs = inpt.astype('float32')
    target = inpt.astype('float32')
   # for batch in iterate_minibatches(inpt, inpt, 250, shuffle = False):
   #     inputs, targets = batch
    print('hier')
    idx=0
    for net in network:
        idx+=1
        test_prediction = lasagne.layers.get_output(net, deterministic = True)
        pred_fn = theano.function([input_var], test_prediction)
        preds = pred_fn(inputs)
           
        np.save(os.path.join(savedir,'featmaps123_layer_'+str(idx)+'.npy'),preds)
        
    
    
    
    
    
    
    
    