from mevis import *
import numpy as np
import os
import glob
import sys
import matplotlib.pyplot as plt 
import datetime
import pydicom
import subprocess as subp

def loadnpy():
  arry = np.load(ctx.field("FileInformation.path").stringValue())
  interface = ctx.module("PythonImage").call("getInterface")

  
  interface.setImage(arry)

def clearnpy():
  interface = ctx.module("PythonImage").call("getInterface")

  
  interface.unsetImage()

def pri():
 ctx.field("SubImage.t").setValue(ctx.field("slid").value)
  
def init():
  ctx.field("SubImage.t").setValue(0)