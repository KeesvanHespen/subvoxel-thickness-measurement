# -*- coding: utf-8 -*-
"""
Network builder for lasagne.

Builds network from saved network struct

Created on Thu Oct 12 11:11:13 2017

@author: khespen
"""
from __future__ import print_function


import lasagne


def construct_network(input_var,struct):
    for layer in struct:
        print(layer.item(0))
        print(layer.item(1))
        if layer[0].__name__ in 'lasagne.layers.InputLayer':
            network = lasagne.layers.InputLayer(shape=layer[1]['shape'],input_var=input_var)
        if layer[0].__name__ in 'lasagne.layers.conv.Conv3DLayer':
            network = lasagne.layers.conv.Conv3DLayer(network,layer[1]['num_filters'],layer[1]['filter_size'],stride=layer[1]['stride'],W=layer[1]['W'],b=layer[1]['b'],pad=layer[1]['pad'],nonlinearity = layer[1]['nonlinearity'])
        if layer[0].__name__ in 'lasagne.layers.MaxPool3DLayer':
            network = lasagne.layers.MaxPool3DLayer(network,layer[1]['pool_size'],stride=layer[1]['stride'],pad=layer[1]['pad'])
        if layer[0].__name__ in 'lasagne.layers.noise.DropoutLayer':
            network = lasagne.layers.noise.DropoutLayer(network,p=layer[1]['p'],rescale=layer[1]['rescale'])
        if layer[0].__name__ in 'lasagne.layers.Denselayer':
                network = lasagne.layers.noise.Denselayer(network,layer[1]['num_units'],nonlinearity = layer[1]['nonlinearity'],W=layer[1]['W'],b=layer[1]['b'])
    return network

