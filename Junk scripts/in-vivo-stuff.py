from mevis import *
import numpy as np
import os
import sys
import matplotlib.pyplot as plt 
import datetime
import pydicom
import subprocess as subp
import scipy.ndimage
from subprocess import Popen

def normalize():
  
  agarimage = ctx.field("Arithmetic1.output0").image()
  agarimage = agarimage.getTile( (0,0,0), (agarimage.UseImageExtent,agarimage.UseImageExtent,agarimage.UseImageExtent) )
  wallimage = ctx.field("IntervalThreshold.output0").image()
  wallimage = wallimage.getTile( (0,0,0), (wallimage.UseImageExtent,wallimage.UseImageExtent,wallimage.UseImageExtent) )
  inputimage = ctx.field("Mask1.output0").image()
  inputimage = inputimage.getTile((0,0,0), (inputimage.UseImageExtent,inputimage.UseImageExtent,inputimage.UseImageExtent) )
  
  wallmasked = inputimage*wallimage
  agarmasked = inputimage*agarimage
  agarmasked2 = agarmasked[agarmasked!=0]
  wallmasked2 = wallmasked[wallmasked!=0]
  wallmean = np.percentile(wallmasked2,95)
  agarmean = np.median(agarmasked2)
  print('Agar mean is not hard coded!!')
  print(agarmean)
  print(np.std(wallmasked2))
  print(wallmean)
  std_agar = ctx.field("ImageStatistics.totalStdDev").value
  mask = inputimage==1
 # c = np.count_nonzero(mask)
  #nums = np.random.normal(agarmean,std_agar/5, c)
  #c = np.prod(mask.shape)
  nums = np.random.normal(agarmean,std_agar, mask.shape)
  #vals = np.where(nums <= 2, np.random.normal(agarmean,std_agar, c) , 0)
  outputimage = scipy.ndimage.gaussian_filter(nums,2.5)
  print(np.mean(outputimage))
  print(np.std(outputimage))
#  inputimage[mask] = vals
  #outputimage[mask]=inputimage[mask]
  ctx.field("Arithmetic12.constant").setValue(agarmean)
  ctx.field("Arithmetic11.constant").setValue(wallmean-agarmean)
  
  interface = ctx.module("PythonImage").call("getInterface")
  interface.setImage(outputimage, minMaxValues =(np.min(outputimage),np.max(outputimage)))

  if 0:
    ctx.field("itkImageFileWriter5.unresolvedFileName").setStringValue((ctx.field("itkImageFileReader1.unresolvedFileName").stringValue()).replace('biascorrected2','normalized').replace('biascorrected','normalized'))
    if not ctx.field("itkImageFileWriter5.unresolvedFileName").stringValue()==ctx.field("itkImageFileReader1.unresolvedFileName").stringValue():
      ctx.field("itkImageFileWriter5.save").touch()
    else:
      print('Save name is same as input file name. Saving aborted!')