folder = 'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\4. Results';
savefolder = 'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\3. Data\20160618_S002_S003\ROI-S003\';
cd(savefolder)
image = dicomread(fullfile(savefolder,'ROI_0049R-S003.dcm'));

HRLRresults=readNPY(fullfile(folder,'results_vsize_0.8001_1_2018-06-08_13;23;48_testsetresults.npy'));
results0=HRLRresults(HRLRresults(:,5)==8,:);

resultsmatrix = zeros(size(image));

for row=1:size(results0,1)
    resultsmatrix(results0(row,2),results0(row,3),1,results0(row,4))=results0(row,6);
end
resultsmatrix = squeeze(resultsmatrix);
save('estimated.mat','resultsmatrix')
resultsmatrix = zeros(size(image));

for row=1:size(results0,1)
    resultsmatrix(results0(row,2),results0(row,3),1,results0(row,4))=results0(row,1);
end
resultsmatrix = squeeze(resultsmatrix);
save('target.mat','resultsmatrix')
'done'