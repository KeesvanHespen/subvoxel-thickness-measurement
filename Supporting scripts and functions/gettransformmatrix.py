# -*- coding: utf-8 -*-
"""
Created on Thu May 24 09:06:59 2018

@author: khespen
"""
import numpy as np

def gettransformmatrix(info):
    """
    ########################################
    #                                      #
    # compute world matrix from dicom info #
    #      works for 2D and 3D data.       #
    #                                      #
    ########################################
    INPUT: info:  dicom info dict
    OUTPUT: Wtransmat: World transformation matrix
            [dx, dy, dz]: voxel dimensions
    """
    instance = 0
    try:
        T1 = np.array(info.ImagePositionPatient)
    except:
        
        foundfirstslice=0
        while not foundfirstslice:
            if info.PerFrameFunctionalGroupsSequence[instance].FrameContentSequence[0].InStackPositionNumber==1:
                foundfirstslice=1
            else:
                instance +=1

            
        T1 = np.array(info.PerFrameFunctionalGroupsSequence[instance].PlanePositionSequence[0].ImagePositionPatient)      
    T1 = T1[...,np.newaxis]
    try:
        pixelspacing = info.PixelSpacing
    except:
        pixelspacing = info.PerFrameFunctionalGroupsSequence[instance].PixelMeasuresSequence[0].PixelSpacing
    dx = pixelspacing[0]
    dX = np.array([1, 1, 1]) * dx
    dy = pixelspacing[1]
    dY = np.array([1, 1, 1]) * dy
    dz = info.SpacingBetweenSlices
    dZ = np.array([1, 1, 1]) * dz
    try:
        Orientation = info.ImageOrientationPatient
    except:
        Orientation = info.PerFrameFunctionalGroupsSequence[instance].PlaneOrientationSequence[0].ImageOrientationPatient
        
    dircosX = np.array(Orientation[0:3])
    dircosY = np.array(Orientation[3:6])
    dircosZ = np.cross(dircosX,dircosY)


    #all dircos together
    dimensionmixing = np.transpose(np.array([dircosX, dircosY, dircosZ]))
    
    #all spacing together
    dimensionscaling = np.transpose(np.array([dX, dY, dZ]))
    
    #mixing and spacing of dimensions together
    R = dimensionmixing * dimensionscaling
    
    lastvec = np.array([0, 0, 0, 1])
    lastvec = lastvec[None, ...]
    Wtransmat = np.concatenate((R, T1), 1)
    Wtransmat = np.concatenate((Wtransmat, lastvec))
    

    return Wtransmat