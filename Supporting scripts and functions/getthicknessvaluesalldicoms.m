%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% File used to sort the ground truth  %
% thickness measurements per scan, in %
% order to create the neural network  %
% test set folds                      %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
% KM van Hespen, UMC Utrecht, 2020
clear all
ROIresultsname = {  
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S005/ROI_0042-S005_thicknessmap.nii',
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160307_S007_S005/ROI-S007/ROI_0042R-S007_thicknessmap.nii',
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S001/ROI_0030-S001_2_thicknessmap.nii',
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160619_S001_S004/ROI-S004/ROI_0043-S004_thicknessmap.nii',
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S006-3/ROI_0037-S006_thicknessmap.nii',
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160222_S011_S006/ROI-S011-3/ROI_0037-S011-3_thicknessmap.nii',
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160324_S012_S010/ROI-S010/ROI_0042R-S010_thicknessmap.nii',
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S002-3/ROI_0049-S002_thicknessmap.nii',
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160618_S002_S003/ROI-S003-3/ROI_0049-S003_thicknessmap.nii',
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S008-3/ROI_0031-S008_thicknessmap.nii',
    'C:/Users/khespen/Documents/1. Vessel wall thickness measurements/2. NN method/3. Data/20160627_S008_S009/ROI-S009-3/ROI_0031-S009_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S021/ROI_0091-S021_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20171711_S017_S018/S017/ROI_0061-S017-2_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20170412_S023_S024/S024/ROI_0121-S024_thicknessmap.nii', 
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/22022018_S033_S034/S034/ROI_0017-S034-33_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01032018_S035_S036/S035/ROI_0024-S035_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/20172710_S015_S016/S015/ROI_0074-S015_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01032018_S035_S036/S036/ROI_0024-S036_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08032018_S037_S038/S037/ROI_0024-S037_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08032018_S037_S038/S038/ROI_0024-S038_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01022018_S029_S030/S029/ROI_0046-S029_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/01022018_S029_S030/S030/ROI_0046-S030_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/07022018_S031_S032/S031/ROI_0059-S031_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/07022018_S031_S032/S032/ROI_0059-S032_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/11012018_S025_S026/S026/ROI_0062-S026_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/18012018_S027_S028/S027/ROI_0071-S027_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/18012018_S027_S028/S028/ROI_0071-S028_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/30112017_S021_S022/S022/ROI_0091-S022_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08092017_S013_S014/S013/ROI_0000-S013_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/08092017_S013_S014/S014/ROI_0000-S014_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/24052018_S041_S042/S042/ROI_0024-S042_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/24052018_S041_S042/S041/ROI_0024-S041_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/05072018_S047_S048/S048/ROI_0024-S048_thicknessmap.nii',
    'C:/Users/khespen/Documents/5. Hypertension/1. Working data/05072018_S047_S048/S047/ROI_0024-S047_thicknessmap.nii'
    }


for u=1:numel(ROIresultsname)
    results.thicknessmapselection = niftiread(ROIresultsname{u}).*double(niftiread(strrep(ROIresultsname{u},'thicknessmap.nii','selectionmask.nii')))
    
    try
        mat(1,u)=nnz(results.thicknessmapselection);
        mat(2,u)=min(nonzeros(results.thicknessmapselection));
        mat(3,u)=max(nonzeros(results.thicknessmapselection));
        mat(4,u)=nnz(results.thicknessmapselection(results.thicknessmapselection>0.6));
        mat(5,u)=nnz(results.thicknessmapselection(results.thicknessmapselection>1.3));
    catch
        mat(1,u)=nnz(results.thicknessmap_selection);
        mat(2,u)=min(nonzeros(results.thicknessmap_selection));
        mat(3,u)=max(nonzeros(results.thicknessmap_selection));
        mat(4,u)=nnz(results.thicknessmap_selection(results.thicknessmap_selection>0.6));
        mat(5,u)=nnz(results.thicknessmap_selection(results.thicknessmap_selection>1.3));
    end
    %figure;
    %histogram(nonzeros(results.thicknessmapselection),40,'Normalization','probability')
    %title(ROIresultsname{u})
    %xlim([0,2]);ylim([0,0.4])
end

[a,i]=sort(mat(4,:),'descend');
val= [2,  3,  4, 11, 16, 17, 18, 24, 28, 31, 33]+1;
idx=ismember(i,val);
i(idx)=[];
for v=1:floor(numel(i)/2)
    testset(v) = i(2*v-1);
end
testset  = sort(testset,'ascend')-1;
trainset = sort([setdiff(i,testset+1)-1,val-1]);
fprintf('\ntestset\n')
fprintf('%i,',testset)
fprintf('\ntrainset \n')
fprintf('%i,',trainset)
