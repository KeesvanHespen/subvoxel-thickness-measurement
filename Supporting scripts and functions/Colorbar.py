#!/usr/bin/env python

"""
Function that creates a colorbar at the right side of a View3D/2D module. 
Allows for changing the text font size, and the thickness of the colorbar """
from mevis import *
import numpy as np
import os
import scipy.ndimage 
import sklearn.linear_model
import glob
import shutil
import matplotlib.pyplot as plt 
import matplotlib as mpl

def init():
  ctx.field("XMarkerListContainer6.deleteAll").touch()

def setlabels():
  ctx.field("XMarkerListContainer6.deleteAll").touch()
  ticklabels = np.linspace(ctx.field("LUTInfo.maxIndex").value,ctx.field("LUTInfo.minIndex").value,3)
  ticklabels = np.around(ticklabels, decimals=2)
  ticklocations = np.linspace(0,ctx.field("LUTToMLImage.rescaleWidth").value,3)
  
  for tick in range(0,3):
    currentloc = ticklocations[tick]
    if tick==0:
      currentloc = ticklocations[tick]+2
    if tick==np.max(range(0,3)):
      currentloc = ticklocations[tick]-4
    ctx.field("XMarkerListContainer6.add").touch()
    ctx.field("WorldVoxelConvert4.voxelY").setValue(currentloc)
    ctx.field("XMarkerListContainer6.name").setStringValue(ticklabels[tick])
  ctx.field("XMarkerListContainer6.add").touch()
  