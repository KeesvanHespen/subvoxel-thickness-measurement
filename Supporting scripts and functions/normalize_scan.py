"""
Normalize input images by setting the median background intensity at 0, and the 95th percentile of foreground intensities at 1.

KM van Hespen, UMC Utrecht, 2020
"""
from mevis import *
import numpy as np
import os
import glob
import sys
import matplotlib.pyplot as plt 
import datetime
import pydicom
import subprocess as subp

def normalize():
  defthresh = ctx.field("IntervalThreshold1.threshMax").value
  
  for shiftt in np.array([100]):
    ctx.field("IntervalThreshold1.threshMax").setValue(defthresh*shiftt/100)
    agarimage = ctx.field("IntervalThreshold1.output0").image()
    agarimage = agarimage.getTile( (0,0,0), (agarimage.UseImageExtent,agarimage.UseImageExtent,agarimage.UseImageExtent) )
    wallimage = ctx.field("IntervalThreshold.output0").image()
    wallimage = wallimage.getTile( (0,0,0), (wallimage.UseImageExtent,wallimage.UseImageExtent,wallimage.UseImageExtent) )
    inputimage = ctx.field("itkImageFileReader.output0").image()
    inputimage = inputimage.getTile((0,0,0), (inputimage.UseImageExtent,inputimage.UseImageExtent,inputimage.UseImageExtent) )
    
    wallmasked = inputimage*wallimage
    agarmasked = inputimage*agarimage
    agarmasked2 = agarmasked[agarmasked!=0]
    wallmasked2 = wallmasked[wallmasked!=0]
    wallmean = np.percentile(wallmasked2,95)
    wallmedian = np.median(wallmasked2)
    print(wallmean)
    
    agarmean = np.median(agarmasked2)
    print(agarmean)
    ctx.field("Arithmetic1.constant").setValue(agarmean)
    ctx.field("Arithmetic11.constant").setValue(wallmean-agarmean)
    ctx.field("itkImageFileWriter.unresolvedFileName").setStringValue((ctx.field("itkImageFileReader.unresolvedFileName").stringValue().replace('.nii','_normalized_'+str(shiftt)+'precentotsu_test.nii')))
    ctx.field("itkImageFileWriter1.unresolvedFileName").setStringValue((ctx.field("itkImageFileReader1.unresolvedFileName").stringValue()).replace('_lumencorrected_bgcorrected','_lumcor_nonbgcor_nonnormtest'))

    if not ctx.field("itkImageFileWriter.unresolvedFileName").stringValue()==ctx.field("itkImageFileReader.unresolvedFileName").stringValue():
      ctx.field("itkImageFileWriter.save").touch()
  #else:
  #  print('Save name is same as input file name. Saving aborted!')

def shide():
  if ctx.field("GVROrthoOverlay.on").value==True:
    ctx.field("GVROrthoOverlay.on").setBoolValue(False)
    ctx.field("GVROrthoOverlay1.on").setBoolValue(False)  
  else:
    ctx.field("GVROrthoOverlay.on").setBoolValue(True)
    ctx.field("GVROrthoOverlay1.on").setBoolValue(True)