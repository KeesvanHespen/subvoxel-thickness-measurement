# -*- coding: utf-8 -*-
"""
Created on Mon Feb 19 13:17:03 2018

Normalize background, by callings ANTS.
@author: khespen
"""

from __future__ import print_function


import os

import subprocess as subp

import dicom
import nibabel as nib
import numpy as np

import scipy
import scipy.ndimage
import SimpleITK

from mode import mode

### Settings ###
scanname = 'C:/Users/khespen/Documents/5. Hypertension/1. Working data/18012018_S027_S028/S028/ROI_0073-S028'
ANTSdir = '"C:\\Program Files\\ANTs\\Release\\N4BiasFieldCorrection"'

##### Start #####
niiinputImage = nib.load(scanname+'.nii')
inputImage= niiinputImage.get_data();
aff = niiinputImage.affine;
head = niiinputImage.header;
#apply otsu
filterotsu = SimpleITK.OtsuMultipleThresholdsImageFilter()
filterotsu.SetNumberOfThresholds(2)
filterotsu.Execute(SimpleITK.GetImageFromArray(inputImage.astype('float32')))
imagethresholds = filterotsu.GetThresholds()
maskImage = inputImage>imagethresholds[0]
    
maskImage = maskImage.astype(int)
niimaskImage = nib.Nifti1Image(maskImage,aff,head)
nib.save(niimaskImage,scanname + '_mask.nii')
print('Computing background correction. Do not close command window! Could take a while')
if not os.path.isfile(scanname + '_biascorrected.nii'):
    systemcall = subp.call(ANTSdir + ' -i "'+ scanname + '.nii'+'" -o "'+ scanname+'_biascorrected.nii"' + ' -x "'+ scanname + '_mask.nii" -s 5 -t [0.05] -c [100x100x100x100,0.000]')
    systemcall = subp.call(ANTSdir + ' -i "'+ scanname + '_biascorrected.nii'+'" -o "'+ scanname+'_biascorrected2.nii"' + ' -x "'+ scanname + '_mask.nii" -s 5 -t [0.05] -c [100x100x100x100,0.000]')
    if systemcall == 0:
        print('Failed to run N4. Further computations will probably crash!!')
    
    niioutputImage = nib.load(scanname+'_biascorrected2.nii')
    
    outputImage = niioutputImage.get_data()
    outputImage = outputImage.astype('float64')
    compiledoutputImage = nib.Nifti1Image(outputImage,aff,head)
    
    niioutputImage=[]
    os.remove(scanname+'_biascorrected.nii')
    os.remove(scanname+'_biascorrected2.nii')
    nib.save(compiledoutputImage,scanname + '_biascorrected.nii') 
else:
    compiledoutputImage = nib.load(scanname + '_biascorrected.nii') 
    outputImage = compiledoutputImage.get_data()
    outputImage = outputImage.astype('float64')
#normalize between all sets
reader=SimpleITK.ImageFileReader()
reader.SetFileName(scanname)

#crop dicom to modulus
#apply otsu
BGremovedoutputImage = outputImage[outputImage>imagethresholds[0]]
mean_agarose = mode(BGremovedoutputImage)[0]
print('Agarose intensity: '+ str(mean_agarose))
#get wall values
agarremovedoutputImage = outputImage[outputImage>mean_agarose]
itkoutputImage=SimpleITK.GetImageFromArray(agarremovedoutputImage[...,None].astype('float32'))
filterotsu = SimpleITK.OtsuMultipleThresholdsImageFilter()
filterotsu.SetNumberOfThresholds(1)
filterotsu.Execute(itkoutputImage)
imagethresholds = filterotsu.GetThresholds()
mean_wall = np.percentile(agarremovedoutputImage[agarremovedoutputImage>imagethresholds[0]],95)
print('Wall intensity: ' + str(mean_wall))
#set BG to agarose values
mask_eroded=scipy.ndimage.morphology.binary_erosion(maskImage,iterations=1)
outputImage[~mask_eroded]=mean_agarose
#normalize
pixdat_norm = (outputImage.astype('float64') - mean_agarose)/(mean_wall-mean_agarose)
niinormalizedoutput = nib.Nifti1Image(pixdat_norm,aff,head)
nib.save(niinormalizedoutput,scanname + '_normalized.nii') 
