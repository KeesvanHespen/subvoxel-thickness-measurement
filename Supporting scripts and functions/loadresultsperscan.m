%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% Loads the neural network results and prepares   %
% them to be visualized by Quick_big_data_surf.m  %
% or the function quick_big_data_surf.m           %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
dir='C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\4. Results\';
res=readNPY(strcat(dir,'results_vsize_0.4004009123124_acq_0_2020-06-29_08;25;43_testsetresults_LR.npy')); %0_2020-06-22_17;02;04_
res=readNPY(strcat(dir,'results_vsize_0.4004009123124_acq_0_2020-06-29_13;37;55_testsetresults_LR.npy')); %0_2020-06-22_17;02;04_

listo=unique(res(:,5))';
% remove scans that were incorrectly added to this test set
res = res(res(:,5)~=30,:);
res = res(res(:,5)~=23,:);
res = res(res(:,5)~=26,:);
HRLRresults = [res(:,6),res(:,1)];

%%
dir='C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\4. Results\';
res=readNPY(strcat(dir,'results_vsize_0.4004009123126_acq_0_2020-06-29_08;54;09_testsetresults_LR.npy'));%7_2020-05-16_09;15;56_
res=readNPY(strcat(dir,'results_vsize_0.4004009123126_acq_0_2020-06-29_11;00;53_testsetresults_LR.npy'));

listo=unique(res(:,5))';
% remove scans that were incorrectly added to this test set
res = res(res(:,5)~=10,:);
HRLRresults = [res(:,6),res(:,1)];

%% loads and plots the results for the various otsu threshold settings.

dir='C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\4. Results\';
HRLRresults95=readNPY(strcat(dir,'results_vsize_0.4000370037_acq_0_2020-06-09_14;16;24_LR_95percentotsu.npy'));
HRLRresults90=readNPY(strcat(dir,'results_vsize_0.4000370037_acq_0_2020-06-09_14;39;39_LR_90percentotsu.npy'));
HRLRresults100=readNPY(strcat(dir,'results_vsize_0.4000370037_acq_0_2020-06-09_14;56;20_LR_100percentotsu.npy'));
HRLRresults105=readNPY(strcat(dir,'results_vsize_0.4000370037_acq_0_2020-06-09_15;28;35_LR_105percentotsu.npy'));
HRLRresults110=readNPY(strcat(dir,'results_vsize_0.4000370037_acq_0_2020-06-09_15;44;18_LR_110percentotsu.npy'));

otsuthreshresults = [HRLRresults90(:,1),HRLRresults95(:,1),HRLRresults100(:,1),HRLRresults105(:,1),HRLRresults110(:,1)];
figure;
set(gca,'LineWidth',2)
set(gca,'FontSize',14,'YGrid','on')
highresvals=HRLRresults100(:,1);
for line =1:5
    lowresvals = otsuthreshresults(:,line)./HRLRresults100(:,1)*100-100+1e-6;
    [his,bars] = hist3(HRLRresults100,[A A]);
    mean(lowresvals)
    std(lowresvals)
    median(lowresvals)
    iqr(lowresvals)
    xhold=[];
    med=[];
    values={};
    intqr=[];
    quantile25=[];
    quantile75=[];

    for linepoint=1:size(his,1)
        if sum(his(:,linepoint))<1 || bars{2}(1,linepoint)>10
             his(:,linepoint)=zeros(size(his(:,linepoint)));
             values{1,linepoint} = bars{2}(1,linepoint);
             values{2,linepoint} = nan;
        else
            if ~exist(('maxval'))
                ll=linepoint;
                dataline = (highresvals<(bars{2}(1,linepoint)+(bars{2}(1,2)-bars{2}(1,1))/2) & highresvals>(bars{2}(1,linepoint)-(bars{2}(1,2)-bars{2}(1,1))/2));

                inrangevalues = lowresvals.*dataline;

                inrangevalues = inrangevalues(inrangevalues~=0);
                minval=0;
            end
            maxval = max(his(:,linepoint));
            val=bars{2}(1,linepoint);
            if maxval~=0
               his(:,linepoint) = his(:,linepoint);

            end

            dataline = (highresvals<(bars{2}(1,linepoint)+(bars{2}(1,2)-bars{2}(1,1))/2)& highresvals>(bars{2}(1,linepoint)-(bars{2}(1,2)-bars{2}(1,1))/2));
            count_per_bin(linepoint)=sum(dataline);
            inrangevalues = lowresvals.*dataline;
            inrangevalues = inrangevalues(inrangevalues~=0);
            bb(linepoint)=numel(inrangevalues);
            values{1,linepoint} = bars{2}(1,linepoint);
            values{2,linepoint} = inrangevalues;


            xhold=[xhold bars{2}(1,linepoint)];
            alpha=1;
            if alpha
                med=[med median(inrangevalues)];
                quantile25 = [quantile25 quantile(inrangevalues,0.25)];
                quantile75 = [quantile75 quantile(inrangevalues,0.75)];
                quantile25(isnan(quantile25))=0;
                quantile75(isnan(quantile75))=0;
            else
                med=[med mean(inrangevalues)];
                quantile25= [quantile25 std(inrangevalues)];
                quantile75= [quantile75 std(inrangevalues)];
                quantile25(isnan(quantile25))=0;
                quantile75(isnan(quantile75))=0;
            end
        end


    end

    hold on;
    xlabel('Ground truth thickness (mm)')
    ylabel('Change in estimated thickness (%)')
    plot(xhold,med,'LineStyle','-','LineWidth',2)
    xticks([0:0.2:1.5])
    xlim([0,1.5])
    legend('90%','95%','100%','105%','110%','Location','northeastoutside')
    box on;

end