clear all
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  File used to concatenate all results  %
%  Generated for the test time           %
%  augmentation and dropout              %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
filename = 'results_vsize_0.4000370037_acq_0_2019-04-03_15;30;17_dropoutLR.npy';


HRLRresults_temp = readNPY(filename);
HRLRresults_temp = [HRLRresults_temp(:,2),HRLRresults_temp(:,1)];
for i=1:99
    temp =  readNPY(strrep(filename,'_0_','_'+string(i)+'_'));
    if size(temp,1)==size(HRLRresults_temp,1)
        HRLRresults_temp = cat(2,HRLRresults_temp,temp(:,1));
    end
end
lr= HRLRresults_temp(:,2:end);
hr = HRLRresults_temp(:,1);
med_values= median(lr,2);
iqr_values= iqr(lr,2);
std_values= std(lr./hr,0,2);
dif = lr-HRLRresults_temp(:,1);
%HRLRresults = [med_values,hr];
HRLRresults = [std_values,hr];%abs(med_values-hr)./hr];
sprintf(' mean squared error')
mean((HRLRresults(:,1)-HRLRresults(:,2)).^2)
sprintf('corr coef')
corr(hr,med_values)
for lineval = 1:numel(hr)
    n=hist(lr(lineval,:));

    p = n / sum(n);
    entropy(lineval) = -sum(p .*log(p));
end
figure; histogram(iqr_values,'BinWidth',0.002,'FaceColor',[0 0.4470 0.7410],'EdgeColor','k','FaceAlpha',1)
xlim([0.00,0.2]); ylim([0 3000]); set(gca,'FontSize',16,'linewidth',2)
xlabel('MC dropout uncertainty (mm)'); ylabel('Frequency');
%%

clear all

filename = 'results_vsize_0.4000370037_acq_0_2019-04-25_09;38;44_augmented+translatedLR.npy';


HRLRresults_temp = readNPY(filename);
HRLRresults_temp = [HRLRresults_temp(:,2),HRLRresults_temp(:,1)];
for i=1:99
    temp =  readNPY(strrep(filename,'_0_','_'+string(i)+'_'));
    uu(i)=mean((temp(:,1)-temp(:,2)).^2);
    vv(i)=size(temp,1);
    if size(temp,1)==size(HRLRresults_temp,1)
        HRLRresults_temp = cat(2,HRLRresults_temp,temp(:,1));
    end
end
lr= HRLRresults_temp(:,2:end);
hr = HRLRresults_temp(:,1);
med_values= mean(lr,2);
iqr_values= iqr(lr,2);
std_values= std(lr,0,2);
dif = lr-HRLRresults_temp(:,1);
%HRLRresults = [std_values,abs(med_values-hr)./hr];
HRLRresults = [std_values,hr];
sprintf('mean squared error')
mean((HRLRresults(:,1)-HRLRresults(:,2)).^2)
sprintf('corr coef')
corr(hr,med_values)
sprintf('average uncertainty')
mean(std(dif,0,2))
for lineval = 1:numel(hr)
    n=hist(lr(lineval,:));

    p = n / sum(n);
    entropy(lineval) = -sum(p .*log(p));
end
figure; histogram(std_values,'BinWidth',0.002,'FaceColor',[0 0.4470 0.7410],'EdgeColor','k','FaceAlpha',1)
xlim([0.00,0.2]); ylim([0 3000])
xlabel('TTA uncertainty (mm)'); ylabel('Frequency');set(gca,'FontSize',16,'linewidth',2)