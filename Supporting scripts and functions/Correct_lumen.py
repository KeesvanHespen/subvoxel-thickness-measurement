"""
  Used to correct the vessel lumen of the magnitude images of the low resolution vessel wall images.
  The chosen image in the Import screen should be the real image.

  SetWall: Value larger than 0: Inverts intensities for slices from value onward
           Value smaller than 0: Inverts intensities for slices until value
           Skip flip allows to skip the intensity inversion, if the wall is positive for every slice.

  LoadMag: Load the magnitude image
  SetAgarAndMean: Set the lumen intensities and the agar background intensities. Results in the segmented lumen to be put at agar background intensity values

  Results: Show corrected vessel lumen.

  KM van Hespen, UMC Utrecht, 2020
"""
from mevis import *
import numpy as np
import os
import sys
import matplotlib.pyplot as plt 
import datetime
import pydicom
import subprocess as subp

def flipslices():
  # Inverts image intensities in the real image
  if ctx.field("flipslice").value>0:
    gradsign=1
  else: 
    gradsign = -1
  fliploc = np.abs(ctx.field("flipslice").value)

  img = ctx.field("ImagePropertyConvert.output0").image()
  img= img.getTile( (0,0,0), (img.UseImageExtent,img.UseImageExtent,img.UseImageExtent) )
  img = img.astype('int16')
    
  img=img-2047
  if gradsign>0:
    img[fliploc-1:]*=-1
  else:
    img[0:fliploc+1]*=-1
  
  img=img+2047
  interface = ctx.module("PythonImage").call("getInterface")
  interface.setImage(img,minMaxValues = (0,4096))

def skipslices():
  img = ctx.field("DirectDicomImport1.output0").image()
  img= img.getTile( (0,0,0), (img.UseImageExtent,img.UseImageExtent,img.UseImageExtent) )
  
  interface = ctx.module("PythonImage").call("getInterface")
  interface.setImage(img,minMaxValues = (0,4096))  
  
def setwallvalues():
  #the correctly flipped real image
  img = ctx.field("PythonImage.output0").image()
  img = img.getTile( (0,0,0), (img.UseImageExtent,img.UseImageExtent,img.UseImageExtent) )
  # the selected lumen mask
  mask = ctx.field("IntervalThreshold1.output0").image()
  mask = mask.getTile( (0,0,0), (mask.UseImageExtent,mask.UseImageExtent,mask.UseImageExtent) )
  # the loaded magnitude image
  img2 = ctx.field("itkImageFileReader.output0").image()
  img2 = img2.getTile( (0,0,0), (img2.UseImageExtent,img2.UseImageExtent,img2.UseImageExtent) )
  substitute_vals = np.random.normal(ctx.field("ImageStatistics2.innerMean").floatValue(),ctx.field("ImageStatistics2.innerStdDev").floatValue(),img2.shape)
  img2[mask == 1] = substitute_vals[mask==1]
  
  interface = ctx.module("PythonImage1").call("getInterface")
  interface.setImage(img2,minMaxValues = (ctx.field("Info2.minValue").floatValue()-0.05,ctx.field("Info2.maxValue").floatValue()))
  
def saveit():
 
  ctx.field("itkImageFileWriter1.save").touch()
  ctx.field("ImageSave1.save").touch()
  print('Saved it!')