# -*- coding: utf-8 -*-
"""
Created on Thu May 24 09:40:15 2018

@author: khespen
"""
import numpy as np
import nibabel as nib
import os
import SimpleITK
import subprocess as subp
import scipy 

from mode import mode


def normalize_input(filename,ANTSdir):
    niiinputImage = nib.load(filename+'.nii')
    inputImage= niiinputImage.get_data();
    aff = niiinputImage.affine;
    head = niiinputImage.header;
    #apply otsu
    filterotsu = SimpleITK.OtsuMultipleThresholdsImageFilter()
    filterotsu.SetNumberOfThresholds(1)
    filterotsu.Execute(SimpleITK.GetImageFromArray(inputImage.astype('float32')))
    imagethresholds = filterotsu.GetThresholds()
    maskImage = inputImage>imagethresholds[0]
    maskImage = maskImage.astype(int)
    niimaskImage = nib.Nifti1Image(maskImage,aff,head)
    nib.save(niimaskImage,filename + '_mask.nii')
    print('Computing background correction. Do not close command window! Could take a while')
    if not os.path.isfile(filename + '_biascorrected2.nii'):
        systemcall = subp.call(ANTSdir + ' -i "'+ filename + '.nii'+'" -o "'+ filename+'_biascorrected.nii"' + ' -x "'+ filename + '_mask.nii" -s 5 -t [0.05] -c [50x50x50x50,0.000]')
        systemcall = subp.call(ANTSdir + ' -i "'+ filename + '_biascorrected.nii'+'" -o "'+ filename+'_biascorrected2.nii"' + ' -x "'+ filename + '_mask.nii" -s 5 -t [0.05] -c [50x50x50x50,0.000]')
   
        if systemcall == 0:
            print('Failed to run N4. Further computations will probably crash!!')
        
        niioutputImage = nib.load(filename+'_biascorrected2.nii')
        
        outputImage = niioutputImage.get_data()
        outputImage = outputImage.astype('float16')
        compiledoutputImage = nib.Nifti1Image(outputImage,aff,head)
        
        niioutputImage=[]
        os.remove(filename+'_biascorrected.nii')
        os.remove(filename+'_biascorrected2.nii')
        nib.save(compiledoutputImage,filename + '_biascorrected.nii') 
    else:
        compiledoutputImage = nib.load(filename + '_biascorrected.nii') 
        outputImage = compiledoutputImage.get_data()
        outputImage = outputImage.astype('float16')
    #normalize between all sets
    reader=SimpleITK.ImageFileReader()
    reader.SetFileName(filename)
   
    #remove temporary dicom
    #os.remove(os.path.join(datadir,'temp.dcm'))
    #crop dicom to modulus
    #apply otsu
    BGremovedoutputImage = outputImage[outputImage>imagethresholds]
    mean_agarose =  mode(BGremovedoutputImage)[0]
    print(mean_agarose)
    #get wall values
    agarremovedoutputImage = outputImage[outputImage>mean_agarose]
    itkoutputImage=SimpleITK.GetImageFromArray(agarremovedoutputImage[...,None].astype('float32'))
    filterotsu = SimpleITK.OtsuMultipleThresholdsImageFilter()
    filterotsu.SetNumberOfThresholds(1)
    filterotsu.Execute(itkoutputImage)
    imagethresholds = filterotsu.GetThresholds()
    mean_wall = np.percentile(agarremovedoutputImage[agarremovedoutputImage>imagethresholds[0]],95)
    print(mean_wall)
    #set BG to agarose values
    mask_eroded=scipy.ndimage.morphology.binary_erosion(maskImage,iterations=1)
    outputImage[~mask_eroded]=mean_agarose
    #normalize
    pixdat_norm = (outputImage.astype('float64') - mean_agarose)/(mean_wall-mean_agarose)
    niinormalizedoutput = nib.Nifti1Image(pixdat_norm,aff,head)
    nib.save(niinormalizedoutput,filename + '_normalized.nii') 
    return pixdat_norm
