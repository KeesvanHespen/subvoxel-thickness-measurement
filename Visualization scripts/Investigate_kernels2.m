function varargout = Investigate_kernels(varargin)
% INVESTIGATE_KERNELS MATLAB code for Investigate_kernels.fig
%      INVESTIGATE_KERNELS, by itself, creates a new INVESTIGATE_KERNELS or raises the existing
%      singleton*.
%
%      H = INVESTIGATE_KERNELS returns the handle to a new INVESTIGATE_KERNELS or the handle to
%      the existing singleton*.
%
%      INVESTIGATE_KERNELS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in INVESTIGATE_KERNELS.M with the given input arguments.
%
%      INVESTIGATE_KERNELS('Property','Value',...) creates a new INVESTIGATE_KERNELS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before Investigate_kernels_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to Investigate_kernels_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%      
%       Investigate_kernels is used to visualize the weights of the kernels
%       used in the neural network thickness estimation script. Inputs are
%       a vessel wall image and the saved network weights.
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help Investigate_kernels

% Last Modified by GUIDE v2.5 05-Feb-2018 17:03:11

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @Investigate_kernels_OpeningFcn, ...
                   'gui_OutputFcn',  @Investigate_kernels_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT




% --- Executes just before Investigate_kernels is made visible.
function Investigate_kernels_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to Investigate_kernels (see VARARGIN)

% Choose default command line output for Investigate_kernels
handles.output = hObject;
handles.clb='';
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes Investigate_kernels wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = Investigate_kernels_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
% function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.text7,'String','Thinking. Hold on!')
set(handles.text8,'Visible','off')
set(handles.text4,'String','')
colormap parula
delete(handles.clb)
handles.stored_kernel=1;
try
    handles.gridsize = str2num(get(handles.edit1,'string'));
    handles.edit1.Value = handles.gridsize;
    if handles.gridsize<=0 || handles.gridsize>12
        handles.gridsize = 8;
        set(handles.edit1,'value',8)
                set(handles.edit1,'String','8')
    end
catch
    handles.gridsize = 8;
    set(handles.edit1,'value',8)
    set(handles.edit1,'String','8')
end
set(handles.axes7,'HandleVisibility', 'off');
try
    for subp = 1:numel(handles.ax)
        set(handles.ax(subp),'Visible','off');cla(handles.ax(subp));
    end
catch
end
delete(get(handles.uipanel3,'Children'))
set(handles.popupmenu1,'String','Select network file');
[FileName,PathName] = uigetfile('*.mat','Select saved neural network file');
handles.network = load(fullfile(PathName,FileName));
try
    set(handles.popupmenu1,'String',handles.network.layers);
catch
    set(handles.popupmenu1,'String','Something went wrong');   
end
handles.selected_kernels = handles.network.weights{1};
set(handles.slider1,'Enable','off')
set(handles.slider3,'Enable','off')
if sum(size(handles.selected_kernels)~=1)<1 %Singular values
    set(handles.text8,'String',num2str(handles.selected_kernels))
    set(handles.text8,'Visible','on')
elseif sum(size(handles.selected_kernels)~=1)<2 %Vector data
    hax = axes('Parent',handles.uipanel3);
    plot(hax,handles.selected_kernels,'*-');
    set(handles.text4,'String','')
elseif numel(size(handles.selected_kernels))<3 %2D matrix
    hax = axes('Parent',handles.uipanel3);
    plot(hax,handles.selected_kernels(:,1),'*-'); 
    set(handles.text4,'String','1')
    set(handles.slider1,'Enable','on')
    set(handles.slider1,'Min',1,'Max',size(handles.selected_kernels,2),'Value',1);
    set(handles.slider1,'SliderStep',[1/(size(handles.selected_kernels,2)-1) 10/(size(handles.selected_kernels,2)-1)]);
    axis(hax,'tight')
else % Convnet kernels
    handles.dcm = datacursormode(gcf);
    handles.sz=size(handles.selected_kernels);
    set(handles.text4,'String','1')
    set(handles.slider1,'Enable','on')
    set(handles.slider3,'Enable','on')
    set(handles.slider1,'Min',1,'Max',size(handles.selected_kernels,5),'Value',1);
    set(handles.slider1,'SliderStep',[1/(size(handles.selected_kernels,5)-1) 10/(size(handles.selected_kernels,5)-1)]);
    if ~(handles.sz(2)==1)
        set(handles.text9,'String','1')
        set(handles.slider3,'Enable','on')
        set(handles.slider3,'Min',1,'Max',size(handles.selected_kernels,2),'Value',1);
        set(handles.slider3,'SliderStep',[1/(size(handles.selected_kernels,2)-1) 10/(size(handles.selected_kernels,2)-1)]);
    else
        set(handles.text9,'String','')
        set(handles.slider3,'Enable','off')
    end
    for kernel = 1:handles.sz(1)
        
         ax = scrollsubplot(handles.gridsize,handles.gridsize,kernel);
         
         imagesc(squeeze(handles.selected_kernels(kernel,1,:,:,1)),'Tag',num2str(kernel));
         caxis([min(handles.selected_kernels(:)) max(handles.selected_kernels(:))]);
         axis(ax,'off')
         
         colormap gray
         caxis([-0.0 0.6])
         ax.Parent = handles.uipanel3;

    end
    set(handles.axes7,'HandleVisibility', 'on');
    handles.clb = colorbar(handles.axes7);
    
    handles.clb.Position(3)=0.2;
    caxis(handles.axes7,[-0.1 0.1]);%min(handles.selected_kernels(:)) max(handles.selected_kernels(:))])

    set(handles.axes7,'HandleVisibility', 'off');
end
set(handles.popupmenu1,'Enable','on')
set(handles.text7,'String','')
guidata(hObject,handles)


% --- Executes on selection change in popupmenu1.
function popupmenu1_Callback(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.selected_kernels = handles.network.weights{handles.popupmenu1.Value};
set(handles.text7,'String','Thinking. Hold on!')
set(handles.text8,'Visible','off')
handles.stored_kernel=1;
try
    handles.gridsize = str2num(get(handles.edit1,'string'));
    handles.edit1.Value = handles.gridsize;
    if handles.gridsize<=0 || handles.gridsize>12
        handles.gridsize = 8;
        set(handles.edit1,'value',8)
        set(handles.edit1,'String','8')
    end
catch
    handles.gridsize = 8;
    set(handles.edit1,'value',8)
    set(handles.edit1,'String','8')
end
delete(get(handles.uipanel3,'Children'))
delete(handles.clb)
set(handles.slider1,'Enable','off')
set(handles.slider3,'Enable','off')
set(handles.text4,'String','')
if sum(size(handles.selected_kernels)~=1)<1 %Singular value
    set(handles.text8,'Visible','on')
    set(handles.text8,'String',num2str(handles.selected_kernels))

elseif sum(size(handles.selected_kernels)~=1)<2 %Single vector data
    hax = axes('Parent',handles.uipanel3);
    plot(hax,handles.selected_kernels,'*-');
    axis(hax,'tight')
elseif numel(size(handles.selected_kernels))<3 %2D Matrix data
    hax = axes('Parent',handles.uipanel3);
    plot(hax,handles.selected_kernels(:,1),'*-');  
    set(handles.text4,'String','1')
    set(handles.slider1,'Enable','on')
    set(handles.slider1,'Min',1,'Max',size(handles.selected_kernels,2),'Value',1);
    set(handles.slider1,'SliderStep',[1/(size(handles.selected_kernels,2)-1) 10/(size(handles.selected_kernels,2)-1)]);
    axis(hax,'tight')
else % Convnet kernels
    handles.dcm = datacursormode(gcf);
    handles.sz=size(handles.selected_kernels);
    set(handles.text4,'String','1')
    set(handles.slider1,'Enable','on')

    set(handles.slider1,'Min',1,'Max',size(handles.selected_kernels,5),'Value',1);
    set(handles.slider1,'SliderStep',[1/(size(handles.selected_kernels,5)-1) 10/(size(handles.selected_kernels,5)-1)]);
    if ~(handles.sz(2)==1)
        set(handles.text9,'String','1')
        set(handles.slider3,'Enable','on')
        set(handles.slider3,'Min',1,'Max',size(handles.selected_kernels,2),'Value',1);
        set(handles.slider3,'SliderStep',[1/(size(handles.selected_kernels,2)-1) 10/(size(handles.selected_kernels,2)-1)]);
    else
        set(handles.text9,'String','')
        set(handles.slider3,'Enable','off')
    end
    for kernel = 1:handles.sz(1)
         
         ax = scrollsubplot(handles.gridsize,handles.gridsize,kernel);
         
         imagesc(squeeze(handles.selected_kernels(kernel,1,:,:,1)),'Tag',num2str(kernel));
         caxis([min(handles.selected_kernels(:)) max(handles.selected_kernels(:))]);
         ax.Parent = handles.uipanel3;
         colormap gray
         caxis([-0.1 0.1])
         axis(ax,'off')   
    end
    set(handles.axes7,'HandleVisibility', 'on');
    handles.clb = colorbar(handles.axes7);
    handles.clb.Position(3)=0.2;
    caxis(handles.axes7,[-0.1 0.1]);%min(handles.selected_kernels(:)) max(handles.selected_kernels(:))])

    set(handles.axes7,'HandleVisibility', 'off');
end
set(handles.text7,'String','')
guidata(hObject,handles)
% Hints: contents = cellstr(get(hObject,'String')) returns popupmenu1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from popupmenu1


% --- Executes during object creation, after setting all properties.
function popupmenu1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to popupmenu1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: popupmenu controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
slice = round(get(hObject,'Value'));
set(handles.text4,'String',num2str(slice))
delete(get(handles.uipanel3,'Children'))
set(handles.text7,'String','Thinking. Hold on!')
try
    handles.stored_kernel = str2num(get(handles.text9,'string'));
catch
    print(' Dont know how i got here. Something bad happened')
end
if isempty(handles.stored_kernel)
    handles.stored_kernel=1;
end
if numel(size(handles.selected_kernels))<3
    hax = axes('Parent',handles.uipanel3);
    plot(hax,handles.selected_kernels(:,slice),'*-');
else
    for kernel = 1:handles.sz(1)
         ax = scrollsubplot(handles.gridsize,handles.gridsize,kernel);
         imagesc(squeeze(handles.selected_kernels(kernel,handles.stored_kernel,:,:,slice)),'Tag',num2str(kernel));
         caxis([min(handles.selected_kernels(:)) max(handles.selected_kernels(:))]);
         ax.Parent = handles.uipanel3;
         axis(ax,'off')   
    end
end
set(handles.text7,'String','')


% --- Executes on slider movement.
function slider3_Callback(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
slice = round(get(hObject,'Value'));
set(handles.text9,'String',num2str(slice))
delete(get(handles.uipanel3,'Children'))
set(handles.text7,'String','Thinking. Hold on!')
try
    stored_kernel = str2num(get(handles.text4,'string'));
catch
    print(' Dont know how i got here. Something bad happened')
end
if isempty(stored_kernel)
    stored_kernel=1;
end

for kernel = 1:handles.sz(1)
     ax = scrollsubplot(handles.gridsize,handles.gridsize,kernel);
     imagesc(squeeze(handles.selected_kernels(kernel,slice,:,:,stored_kernel)),'Tag',num2str(kernel));
     caxis([min(handles.selected_kernels(:)) max(handles.selected_kernels(:))]);
     ax.Parent = handles.uipanel3;
     axis(ax,'off')   
end
set(handles.text7,'String','')

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider


% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called


% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes during object creation, after setting all properties.
function hSld_CreateFcn(hObject, eventdata, handles)
% hObject    handle to hSld (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on slider movement.
function hSld_Callback(hObject, eventdata, handles)
% hObject    handle to hSld (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider



function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end

% --- Executes during object creation, after setting all properties.
function slider3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end

function [output_txt,handles] = myfunc(hObject,event_obj,handles)
% Display the position of the data cursor
% obj          Currently not used (empty)
% event_obj    Handle to event object
% output_txt   Data cursor text string (string or cell array of strings).

kernel = str2double(get(get(event_obj,'Target'),'Tag'));
handles.selected_kernels = handles.network.weights{handles.popupmenu1.Value};
selected_patch = 27692;
try
    if ndims(handles.image)==5
        figure; hold on;

        for kernel = 1:size(handles.selected_kernels,1)
            subplot(4,4,kernel)
            original_image = handles.image(selected_patch,:,:,:,:);
            convedimg = convn(original_image,handles.selected_kernels(kernel,handles.stored_kernel,:,:,:,:),'same');
            crop = (size(handles.selected_kernels,5)-1)/2;
            resizedimg = imresize(squeeze(convedimg(1,1,crop+1:end-crop-1,crop+1:end-crop-1,10)),5);
            imagesc(resizedimg);
            caxis([-0.5 0.5])
            
            axis off
            colormap gray
        end
        figure;
        imagesc(imresize(squeeze(handles.image(selected_patch,1,:,:,10)),5))
        
        axis off
        colormap gray
    else
        original_image = squeeze(handles.image);
        convedimg = convn(original_image,squeeze(handles.selected_kernels(kernel,handles.stored_kernel,:,:,:,:)),'same');
    end
catch
    warning('Probably need to open image file to get this to work')
    output_txt={['Probably need to open image file to get this to work']};
end
figure;
imagebrowse(cat(ndims(convedimg)+1,double(convedimg),original_image))

output_txt={['See produced figure']};
guidata(hObject,handles)



% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FileName,PathName,filtindex] = uigetfile({'*.dcm';'*.nii';'*.npy';'*.mat'},'Select saved dicom image');
if filtindex==2
   handles.image = load_untouch_nii(fullfile(PathName,FileName));
   handles.image = handles.image.img;
   handles.image = permute(handles.image,[2 1 3]);
elseif filtindex==3
   handles.image = readNPY(fullfile(PathName,FileName));
elseif filtindex==4
   patches = load(fullfile(PathName,FileName));
   handles.image = patches.results;
else
   handles.image = dicomread(fullfile(PathName,FileName)); 
end
set(handles.dcm,'UpdateFcn',{@myfunc,handles});
guidata(hObject,handles)


% --- Executes during object creation, after setting all properties.
function uipanel3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to uipanel3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called
