#!/usr/bin/env python

"""
Used to visualize the ground truth thickness measurements (FWHM) and those made by the neural network (NN) on the low resolution images, in 2D and 3D.

Input needed is a NN results file, with extension ....setresults.npy. 
If scanindexdict.txt is OK, the Dicoms are automatically correctly linked to the indices used in the ....setresults.npy file


KM van Hespen, UMC Utrecht, 2020"""

from mevis import *
import numpy as np
import os
import scipy.ndimage 
import scipy.io 
import sklearn.linear_model
import glob
import shutil
import matplotlib.pyplot as plt 
import matplotlib as mpl
from scipy.interpolate import RegularGridInterpolator
import h5py

def load_all_data():
  # loads the vessel wall image, and visualizes the thickness map in the image domain of the vessel wall image
  global fwhm_orientation,fwhm_results,MCA_mask
  if ctx.field("currenttab").value==0 and ctx.field("tab_memory").value!=0:
    ctx.field("SoView2DMarkerEditor.deleteAll").touch()
    ctx.field("VectorLine1.show").setValue(False)
    ctx.field("tab_memory").setValue(0)
  if ctx.field("currenttab").value>0 and ctx.field("tab_memory").value==0:
    ctx.field("Reference_image.filename").setValue(ctx.field("dicom_folder_location").value)
    magnitude = ctx.field("ImageLoad.output0").image()
    magnitude = magnitude.getTile( (0,0,0), (magnitude.UseImageExtent,magnitude.UseImageExtent,magnitude.UseImageExtent) )   
    fwhm_temp = np.zeros((magnitude.shape))
    NN_temp = np.zeros((magnitude.shape))
    selected_thicknesses = np.load(ctx.field("results_location").value)
    selected_thicknesses = selected_thicknesses[selected_thicknesses[:,4]==int(ctx.field("Index_list.name").value),:]
    for rows in range(0,selected_thicknesses.shape[0]):
      fwhm_temp[int(selected_thicknesses[rows,3]),int(selected_thicknesses[rows,2]),int(selected_thicknesses[rows,1])]=selected_thicknesses[rows,0]
      NN_temp[int(selected_thicknesses[rows,3]),int(selected_thicknesses[rows,2]),int(selected_thicknesses[rows,1])]=selected_thicknesses[rows,-1]
    interface = ctx.module("NN_image").call("getInterface")
    interface.setImage(NN_temp, minMaxValues =(np.min(NN_temp),np.max(NN_temp)))
    interface2 = ctx.module("FWHM_image").call("getInterface")
    interface2.setImage(fwhm_temp, minMaxValues =(np.min(fwhm_temp),np.max(fwhm_temp)))
 
    ctx.field("tab_memory").setValue(1)
    ctx.field("SoExaminerViewer.rescanScene").touch()
    ctx.field("Histogram1.update").touch()
    ctx.field("SoLUTEditor3.updateRangeFromHistogram").touch()
    
def set_canvas():
  # sets the canvas for the matplotlib plot, used to visualize the intensity profile along the vessel normal. 
  if ctx.field("currenttab").value==1:
    global subplot,ax, backgrounds
    control = ctx.control("canvas_intensity").object()
    control.figure().clear()
    # clear the user interaction of plotC example:
    control.myUserInteraction = None
  
    subplot = ctx.control("canvas_intensity").object().figure()
    ax = subplot.add_subplot(111)
    ax.set_xlabel('Position') 
    ax.set_ylabel('Intensity') 
    ax.set_title('') 
    
    # We need to draw the canvas before we start animating...
    subplot.canvas.draw()
    backgrounds = subplot.canvas.copy_from_bbox(subplot.bbox)

  
def interpolate_image():
  global fwhm_orientation, subplot,ax, backgrounds, intensity_profile,lineproflength
  # Used to create the intensity profile along the vessel wall. 
  if ctx.field("XMarkerListContainer.numItems").value>0:
    if fwhm_orientation[ctx.field("WorldVoxelConvert1.voxelX").intValue(),ctx.field("WorldVoxelConvert1.voxelY").intValue(),ctx.field("WorldVoxelConvert1.voxelZ").intValue(),0]>0:
      ctx.field("VectorLine1.show").setValue(True)
      magnitude = ctx.field("ImageLoad.output0").image()
      magnitude = magnitude.getTile( (0,0,0), (magnitude.UseImageExtent,magnitude.UseImageExtent,magnitude.UseImageExtent) )   
      phi = fwhm_orientation[ctx.field("WorldVoxelConvert1.voxelX").intValue(),ctx.field("WorldVoxelConvert1.voxelY").intValue(),ctx.field("WorldVoxelConvert1.voxelZ").intValue(),1]
      theta = fwhm_orientation[ctx.field("WorldVoxelConvert1.voxelX").intValue(),ctx.field("WorldVoxelConvert1.voxelY").intValue(),ctx.field("WorldVoxelConvert1.voxelZ").intValue(),2]
      linefits=0
      lineproflength=5
      while not linefits and lineproflength>0:
        lineproflength = lineproflength-1
        steps=100
        xline = np.linspace(ctx.field("WorldVoxelConvert1.worldX").value - (lineproflength / 2 * np.sin(theta) * np.cos(phi)) ,ctx.field("WorldVoxelConvert1.worldX").value + (lineproflength / 2 * np.sin(theta) * np.cos(phi)), steps)
        yline = np.linspace(ctx.field("WorldVoxelConvert1.worldY").value - (lineproflength / 2 * np.sin(theta) * np.sin(phi)), ctx.field("WorldVoxelConvert1.worldY").value + (lineproflength / 2 * np.sin(theta) * np.sin(phi)), steps)
        zline = np.linspace(ctx.field("WorldVoxelConvert1.worldZ").value - lineproflength / 2 * (np.cos(theta)), ctx.field("WorldVoxelConvert1.worldZ").value + lineproflength / 2 * np.cos(theta), steps)
        x_mesh,y_mesh,z_mesh = np.linspace(0,magnitude.shape[0]-1,magnitude.shape[0]).astype(int),np.linspace(0,magnitude.shape[1]-1,magnitude.shape[1]).astype(int),np.linspace(0,magnitude.shape[2]-1,magnitude.shape[2]).astype(int)
        interp3 = RegularGridInterpolator((x_mesh,y_mesh,z_mesh),magnitude)
        worldcoords = np.array((xline,yline,zline,np.ones(zline.shape)))
        localcoords = np.dot(np.linalg.inv(ctx.field("Info1.worldMatrix").value),worldcoords)
        try:
          intensity_profile = interp3((localcoords[2,:],localcoords[0,:],localcoords[1,:]))
          linefits=1
        except:
          linefits=0
          
      control = ctx.control("canvas_intensity").object()
      #control.figure().clear()
      # clear the user interaction of plotC example:
      control.myUserInteraction = None
  
      
      x = np.linspace(0,lineproflength,steps)
      style='r-'
      def plot(ax, style):
        return ax.plot(x, intensity_profile, linestyle = '-', color = style, animated=True)[0]
      line = plot(ax,'r')
      
      subplot.canvas.restore_region(backgrounds)    
      line.set_ydata(intensity_profile)
      ax.set_ylim([0,np.max(intensity_profile)+10])
      ax.set_xlim([0,lineproflength])
      ctx.field("GetVoxelValue1.position").setStringValue(ctx.field("XMarkerListContainer.posXYZ").stringValue())
      ctx.field("GetVoxelValue1.update").touch()

      ax.set_title(str(ctx.field("GetVoxelValue1.outputValue").value)) 
      #ax.draw_artist(ax.get_yaxis())
      subplot.canvas.draw()
      ax.draw_artist(line)
      
      subplot.canvas.blit(ax.bbox)
      ctx.field("VectorLine.start").setValue(((worldcoords[2,0],worldcoords[0,0],worldcoords[1,0])))
      ctx.field("VectorLine.end").setValue(((worldcoords[2,-1]),worldcoords[0,-1],worldcoords[1,-1]))
    elif ctx.field("SoView2DMarkerEditor.editingOn").value==True:
      subplot.canvas.restore_region(backgrounds)  
      ctx.field("GetVoxelValue1.update").touch()
      ax.set_title(str(ctx.field("GetVoxelValue1.outputValue").value)) 
      subplot.canvas.draw()
      subplot.canvas.blit(ax.bbox)
def init():
  ctx.field("Index_list.deleteAll").touch()
  ctx.field("File_list.deleteAll").touch()
  ctx.field("FileDirectory.clear").touch()
  ctx.field("FileDirectory1.clear").touch()
  ctx.field("SoView2DMarkerEditor.deleteAll").touch()
  ctx.field("DrawVoxels3D1.clear").touch()
  ctx.field("BoolInt1.boolValue").setValue(False)
  
def set_load_different_dicom_default():

  ctx.field("dicom_folder_location").setValue(ctx.field("Reference_image.filename").value)
  ctx.field("FileDirectory.update").touch()
  ctx.field("FileDirectory1.update").touch()
  ctx.field("FieldIterator1.curIndex").setValue(0)
  
def get_indices():
  ctx.field("FieldBypass.noBypass").setValue(True)
  ctx.field("FieldBypass2.noBypass").setValue(True)
  ctx.field("FieldBypass3.noBypass").setValue(True)
  results = np.load(ctx.field("results_location").value)
  ctx.field("File_list.deleteAll").touch()
  ctx.field("Index_list.deleteAll").touch()
  ctx.field("SoView2DMarkerEditor.deleteAll").touch()
  unique_indices = np.unique(results[:,4]).astype(int)
  
  for ind in unique_indices:
    ctx.field("Index_list.add").touch()
    ctx.field("Index_list.name").setValue(str(ind))
    ctx.field("FieldIterator.curIndex").setValue(ind)
    ctx.field("File_list.add").touch()
    ctx.field("File_list.name").setValue(ctx.field("FieldIterator.curValue").value)
  ctx.field("Index_list.index").setValue(0)
  ctx.field("FieldBypass.noBypass").setValue(False)
  ctx.field("FieldBypass2.noBypass").setValue(False)
  ctx.field("FieldBypass3.noBypass").setValue(False)
  ctx.field("FieldIterator1.curIndex").setValue(0)
  
def draw_external_plot():
    global intensity_profile,lineproflength
    control = ctx.control("canvas_extra").object()
    control.figure().clear()
    # clear the user interaction of plotC example:
    control.myUserInteraction = None
  
    subplot = ctx.control("canvas_extra").object().figure()
    ax = subplot.add_subplot(111)
    ax.set_xlabel('Position') 
    ax.set_ylabel('Intensity') 
    ax.set_title('') 
    x = np.linspace(0,lineproflength,100)
    ax.grid(True,'both')
    ax.plot(x,intensity_profile)
    plt.show()

def save_edited_mask():
  global fwhm_results,MCA_mask
  # Allows to save different ROI masks, in case the FWHM measurements are incorrect. Not currently used.
  mask = ctx.field("DrawVoxels3D1.output0").image()
  mask = mask.getTile( (0,0,0), (mask.UseImageExtent,mask.UseImageExtent,mask.UseImageExtent) )
  mask = np.transpose(mask,(1,2,0))
  try:
    mask = MCA_mask['mask']*mask
  except:
    a=1
  try:
    fwhm_results['results']['thicknessmap_selection'][0][0] = fwhm_results['results']['thicknessmap_selection'][0][0]*mask
  except:
    fwhm_results['results']['thicknessmapselection'][0][0] = fwhm_results['results']['thicknessmapselection'][0][0]*mask 
  try:
    scipy.io.savemat(ctx.field("FileInformation1.path").value.replace('.mat','')+'_stricter_ROI.mat', {'results':fwhm_results['results']})
  except:
    a=1