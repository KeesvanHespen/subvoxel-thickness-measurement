function varargout = NNresultsevaluationGUI(varargin)
% NNRESULTSEVALUATIONGUI MATLAB code for NNresultsevaluationGUI.fig
%      NNRESULTSEVALUATIONGUI, by itself, creates a new NNRESULTSEVALUATIONGUI or raises the existing
%      singleton*.
%
%      H = NNRESULTSEVALUATIONGUI returns the handle to a new NNRESULTSEVALUATIONGUI or the handle to
%      the existing singleton*.
%
%      NNRESULTSEVALUATIONGUI('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in NNRESULTSEVALUATIONGUI.M with the given input arguments.
%
%      NNRESULTSEVALUATIONGUI('Property','Value',...) creates a new NNRESULTSEVALUATIONGUI or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before NNresultsevaluationGUI_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to NNresultsevaluationGUI_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help NNresultsevaluationGUI

% Last Modified by GUIDE v2.5 17-Jan-2018 13:58:39

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @NNresultsevaluationGUI_OpeningFcn, ...
                   'gui_OutputFcn',  @NNresultsevaluationGUI_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before NNresultsevaluationGUI is made visible.
function NNresultsevaluationGUI_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to NNresultsevaluationGUI (see VARARGIN)

% Choose default command line output for NNresultsevaluationGUI
handles.output = hObject;
handles.vars.path_inf = pwd;
handles.vars.path_dicom = pwd;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes NNresultsevaluationGUI wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = NNresultsevaluationGUI_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
cla(handles.axes1,'reset');
set(handles.axes1,'xtick',[],'ytick',[])
set(handles.axes3,'xtick',[],'ytick',[])
cla(handles.cbar,'reset');
set(handles.axes3,'xtick',[],'ytick',[])
cla(handles.scataxes,'reset');
set(handles.scataxes,'xtick',[],'ytick',[])

cla(handles.axes2,'reset');

catch
end
if isfield(handles.vars,'path_dicom')
    [filename, path] = uigetfile(fullfile(handles.vars.path_dicom,'*.dcm'), 'Select Dicom','MultiSelect', 'on');
else
    [filename, path] = uigetfile('*.dcm', 'Select Dicom','MultiSelect', 'on');
end    
handles.vars.filename_inf = filename;
handles.vars.path_inf = path;
if ~filename==0
    set(handles.edit2,'String',fullfile(path,filename))
    handles.vars.dicominfo = dicominfo(fullfile(path,filename));   
    
else
    set(handles.edit2,'String','Something went wrong')
    return;
end

guidata(hObject, handles);

% --- Executes on button press in pushbutton1.
function pushbutton6_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
set(handles.pushbutton3,'Enable','off')
try
cla(handles.axes1,'reset');
set(handles.axes1,'xtick',[],'ytick',[])
set(handles.axes3,'xtick',[],'ytick',[])
cla(handles.cbar,'reset');
set(handles.axes3,'xtick',[],'ytick',[])
cla(handles.scataxes,'reset');
set(handles.scataxes,'xtick',[],'ytick',[])

cla(handles.axes2,'reset');

catch
end
if isfield(handles.vars,'path_inf')
    [filename, path] = uigetfile(fullfile(handles.vars.path_inf,'*.mat'), 'Select FWHM results','MultiSelect', 'on');
else
    [filename, path] = uigetfile('*.mat', 'Select FWHM results','MultiSelect', 'on');
end
    handles.vars.filename_dicom = filename;
handles.vars.path_dicom = path;
if ~filename==0
    set(handles.edit5,'String',fullfile(path,filename))
    load(fullfile(path,filename));
    handles.vars.results = results;
    handles.vars.dicom = handles.vars.results.Image;   
    
else
    set(handles.edit5,'String','Something went wrong')
    return;
end
set(handles.pushbutton3,'Enable','on')
guidata(get(hObject,'parent'), handles);

% --- Executes on button press in pushbutton2.
function pushbutton2_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
cla(handles.axes1,'reset');
set(handles.axes1,'xtick',[],'ytick',[])
set(handles.axes3,'xtick',[],'ytick',[])
cla(handles.cbar,'reset');
set(handles.axes3,'xtick',[],'ytick',[])
cla(handles.scataxes,'reset');
set(handles.scataxes,'xtick',[],'ytick',[])

cla(handles.axes2,'reset');

catch
end
[filename, path] = uigetfile('.npy', 'Select *setresults.npy','MultiSelect', 'on');
handles.vars.filename_npy = filename;
handles.vars.path_npy = path;
if ~filename==0
    set(handles.edit1,'String',fullfile(path,filename))
    handles.vars.points = readNPY(fullfile(path,filename));
    listo = unique(handles.vars.points(:,5));
    listo = listo+1;
    try
        scanlist=handles.vars.dict(listo);
        [path,shortdicomnames,exten]=cellfun(@fileparts,scanlist,'un',0);
        shortdicomnames2 = strcat(string(listo-1),': ',shortdicomnames);
        [idx,~]=listdlg('PromptString','Select dicom file','ListString',cellstr(shortdicomnames2),'SelectionMode','single');

        handles.vars.filename_inf = char(strcat(shortdicomnames(idx),exten(idx)));
        handles.vars.path_inf = char(path(idx));
        if ~filename==0
            set(handles.edit2,'String',fullfile(char(path(idx)), char(strcat(shortdicomnames(idx),exten(idx)))))
            handles.vars.dicominfo = dicominfo(fullfile(char(path(idx)), char(strcat(shortdicomnames(idx),exten(idx)))));   

        else
            set(handles.edit2,'String','Something went wrong')
            return;
        end
    catch
    end
else
    set(handles.edit1,'String','Something went wrong')
end
guidata(hObject, handles);

% --- Executes on button press in pushbutton3.
function pushbutton3_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

try
cla(handles.axes1,'reset');
set(handles.axes1,'xtick',[],'ytick',[])
set(handles.axes3,'xtick',[],'ytick',[])
cla(handles.cbar,'reset');
set(handles.axes3,'xtick',[],'ytick',[])
cla(handles.scataxes,'reset');
set(handles.scataxes,'xtick',[],'ytick',[])

cla(handles.axes2,'reset');

catch
end

%% preprocessing directions for intensity plot
voxelsize=[getSubFieldval(handles.vars.dicominfo, 'PixelSpacing');getSubFieldval(handles.vars.dicominfo, 'SpacingBetweenSlices')];
handles.vars.dicom = double(squeeze(handles.vars.dicom));
Blockradius=[2,2,2];
[Gy,Gx,Gz] = gradient(handles.vars.dicom);
Gy = imgaussfilt(Gy,0.1/voxelsize(2));
Gx = imgaussfilt(Gx,0.1/voxelsize(1));
Gz = imgaussfilt(Gz,0.1/voxelsize(3));

[~,outlist_gradient] = Dicom_Info_To_World_Coords(handles.vars.dicominfo,0,[Gx(:),Gy(:),Gz(:)]);

G = reshape(outlist_gradient,[size(handles.vars.dicom),3]);
r=sqrt(G(:,:,:,1).^2+G(:,:,:,2).^2+G(:,:,:,3).^2);
phi = atan2(G(:,:,:,2),G(:,:,:,1));
theta = acos(G(:,:,:,3)./r);
theta(isnan(theta))=0;
handles.vars.direc_3D = cat(4,r,phi,theta);

%Angle calculation
handles.vars.phi = handles.vars.direc_3D(:,:,:,2);
handles.vars.theta = handles.vars.direc_3D(:,:,:,3);
handles.vars.r = handles.vars.direc_3D(:,:,:,1);

handles.vars.paddedphi2 = padarray(handles.vars.phi,Blockradius);
handles.vars.paddedtheta2 = padarray(handles.vars.theta,Blockradius);
for ind=1:numel(handles.vars.dict)
    findindex = strcmp(handles.vars.dict{ind},fullfile(handles.vars.path_inf,handles.vars.filename_inf));
    if findindex
        scanindex=ind-1;
    end
end

handles.vars.worldtransmat =Dicom_Info_To_World_Coords(handles.vars.dicominfo, 1,[]);
     
%process text boxes
try
    handles.vars.minth = str2num(get(handles.edit6,'string'));
    handles.edit6.Value = handles.vars.minth;
catch
    handles.vars.minth = 0;
    set(handles.edit6,'value',0)
    set(handles.edit6,'String','0')
end
try
    handles.vars.maxth = str2num(get(handles.edit7,'string'));
    handles.edit7.Value = handles.vars.maxth;
catch
    handles.vars.maxth = 2;
    set(handles.edit7,'value',2)
    set(handles.edit7,'String','2')
end
set(handles.axes1,'xtick',[],'ytick',[])
selectedcoords = handles.vars.points(handles.vars.points(:,5)==scanindex & handles.vars.points(:,6)~=0 & handles.vars.points(:,1)>handles.vars.minth & handles.vars.points(:,1)<handles.vars.maxth & abs(handles.vars.points(:,6)-handles.vars.points(:,1))>0.1 ,:);
if numel(selectedcoords)==0
    print('Selected dicom was not found in the results, for this particular validation/test set')
    return
end
handles.vars.selectedcoords = selectedcoords;
handles.vars.selectedcoords(:,2:4)= handles.vars.selectedcoords(:,2:4)+1;

handles.axes3=axes('position', [0.5395 0.106 0.4145 0.698], 'color', 'none', ...
           'XLim', [0, size(handles.vars.dicom,1)], 'YLim', [0, size(handles.vars.dicom,2)]);
imagesc(handles.vars.dicom(:,:,1),'Parent',handles.axes3);

set(handles.text13,'String',1);
handles.scataxes=axes('position', [0.5395 0.106 0.4145 0.698], 'color', 'none', ...
           'XLim', [0, 1], 'YLim', [0, 1]);

scatter(handles.scataxes,handles.vars.selectedcoords(handles.vars.selectedcoords(:,4)==1,2),...
    handles.vars.selectedcoords(handles.vars.selectedcoords(:,4)==1,3),[],...
    abs(handles.vars.selectedcoords(handles.vars.selectedcoords(:,4)==1,6)-handles.vars.selectedcoords(handles.vars.selectedcoords(:,4)==1,1)),'filled')
set(handles.scataxes,'xtick',[],'ytick',[])
set(gca,'YDir','reverse')
linkaxes([handles.axes3,handles.scataxes])

handles.scataxes.Visible = 'off';
handles.scataxes.XTick = [];
handles.scataxes.YTick = [];
%% Give each one its own colormap
colormap(handles.axes3,'gray')
colormap(handles.scataxes,'hsv')
handles.cbar = colorbar(handles.scataxes,'Position',[0.96 0.106 0.0145 0.698]);
handles.vars.cmax = max(abs(handles.vars.selectedcoords(:,6)-handles.vars.selectedcoords(:,1)));
handles.vars.cmin = min(abs(handles.vars.selectedcoords(:,6)-handles.vars.selectedcoords(:,1)));
try
   caxis([handles.vars.cmin handles.vars.cmax])
catch
    caxis([0 1]) 
end
% handles.vars.dicomsc(handles.vars.dicom(:,:,1),'Parent',handles.axes1);
zoom(handles.axes3,'reset')
%colormap(handles.axes1,gray);
set(handles.slider1,'Min',1,'Max',size(handles.vars.dicom,3),'Value',1);
set(handles.text13,'String','1');
set(handles.slider1,'SliderStep',[1/(size(handles.vars.dicom,3)-1) 10/(size(handles.vars.dicom,3)-1)]);
handles.dcm = datacursormode(gcf);
handles.vars.slice=1;

set(handles.dcm,'UpdateFcn',{@myfunc,handles});
handles.vars.ylim = get(handles.axes3,'YLim');
handles.vars.xlim = get(handles.axes3,'XLim');


% %handles.vars.cmap = cmin + (cmax-cmin)*linspace(0,cmax,numel(handles.vars.selectedcoords(:,6)));
% handles.scataxes=axes;
% scatter(handles.scataxes,selectedcoords(selectedcoords(:,4)==1,2),selectedcoords(selectedcoords(:,4)==1,3),...
%     [],abs(handles.vars.selectedcoords(selectedcoords(:,4)==1,6)-handles.vars.selectedcoords(selectedcoords(:,4)==1,1))/handles.vars.cmin);
% linkaxes([handles.axes3,handles.scataxes])
% handles.scataxes.Visible = 'off';
% 
% %% Give each one its own colormap
% colormap(handles.scataxes,'cool')
% %% Then add colorbars and get everything lined up
% set([handles.axes1,handles.scataxes],'Position',[189.8 8.231 145.8 54.154]);
guidata(hObject, handles);






function edit1_Callback(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit1 as text
%        str2double(get(hObject,'String')) returns contents of edit1 as a double


% --- Executes during object creation, after setting all properties.
function edit1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit2_Callback(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit2 as text
%        str2double(get(hObject,'String')) returns contents of edit2 as a double


% --- Executes during object creation, after setting all properties.
function edit2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton4.
function pushbutton4_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton4 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
handles.vars.path_opts = 'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method'
handles.vars.filename_opts = 'scanindexdict.txt'
set(handles.edit3,'String',fullfile(handles.vars.path_opts,handles.vars.filename_opts))
if exist(fullfile(handles.vars.path_opts,handles.vars.filename_opts),'file')
    txt = fopen(fullfile(handles.vars.path_opts,handles.vars.filename_opts));
    scannedtext = textscan(txt,'%s','Whitespace','\n','TreatAsEmpty',' ');
    handles.vars.dict = scannedtext{1};
else
    set(handles.edit3,'String','File not found. Open manually')
    [filename, path] = uigetfile('.txt', 'Select sample options file','MultiSelect', 'on');
    handles.vars.filename_opts = filename;
    handles.vars.path_opts = path;
    if ~filename==0
        set(handles.edit3,'String',fullfile(path,filename))
        txt = fopen(fullfile(path,filename));
        scannedtext = textscan(txt,'%s','Whitespace','\n','TreatAsEmpty',' ');
        handles.vars.dict = scannedtext{1};
    else
        set(handles.edit3,'String','Something went wrong')
    end
end

guidata(hObject, handles);


function edit3_Callback(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit3 as text
%        str2double(get(hObject,'String')) returns contents of edit3 as a double


% --- Executes during object creation, after setting all properties.
function edit3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'Value') returns position of slider
%        get(hObject,'Min') and get(hObject,'Max') to determine range of slider
try
cla(handles.axes2,'reset');

%delete(findall(gcf,'Type','hggroup'));
catch
end

set(handles.axes1,'xtick',[],'ytick',[])
set(handles.axes3,'xtick',[],'ytick',[])
slice = round(get(hObject,'Value'));
handles.axes3=axes('position', [0.5395 0.106 0.4145 0.698], 'color', 'none', ...
           'XLim', [0, size(handles.vars.dicom(:,:,slice),1)], 'YLim', [0, size(handles.vars.dicom(:,:,slice),2)]);
imagesc(handles.vars.dicom(:,:,slice),'Parent',handles.axes3);

set(handles.text13,'String',slice);
handles.scataxes=axes('position', [0.5395 0.106 0.4145 0.698], 'color', 'none', ...
           'XLim', [0, 1], 'YLim', [0, 1]);

scatter(handles.scataxes,handles.vars.selectedcoords(handles.vars.selectedcoords(:,4)==slice,2),...
    handles.vars.selectedcoords(handles.vars.selectedcoords(:,4)==slice,3),[],...
    abs(handles.vars.selectedcoords(handles.vars.selectedcoords(:,4)==slice,6)-handles.vars.selectedcoords(handles.vars.selectedcoords(:,4)==slice,1)),'filled')
set(handles.scataxes,'xtick',[],'ytick',[])
caxis([handles.vars.cmin handles.vars.cmax])
handles.scataxes.Visible = 'off';
set(gca,'YDir','reverse')
linkaxes([handles.axes3,handles.scataxes])


handles.scataxes.XTick = [];
handles.scataxes.YTick = [];
%% Give each one its own colormap
colormap(handles.axes3,'gray')
colormap(handles.scataxes,'hsv')

handles.vars.slice=slice;
handles.scataxes.Visible = 'off';
hold(handles.axes3,'off');
set(handles.text13,'String',slice);
setappdata(0,'currentslice',slice);
set(handles.dcm,'UpdateFcn',{@myfunc,handles});


guidata(hObject,handles)

% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on button press in pushbutton5.
function pushbutton5_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
xlim=handles.vars.xlim;
ylim=handles.vars.ylim;
set(handles.axes3,'Xlim',xlim,'Ylim',ylim)

guidata(hObject,handles)




function output_txt = myfunc(hObject,event_obj,handles)

Blockradius=[2,2,2];
clickedpoint=get(event_obj,'Position');
clickedpoint = [round(clickedpoint(1,1)),round(clickedpoint(1,2)),str2double(get(handles.text13,'String'))];
handles.vars.clickedpoint = clickedpoint;
[~, outlist_skelo_points] = Dicom_Info_To_World_Coords(handles.vars.dicominfo, 1, [clickedpoint(2),clickedpoint(1),clickedpoint(3)] - 1);
Lia=ismember(handles.vars.selectedcoords(:,2:4),clickedpoint,'rows');
indx = find(Lia==1);
if ~indx==0
    set(handles.text4,'String',num2str(handles.vars.selectedcoords(indx,1)));
    set(handles.text5,'String',num2str(handles.vars.selectedcoords(indx,6)));   
else
    set(handles.text4,'String','');
    set(handles.text5,'String','');     
end
clickedpoint = [round(clickedpoint(1,1)),round(clickedpoint(1,2)),str2double(get(handles.text13,'String'))]+Blockradius;
%Initialization and marker position reader


nb_phi = handles.vars.paddedphi2(clickedpoint(1,2)-Blockradius(1):clickedpoint(1,2)+Blockradius(1),clickedpoint(1,1)-Blockradius(2):clickedpoint(1,1)+Blockradius(2),clickedpoint(1,3)-Blockradius(3):clickedpoint(1,3)+Blockradius(3));
nb_theta = handles.vars.paddedtheta2(clickedpoint(1,2)-Blockradius(1):clickedpoint(1,2)+Blockradius(1),clickedpoint(1,1)-Blockradius(2):clickedpoint(1,1)+Blockradius(2),clickedpoint(1,3)-Blockradius(3):clickedpoint(1,3)+Blockradius(3));
removecenter=zeros(Blockradius(1)*2+1,Blockradius(1)*2+1,Blockradius(1)*2+1);
removecenter(Blockradius(1)+1,Blockradius(1)+1,Blockradius(1)+1)=1;
long_theta = nb_theta(~removecenter);

[~,dens,xvec,yvec] = kde2d([nb_phi(~removecenter),long_theta],2^7);

max_height = max(max(dens));
newphi = xvec(dens==max_height);
newtheta = yvec(dens==max_height);

% use new calculated point
point = [1,newphi,newtheta];

% use old point from results
[xline_region, yline_region, zline_region]  = Drawline(point(2), point(3), outlist_skelo_points, 4, 100);

Local_evalline_region   =  handles.vars.worldtransmat^-1*[ [xline_region; yline_region; zline_region]; ones(1,size([xline_region; yline_region; zline_region], 2))]+1;
inter_val_region        = interp3(handles.vars.dicom, Local_evalline_region(2, :), Local_evalline_region(1, :), Local_evalline_region(3, :), 'cubic');

plot(handles.axes2,linspace(0,4,100),inter_val_region)
output_txt={[['X:',num2str(clickedpoint(1,1)-Blockradius(1))],[
    ',Y:',num2str(clickedpoint(1,2)-Blockradius(1))]]};

guidata(hObject,handles)


function edit5_Callback(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit5 as text
%        str2double(get(hObject,'String')) returns contents of edit5 as a double


% --- Executes during object creation, after setting all properties.
function edit5_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit5 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit6_Callback(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit6 as text
%        str2double(get(hObject,'String')) returns contents of edit6 as a double


% --- Executes during object creation, after setting all properties.
function edit6_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit6 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function edit7_Callback(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of edit7 as text
%        str2double(get(hObject,'String')) returns contents of edit7 as a double


% --- Executes during object creation, after setting all properties.
function edit7_CreateFcn(hObject, eventdata, handles)
% hObject    handle to edit7 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in pushbutton_vview.
function pushbutton_vview_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton_vview (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
clickedpoint=handles.vars.clickedpoint;
phieval = handles.vars.results.GradDirReg(clickedpoint(2),clickedpoint(1),clickedpoint(3),2);
thetaeval = handles.vars.results.GradDirReg(clickedpoint(2),clickedpoint(1),clickedpoint(3),3);

Vesselviewer(handles.vars.results.skeleton,4,phieval,thetaeval,[clickedpoint(2),clickedpoint(1),clickedpoint(3)],[10 10 10],handles.vars.worldtransmat);


% --- Executes during object creation, after setting all properties.
function axes2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to axes2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: place code in OpeningFcn to populate axes2
