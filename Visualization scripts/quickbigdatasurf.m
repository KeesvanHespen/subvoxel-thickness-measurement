function quickbigdatasurf(HRLRresults, bins)
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%  File used plot the high vs low res    %
%  thickness measurement results         %
%                                        %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% INPUT: HRLRresults: 2D column array, with the first column the low res
% thickness measurements, and the second column the high res thickness
% measurements.
%        bins: number of bins used to plot the median and IQRs
%
% Kees van Hespen, UMC Utrecht, 2020
%
HRLRresults(HRLRresults>10)=0;
bins=20;
clear maxval
[his,bars]=hist3(HRLRresults,[bins bins]);
count_per_bin=[];
highresvals=HRLRresults(:,2);
lowresvals=HRLRresults(:,1);
xhold=[];
med=[];
intqr=[];
quantile25=[];
quantile75=[];
for linepoint=1:size(his,1)
    if sum(his(:,linepoint))<1 || bars{2}(1,linepoint)>1.51
         his(:,linepoint)=zeros(size(his(:,linepoint)));
         values{1,linepoint} = bars{2}(1,linepoint);
         values{2,linepoint} = nan;
    else
        if ~(exist('maxval'))
            ll=linepoint;
            dataline = (highresvals<(bars{2}(1,linepoint)+(bars{2}(1,2)-bars{2}(1,1))/2) & highresvals>(bars{2}(1,linepoint)-(bars{2}(1,2)-bars{2}(1,1))/2));
            
            inrangevalues = lowresvals.*dataline;

            inrangevalues = inrangevalues(inrangevalues~=0);
  
            minval=0;
        end
        maxval = max(his(:,linepoint));
        val=bars{2}(1,linepoint);
        if maxval~=0
        his(:,linepoint)=his(:,linepoint)/max(his(:,linepoint));
        end
 
        dataline = (highresvals<(bars{2}(1,linepoint)+(bars{2}(1,2)-bars{2}(1,1))/2)& highresvals>(bars{2}(1,linepoint)-(bars{2}(1,2)-bars{2}(1,1))/2));
        count_per_bin(linepoint)=sum(dataline);
        inrangevalues = lowresvals.*dataline;
        inrangevalues = inrangevalues(inrangevalues~=0);
        values{1,linepoint} = bars{2}(1,linepoint);
        values{2,linepoint} = inrangevalues;
        med=[med median(inrangevalues)];
        intqr=[intqr iqr(inrangevalues)/2];
        xhold=[xhold bars{2}(1,linepoint)];
        quantile25 = [quantile25 quantile(inrangevalues,0.25)];
        quantile75 = [quantile75 quantile(inrangevalues,0.75)];
        quantile25(isnan(quantile25))=0;
        quantile75(isnan(quantile75))=0;
    end

end
hold off;
if 0
    midquantile = [(quantile75+quantile25)/2];
    lengthbar= quantile75-quantile25;
      errorbar(xhold,midquantile...
     ,lengthbar/2,'LineStyle','none','marker','none','MarkerSize',8)
    hold on;
    plot(minval:0.01:round(val+0.2,1),minval:0.01:round(val+0.2,1),'Color',[0.4,0.4,0.4],'linewidth',2)
    plot(xhold,med,'LineStyle','-','marker','o','MarkerSize',8)
    hold off;
end
if 1
    figure;
    subplot(2,1,1)
    hold on;
    surf(bars{2},bars{1},his,'EdgeColor','none');
    plot3([max(bars{1}(1),bars{2}(1)),min(bars{1}(end),bars{2}(end))],[max(bars{1}(1),bars{2}(1)),min(bars{1}(end),bars{2}(end))],[10000;10000],'LineStyle','-','Color',[1,1,1],'linewidth',2)
    xlabel('Reference thickness')
    ylabel('Estimated thickness')
    set(gca,'linewidth',2)
    set(gca,'FontSize',14)
    hold off;
    yyaxis right
    plot(bars{2},count_per_bin)
    ylabel('Number of samples per bin')
    set(gca,'linewidth',2)
    set(gca,'FontSize',14)
    view(2)
    box on
    axis tight
end
subplot(2,1,2)
fill([xhold';flipud(xhold')],[quantile25';flipud(quantile75')],[.9 .9 .9],'linestyle','none','FaceColor',[ .93 .84 .84],'FaceAlpha',1);
line(xhold,med,'linewidth',2,'color','r');
lh = legend('IQR','Median','Location','northwest');
lh.PlotChildren = lh.PlotChildren([2,1]);
hold on;
xlabel('Reference thickness (mm)');ylabel('Estimated thickness (mm)');
plot(minval:0.01:round(val+0.4,1),minval:0.01:round(val+0.4,1),'Color',[0.4,0.4,0.4],'linewidth',2)
box on;
axis tight
set(gca,'LineWidth',2)
set(gca,'FontSize',14)
hold off;
[a,dens,y,x]= kde2d(HRLRresults,2^8,[ 0.001 0.001],[1.5 1.5]);
if 0
    subplot(2,2,3)
    cmap=colormap('parula');
    cmap(1,:)=[1,1,1];
    
    surf(x,y,dens,'EdgeColor','none')
    plot3([max(bars{1}(1),bars{2}(1)),min(bars{1}(end),bars{2}(end))],[max(bars{1}(1),bars{2}(1)),min(bars{1}(end),bars{2}(end))],[10000;10000],'LineStyle','-','Color',[1,1,1],'linewidth',2)

    xlabel('Reference thickness')
    ylabel('Estimated thickness')

view(2);
box on;
set(gca,'LineWidth',4)
set(gca,'FontSize',14)
clb = colorbar;
set(clb,'Ytick',[min(dens(:));max(dens(:))],'YTicklabel',{'low','high'},'LineWidth',2,'FontSize',12)
colormap(cmap)
end
end