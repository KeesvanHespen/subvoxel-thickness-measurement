%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%                                                                   %
% Loads .mat image patches and corresponding high resolution images %
% Visualizes the low resolution image patches and the corresponding %
% locations in the high resolution images                           %  
%                                                                   %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% KM van Hespen, UMC Utrecht, 2020
%
FULLdcmname = {
'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\3. Data\20160307_S007_S005\ROI-S005\ROI_0042-S005.dcm',
'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\3. Data\20160307_S007_S005\ROI-S007\ROI_0042R-S007.dcm',
'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\3. Data\20160619_S001_S004\ROI-S001\ROI_0030-S001_2.dcm',
'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\3. Data\20160619_S001_S004\ROI-S004\ROI_0043-S004.dcm',
'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\3. Data\20160222_S011_S006\ROI-S006-3\ROI_0037-S006.dcm',
'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\3. Data\20160222_S011_S006\ROI-S011-3\ROI_0037-S011-3.dcm',
'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\3. Data\20160324_S012_S010\ROI-S010\ROI_0042R-S010.dcm',
'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\3. Data\20160618_S002_S003\ROI-S002-3\ROI_0049-S002.dcm',
'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\3. Data\20160618_S002_S003\ROI-S003-3\ROI_0049-S003.dcm',
'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\3. Data\20160627_S008_S009\ROI-S008-3\ROI_0031-S008.dcm',
'C:\Users\khespen\Documents\1. Vessel wall thickness measurements\2. NN method\3. Data\20160627_S008_S009\ROI-S009-3\ROI_0031-S009.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\30112017_S021_S022\S021\ROI_0091-S021.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\20171711_S017_S018\S017\ROI_0061-S017-2.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\20170412_S023_S024\S024\ROI_0121-S024.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\22022018_S033_S034\S034\ROI_0017-S034-33.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\01032018_S035_S036\S035\ROI_0024-S035.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\20172710_S015_S016\S015\ROI_0074-S015.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\01032018_S035_S036\S036\ROI_0024-S036.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\08032018_S037_S038\S037\ROI_0024-S037.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\08032018_S037_S038\S038\ROI_0024-S038.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\01022018_S029_S030\S029\ROI_0046-S029.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\01022018_S029_S030\S030\ROI_0046-S030.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\07022018_S031_S032\S031\ROI_0059-S031.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\07022018_S031_S032\S032\ROI_0059-S032.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\11012018_S025_S026\S026\ROI_0062-S026.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\18012018_S027_S028\S027\ROI_0071-S027.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\18012018_S027_S028\S028\ROI_0071-S028.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\30112017_S021_S022\S022\ROI_0091-S022.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\08092017_S013_S014\S013\ROI_0000-S013.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\08092017_S013_S014\S014\ROI_0000-S014.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\24052018_S041_S042\S042\ROI_0024-S042.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\24052018_S041_S042\S041\ROI_0024-S041.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\05072018_S047_S048\S048\ROI_0024-S048.dcm',
'C:\Users\khespen\Documents\5. Hypertension\1. Working data\05072018_S047_S048\S047\ROI_0024-S047.dcm'  }

ax1=figure;
HRLRresults2 = load('samples_0.4000370037_acq_0_test_2019-02-04_17;21;28_LR.mat');
HRLRresults2 = HRLRresults2.results;
HRLRresults = readNPY('samples_0.4000370037_acq_0_testcoords+origin_2019-02-04_17;21;28_LR.npy');
ax2=figure;
x=linspace(1,19,81)';
[xq,yq]=meshgrid(x,x);
for i=1:16
set(0, 'currentfigure', ax1);
subplot(4,4,i); imagesc(interp2(squeeze(HRLRresults2(50+i*500,1,:,:,10)),xq,yq,'spline'));
colormap gray
axis off
ind = HRLRresults(50+i*500,:);
thick(i)=ind(1);
img = dicomread(FULLdcmname{ind(5)+1});
subimg=double(img(ind(3)-34:ind(3)+34,ind(2)-34:ind(2)+34,ind(4)));
subimg = (subimg-min(subimg(:)))/(max(subimg(:))-min(subimg(:)));
[u,uu]=graythresh(subimg);
bg = mean(subimg(subimg<u));
wl=quantile(subimg(subimg>u),0.95);

set(0, 'currentfigure', ax2);
subplot(4,4,i); image((double(subimg)-bg)/(wl-bg)*64+10);
axis off
end
reshape(thick,[4,4])'
colormap gray